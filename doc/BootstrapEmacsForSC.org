#+TITLE: Bootstrap Emacs for SuperCollider
#+AUTHOR: Michal Seta
#+OPTIONS: num:nil toc:nil
#+STARTUP: showall
#+STARTUP: showstars

** Prerequisites

- [[https://github.com/supercollider/supercollider][SuperCollider]]
- Emacs (>25)
- [[https://github.com/supercollider/scel][sc-el]] - sclang-mode for Emacs
- [[https://orgmode.org][Org-mode]]
- [[https://github.com/djiamnot/ob-sclang.git][ob-sclang]]

** Installation

Follow the respective software installation and configuration instructions.
