HOADecSatoCube{

classvar <>speakerPositions;
classvar <>sweeterPositions;
classvar <>speakerLabels;
classvar <>sweetSpot;

*initClass { 
speakerPositions = [
					[ -1, 1, 2.5499 ],  //topfrontleft
					[ 1, 1, 2.5499 ],  //topfrontright
					[ 1, -1, 2.5499 ],  //topbackright
					[ -1, -1, 2.5499 ],  //topbackleft
					[ -1, 1, 0.4499 ],  //bottomfrontleft
					[ 1, 1, 0.4499 ],  //bottomfrontright
					[ 1, -1, 0.4499 ],  //bottombackleft
					[ -1, -1, 0.4499 ]  //bottombackright
					];

sweeterPositions = [
					[ -1.0, 1.0, 2.05 ],  //topfrontleft
					[ 1.0, 1.0, 2.05 ],  //topfrontright
					[ 1.0, -1.0, 2.05 ],  //topbackright
					[ -1.0, -1.0, 2.05 ],  //topbackleft
					[ -1.0, 1.0, -0.05 ],  //bottomfrontleft
					[ 1.0, 1.0, -0.05 ],  //bottomfrontright
					[ 1.0, -1.0, -0.05 ],  //bottombackleft
					[ -1.0, -1.0, -0.05 ]  //bottombackright
					];

speakerLabels = [
					'topfrontleft',
					'topfrontright',
					'topbackright',
					'topbackleft',
					'bottomfrontleft',
					'bottomfrontright',
					'bottombackleft',
					'bottombackright'
					];

sweetSpot = [ 0.0, 0.0, 0.499 ];

}

*ar { |order, in, gain(-10.0), lf_hf(0.0), mute(0.0), xover(400.0)|
// declare variables for the b-format array in
// distribute the channels from the array over in1 ... inN
// return the Ugen with the b-format channels and with the args from the *ar method
case
{order == 1}
	{ var in1, in2, in3, in4; 
	#in1,in2, in3, in4 = in; 
	^SatoCube1.ar(in1, in2, in3, in4, gain, lf_hf, mute, xover)}

{order == 2}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9 = in; 
	^SatoCube2.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, gain, lf_hf, mute, xover)}

{order == 3}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16 = in; 
	^SatoCube3.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, gain, lf_hf, mute, xover)}

{order == 4}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25 = in; 
	^SatoCube4.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, gain, lf_hf, mute, xover)} 

{order == 5}
	{var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36 = in; 
	^SatoCube5.ar(in1,in2, in3, in4,in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36, gain, lf_hf, mute, xover)} 

{"Order 5 is not implemented for HOADecSatoCube".postln} 
 } 



}