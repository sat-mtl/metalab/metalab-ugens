HOADecHaptic2000{

classvar <>speakerPositions;
classvar <>sweeterPositions;
classvar <>speakerLabels;
classvar <>sweetSpot;

*initClass { 
speakerPositions = [
					[ 0.0, 0.0, 0.5 ],  //center
					[ -0.5, 1, 0.3 ],  //LU
					[ 0.5, 1, 0.3 ],  //top
					[ 1, 0.2, 0.3 ],  //RU
					[ 0, -1, 0.3 ],  //RD
					[ -1, -0.2, 0.3 ],  //bottom
					[ -1, 0.3, 0.3 ]  //LD
					];

sweeterPositions = [
					[ 0.142, -0.186, 1.171 ],  //center
					[ -0.358, 0.814, 0.971 ],  //LU
					[ 0.642, 0.814, 0.971 ],  //top
					[ 1.142, 0.014, 0.971 ],  //RU
					[ 0.142, -1.186, 0.971 ],  //RD
					[ -0.858, -0.386, 0.971 ],  //bottom
					[ -0.858, 0.114, 0.971 ]  //LD
					];

speakerLabels = [
					"center",
					"LU",
					"top",
					"RU",
					"RD",
					"bottom",
					"LD"
					];

sweetSpot = [ -0.143, 0.185, -0.672 ];

}

*ar { |order, in, gain(-10.0), lf_hf(0.0), mute(0.0), xover(400.0)|
// declare variables for the b-format array in
// distribute the channels from the array over in1 ... inN
// return the Ugen with the b-format channels and with the args from the *ar method
case
{order == 1}
	{ var in1, in2, in3, in4; 
	#in1,in2, in3, in4 = in; 
	^Haptic20001.ar(in1, in2, in3, in4, gain, lf_hf, mute, xover)}

{order == 2}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9 = in; 
	^Haptic20002.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, gain, lf_hf, mute, xover)}

{order == 3}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16 = in; 
	^Haptic20003.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, gain, lf_hf, mute, xover)}

{order == 4}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25 = in; 
	^Haptic20004.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, gain, lf_hf, mute, xover)} 

{order == 5}
	{var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36 = in; 
	^Haptic20005.ar(in1,in2, in3, in4,in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36, gain, lf_hf, mute, xover)} 

{"Order 5 is not implemented for HOADecHaptic2000".postln} 
 } 



}