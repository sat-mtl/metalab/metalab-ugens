HOADecSATOsw0o{

classvar <>speakerPositions;
classvar <>sweeterPositions;
classvar <>speakerLabels;
classvar <>sweetSpot;

*initClass { 
speakerPositions = [
					[ 0.0, 0.0, 8.94 ],  //top
					[ 1.47, 5.48, 6.91 ],  //high
					[ -4.01, 4.01, 6.91 ],  //high
					[ -5.48, -1.47, 6.91 ],  //high
					[ -1.47, -5.48, 6.91 ],  //high
					[ 4.01, -4.01, 6.91 ],  //high
					[ 5.48, 1.47, 6.91 ],  //high
					[ 0.0, 7.88, 4.21 ],  //middle
					[ -3.94, 6.83, 4.21 ],  //middle
					[ -6.83, 3.94, 4.21 ],  //middle
					[ -7.88, 0.0, 4.21 ],  //middle
					[ -6.83, -3.94, 4.21 ],  //middle
					[ -3.94, -6.83, 4.21 ],  //middle
					[ 0.0, -7.88, 4.21 ],  //middle
					[ 3.94, -6.83, 4.21 ],  //middle
					[ 6.83, -3.94, 4.21 ],  //middle
					[ 7.88, 0.0, 4.21 ],  //middle
					[ 6.83, 3.94, 4.21 ],  //middle
					[ 3.94, 6.83, 4.21 ],  //middle
					[ 0.0, 8.9, -0.88 ],  //bottom
					[ -4.45, 7.71, -0.88 ],  //bottom
					[ -7.71, 4.45, -0.88 ],  //bottom
					[ -8.9, 0.0, -0.88 ],  //bottom
					[ -7.71, -4.45, -0.88 ],  //bottom
					[ -4.45, -7.71, -0.88 ],  //bottom
					[ 0.0, -8.9, -0.88 ],  //bottom
					[ 4.45, -7.71, -0.88 ],  //bottom
					[ 7.71, -4.45, -0.88 ],  //bottom
					[ 8.9, 0.0, -0.88 ],  //bottom
					[ 7.71, 4.45, -0.88 ],  //bottom
					[ 4.45, 7.71, -0.88 ]  //bottom
					];

sweeterPositions = [
					[ 0.0, 0.0, 7.025 ],  //top
					[ 1.47, 5.48, 4.995 ],  //high
					[ -4.01, 4.009, 4.995 ],  //high
					[ -5.48, -1.47, 4.995 ],  //high
					[ -1.47, -5.48, 4.995 ],  //high
					[ 4.009, -4.01, 4.995 ],  //high
					[ 5.48, 1.47, 4.995 ],  //high
					[ 0.0, 7.88, 2.295 ],  //middle
					[ -3.94, 6.83, 2.295 ],  //middle
					[ -6.83, 3.94, 2.295 ],  //middle
					[ -7.88, 0.0, 2.295 ],  //middle
					[ -6.83, -3.94, 2.295 ],  //middle
					[ -3.94, -6.83, 2.295 ],  //middle
					[ 0.0, -7.88, 2.295 ],  //middle
					[ 3.94, -6.83, 2.295 ],  //middle
					[ 6.83, -3.94, 2.295 ],  //middle
					[ 7.88, 0.0, 2.295 ],  //middle
					[ 6.83, 3.94, 2.295 ],  //middle
					[ 3.94, 6.83, 2.295 ],  //middle
					[ 0.0, 8.9, -2.795 ],  //bottom
					[ -4.45, 7.71, -2.795 ],  //bottom
					[ -7.71, 4.45, -2.795 ],  //bottom
					[ -8.9, 0.0, -2.795 ],  //bottom
					[ -7.71, -4.45, -2.795 ],  //bottom
					[ -4.45, -7.71, -2.795 ],  //bottom
					[ 0.0, -8.9, -2.795 ],  //bottom
					[ 4.45, -7.71, -2.795 ],  //bottom
					[ 7.71, -4.45, -2.795 ],  //bottom
					[ 8.9, 0.0, -2.795 ],  //bottom
					[ 7.71, 4.45, -2.795 ],  //bottom
					[ 4.45, 7.71, -2.795 ]  //bottom
					];

speakerLabels = [
					"top",
					"high",
					"high",
					"high",
					"high",
					"high",
					"high",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"middle",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom",
					"bottom"
					];

sweetSpot = [ -0.001, -0.001, 1.914 ];

}

*ar { |order, in, gain(-10.0), lf_hf(0.0), mute(0.0), xover(400.0)|
// declare variables for the b-format array in
// distribute the channels from the array over in1 ... inN
// return the Ugen with the b-format channels and with the args from the *ar method
case
{order == 1}
	{ var in1, in2, in3, in4; 
	#in1,in2, in3, in4 = in; 
	^SATOsw0o1.ar(in1, in2, in3, in4, gain, lf_hf, mute, xover)}

{order == 2}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9 = in; 
	^SATOsw0o2.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, gain, lf_hf, mute, xover)}

{order == 3}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16 = in; 
	^SATOsw0o3.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, gain, lf_hf, mute, xover)}

{order == 4}
	{ var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25 = in; 
	^SATOsw0o4.ar(in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, gain, lf_hf, mute, xover)} 

{order == 5}
	{var in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36; 
	#in1, in2, in3, in4, in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36 = in; 
	^SATOsw0o5.ar(in1,in2, in3, in4,in5, in6, in7, in8, in9, in10, in11, in12, in13, in14, in15, in16, in17, in18, in19, in20, in21, in22, in23, in24, in25, in26, in27, in28, in29, in30, in31, in32, in33, in34, in35, in36, gain, lf_hf, mute, xover)} 

{"Order 5 is not implemented for HOADecSATOsw0o".postln} 
 } 



}