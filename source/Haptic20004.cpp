/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "Haptic20004"
version: "1.2"
Code generated with Faust 2.5.23 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// The prefix is set to "Faust" in the faust2supercollider script, otherwise set empty
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX ""
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fHslider0;
	FAUSTFLOAT fCheckbox0;
	float fRec0[2];
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	FAUSTFLOAT fHslider1;
	float fRec1[2];
	float fRec2[3];
	FAUSTFLOAT fHslider2;
	float fRec3[2];
	float fConst2;
	float fConst3;
	float fConst4;
	float fConst5;
	float fConst6;
	float fConst7;
	float fConst8;
	float fRec16[3];
	float fRec17[3];
	float fRec18[3];
	float fRec19[3];
	float fRec20[3];
	float fRec21[3];
	float fRec22[3];
	float fRec23[3];
	float fRec24[3];
	float fConst9;
	float fConst10;
	float fRec15[2];
	float fRec13[2];
	float fRec12[2];
	float fRec10[2];
	float fRec9[2];
	float fRec7[2];
	float fRec6[2];
	float fRec4[2];
	float fConst11;
	float fConst12;
	float fConst13;
	float fConst14;
	float fRec34[3];
	float fRec35[3];
	float fRec36[3];
	float fRec37[3];
	float fRec38[3];
	float fRec39[3];
	float fRec40[3];
	float fConst15;
	float fConst16;
	float fRec33[2];
	float fRec31[2];
	float fRec30[2];
	float fRec28[2];
	float fRec27[2];
	float fRec25[2];
	float fConst17;
	float fConst18;
	float fRec44[3];
	float fRec45[3];
	float fRec46[3];
	float fConst19;
	float fRec43[2];
	float fRec41[2];
	float fConst20;
	float fConst21;
	float fRec53[3];
	float fRec54[3];
	float fRec55[3];
	float fRec56[3];
	float fRec57[3];
	float fConst22;
	float fConst23;
	float fRec52[2];
	float fRec50[2];
	float fRec49[2];
	float fRec47[2];
	int IOTA;
	float fVec0[256];
	int iConst24;
	float fConst25;
	float fConst26;
	float fConst27;
	float fConst28;
	float fConst29;
	float fConst30;
	float fConst31;
	float fRec69[2];
	float fRec67[2];
	float fRec66[2];
	float fRec64[2];
	float fRec63[2];
	float fRec61[2];
	float fRec60[2];
	float fRec58[2];
	float fConst32;
	float fConst33;
	float fConst34;
	float fConst35;
	float fConst36;
	float fConst37;
	float fRec78[2];
	float fRec76[2];
	float fRec75[2];
	float fRec73[2];
	float fRec72[2];
	float fRec70[2];
	float fConst38;
	float fConst39;
	float fConst40;
	float fRec81[2];
	float fRec79[2];
	float fConst41;
	float fConst42;
	float fConst43;
	float fConst44;
	float fRec87[2];
	float fRec85[2];
	float fRec84[2];
	float fRec82[2];
	float fVec1[128];
	int iConst45;
	float fConst46;
	float fConst47;
	float fConst48;
	float fConst49;
	float fConst50;
	float fConst51;
	float fConst52;
	float fRec99[2];
	float fRec97[2];
	float fRec96[2];
	float fRec94[2];
	float fRec93[2];
	float fRec91[2];
	float fRec90[2];
	float fRec88[2];
	float fConst53;
	float fConst54;
	float fConst55;
	float fConst56;
	float fConst57;
	float fConst58;
	float fRec108[2];
	float fRec106[2];
	float fRec105[2];
	float fRec103[2];
	float fRec102[2];
	float fRec100[2];
	float fConst59;
	float fConst60;
	float fConst61;
	float fRec111[2];
	float fRec109[2];
	float fConst62;
	float fConst63;
	float fConst64;
	float fConst65;
	float fRec117[2];
	float fRec115[2];
	float fRec114[2];
	float fRec112[2];
	float fVec2[128];
	int iConst66;
	float fConst67;
	float fConst68;
	float fConst69;
	float fConst70;
	float fConst71;
	float fConst72;
	float fConst73;
	float fRec129[2];
	float fRec127[2];
	float fRec126[2];
	float fRec124[2];
	float fRec123[2];
	float fRec121[2];
	float fRec120[2];
	float fRec118[2];
	float fConst74;
	float fConst75;
	float fConst76;
	float fConst77;
	float fConst78;
	float fConst79;
	float fRec138[2];
	float fRec136[2];
	float fRec135[2];
	float fRec133[2];
	float fRec132[2];
	float fRec130[2];
	float fConst80;
	float fConst81;
	float fConst82;
	float fRec141[2];
	float fRec139[2];
	float fConst83;
	float fConst84;
	float fConst85;
	float fConst86;
	float fRec147[2];
	float fRec145[2];
	float fRec144[2];
	float fRec142[2];
	float fVec3[32];
	int iConst87;
	float fConst88;
	float fConst89;
	float fConst90;
	float fConst91;
	float fConst92;
	float fConst93;
	float fConst94;
	float fRec159[2];
	float fRec157[2];
	float fRec156[2];
	float fRec154[2];
	float fRec153[2];
	float fRec151[2];
	float fRec150[2];
	float fRec148[2];
	float fConst95;
	float fConst96;
	float fConst97;
	float fConst98;
	float fConst99;
	float fConst100;
	float fRec168[2];
	float fRec166[2];
	float fRec165[2];
	float fRec163[2];
	float fRec162[2];
	float fRec160[2];
	float fConst101;
	float fConst102;
	float fConst103;
	float fRec171[2];
	float fRec169[2];
	float fConst104;
	float fConst105;
	float fConst106;
	float fConst107;
	float fRec177[2];
	float fRec175[2];
	float fRec174[2];
	float fRec172[2];
	float fConst108;
	float fConst109;
	float fConst110;
	float fConst111;
	float fConst112;
	float fConst113;
	float fConst114;
	float fRec189[2];
	float fRec187[2];
	float fRec186[2];
	float fRec184[2];
	float fRec183[2];
	float fRec181[2];
	float fRec180[2];
	float fRec178[2];
	float fConst115;
	float fConst116;
	float fConst117;
	float fConst118;
	float fConst119;
	float fConst120;
	float fRec198[2];
	float fRec196[2];
	float fRec195[2];
	float fRec193[2];
	float fRec192[2];
	float fRec190[2];
	float fConst121;
	float fConst122;
	float fConst123;
	float fRec201[2];
	float fRec199[2];
	float fConst124;
	float fConst125;
	float fConst126;
	float fConst127;
	float fRec207[2];
	float fRec205[2];
	float fRec204[2];
	float fRec202[2];
	float fVec4[128];
	int iConst128;
	float fConst129;
	float fConst130;
	float fConst131;
	float fConst132;
	float fConst133;
	float fConst134;
	float fConst135;
	float fRec219[2];
	float fRec217[2];
	float fRec216[2];
	float fRec214[2];
	float fRec213[2];
	float fRec211[2];
	float fRec210[2];
	float fRec208[2];
	float fConst136;
	float fConst137;
	float fConst138;
	float fConst139;
	float fConst140;
	float fConst141;
	float fRec228[2];
	float fRec226[2];
	float fRec225[2];
	float fRec223[2];
	float fRec222[2];
	float fRec220[2];
	float fConst142;
	float fConst143;
	float fConst144;
	float fRec231[2];
	float fRec229[2];
	float fConst145;
	float fConst146;
	float fConst147;
	float fConst148;
	float fRec237[2];
	float fRec235[2];
	float fRec234[2];
	float fRec232[2];
	float fVec5[256];
	int iConst149;
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("filename", "Haptic20004");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "Haptic20004");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 25;
		
	}
	virtual int getNumOutputs() {
		return 7;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = (3.14159274f / float(iConst0));
		fConst2 = float(iConst0);
		fConst3 = mydsp_faustpower2_f(fConst2);
		fConst4 = ((((188489.938f / fConst2) + 831.817444f) / fConst2) + 1.0f);
		fConst5 = (0.0f - (753959.75f / (fConst3 * fConst4)));
		fConst6 = (0.0f - (((753959.75f / fConst2) + 1663.63489f) / (fConst2 * fConst4)));
		fConst7 = ((((236904.141f / fConst2) + 604.22699f) / fConst2) + 1.0f);
		fConst8 = (1.0f / (fConst4 * fConst7));
		fConst9 = (0.0f - (947616.562f / (fConst3 * fConst7)));
		fConst10 = (0.0f - (((947616.562f / fConst2) + 1208.45398f) / (fConst2 * fConst7)));
		fConst11 = ((333.476135f / fConst2) + 1.0f);
		fConst12 = (0.0f - (666.952271f / (fConst2 * fConst11)));
		fConst13 = ((((133207.953f / fConst2) + 528.150513f) / fConst2) + 1.0f);
		fConst14 = (1.0f / (fConst11 * fConst13));
		fConst15 = (0.0f - (532831.812f / (fConst3 * fConst13)));
		fConst16 = (0.0f - (((532831.812f / fConst2) + 1056.30103f) / (fConst2 * fConst13)));
		fConst17 = ((143.604446f / fConst2) + 1.0f);
		fConst18 = (1.0f / fConst17);
		fConst19 = (0.0f - (287.208893f / (fConst2 * fConst17)));
		fConst20 = ((((61866.7109f / fConst2) + 430.813324f) / fConst2) + 1.0f);
		fConst21 = (1.0f / fConst20);
		fConst22 = (0.0f - (247466.844f / (fConst3 * fConst20)));
		fConst23 = (0.0f - (((247466.844f / fConst2) + 861.626648f) / (fConst2 * fConst20)));
		iConst24 = int(((0.00100520195f * float(iConst0)) + 0.5f));
		fConst25 = ((((155185.969f / fConst2) + 754.762207f) / fConst2) + 1.0f);
		fConst26 = (0.0f - (620743.875f / (fConst3 * fConst25)));
		fConst27 = (0.0f - (((620743.875f / fConst2) + 1509.52441f) / (fConst2 * fConst25)));
		fConst28 = ((((195045.938f / fConst2) + 548.254578f) / fConst2) + 1.0f);
		fConst29 = (1.0f / (fConst25 * fConst28));
		fConst30 = (0.0f - (780183.75f / (fConst3 * fConst28)));
		fConst31 = (0.0f - (((780183.75f / fConst2) + 1096.50916f) / (fConst2 * fConst28)));
		fConst32 = ((302.584656f / fConst2) + 1.0f);
		fConst33 = (0.0f - (605.169312f / (fConst2 * fConst32)));
		fConst34 = ((((109671.656f / fConst2) + 479.225433f) / fConst2) + 1.0f);
		fConst35 = (1.0f / (fConst32 * fConst34));
		fConst36 = (0.0f - (438686.625f / (fConst3 * fConst34)));
		fConst37 = (0.0f - (((438686.625f / fConst2) + 958.450867f) / (fConst2 * fConst34)));
		fConst38 = ((130.301682f / fConst2) + 1.0f);
		fConst39 = (1.0f / fConst38);
		fConst40 = (0.0f - (260.603363f / (fConst2 * fConst38)));
		fConst41 = ((((50935.582f / fConst2) + 390.905029f) / fConst2) + 1.0f);
		fConst42 = (1.0f / fConst41);
		fConst43 = (0.0f - (203742.328f / (fConst3 * fConst41)));
		fConst44 = (0.0f - (((203742.328f / fConst2) + 781.810059f) / (fConst2 * fConst41)));
		iConst45 = int(((0.000649739231f * float(iConst0)) + 0.5f));
		fConst46 = ((((133301.75f / fConst2) + 699.522766f) / fConst2) + 1.0f);
		fConst47 = (0.0f - (533207.0f / (fConst3 * fConst46)));
		fConst48 = (0.0f - (((533207.0f / fConst2) + 1399.04553f) / (fConst2 * fConst46)));
		fConst49 = ((((167540.688f / fConst2) + 508.128967f) / fConst2) + 1.0f);
		fConst50 = (1.0f / (fConst46 * fConst49));
		fConst51 = (0.0f - (670162.75f / (fConst3 * fConst49)));
		fConst52 = (0.0f - (((670162.75f / fConst2) + 1016.25793f) / (fConst2 * fConst49)));
		fConst53 = ((280.439117f / fConst2) + 1.0f);
		fConst54 = (0.0f - (560.878235f / (fConst2 * fConst53)));
		fConst55 = ((((94205.8359f / fConst2) + 444.151917f) / fConst2) + 1.0f);
		fConst56 = (1.0f / (fConst53 * fConst55));
		fConst57 = (0.0f - (376823.344f / (fConst3 * fConst55)));
		fConst58 = (0.0f - (((376823.344f / fConst2) + 888.303833f) / (fConst2 * fConst55)));
		fConst59 = ((120.765175f / fConst2) + 1.0f);
		fConst60 = (1.0f / fConst59);
		fConst61 = (0.0f - (241.53035f / (fConst2 * fConst59)));
		fConst62 = ((((43752.6797f / fConst2) + 362.295532f) / fConst2) + 1.0f);
		fConst63 = (1.0f / fConst62);
		fConst64 = (0.0f - (175010.719f / (fConst3 * fConst62)));
		fConst65 = (0.0f - (((175010.719f / fConst2) + 724.591064f) / (fConst2 * fConst62)));
		iConst66 = int(((0.000346721819f * float(iConst0)) + 0.5f));
		fConst67 = ((((119630.375f / fConst2) + 662.681213f) / fConst2) + 1.0f);
		fConst68 = (0.0f - (478521.5f / (fConst3 * fConst67)));
		fConst69 = (0.0f - (((478521.5f / fConst2) + 1325.36243f) / (fConst2 * fConst67)));
		fConst70 = ((((150357.797f / fConst2) + 481.367523f) / fConst2) + 1.0f);
		fConst71 = (1.0f / (fConst67 * fConst70));
		fConst72 = (0.0f - (601431.188f / (fConst3 * fConst70)));
		fConst73 = (0.0f - (((601431.188f / fConst2) + 962.735046f) / (fConst2 * fConst70)));
		fConst74 = ((265.669312f / fConst2) + 1.0f);
		fConst75 = (0.0f - (531.338623f / (fConst2 * fConst74)));
		fConst76 = ((((84544.125f / fConst2) + 420.759918f) / fConst2) + 1.0f);
		fConst77 = (1.0f / (fConst74 * fConst76));
		fConst78 = (0.0f - (338176.5f / (fConst3 * fConst76)));
		fConst79 = (0.0f - (((338176.5f / fConst2) + 841.519836f) / (fConst2 * fConst76)));
		fConst80 = ((114.404877f / fConst2) + 1.0f);
		fConst81 = (1.0f / fConst80);
		fConst82 = (0.0f - (228.809753f / (fConst2 * fConst80)));
		fConst83 = ((((39265.4258f / fConst2) + 343.21463f) / fConst2) + 1.0f);
		fConst84 = (1.0f / fConst83);
		fConst85 = (0.0f - (157061.703f / (fConst3 * fConst83)));
		fConst86 = (0.0f - (((157061.703f / fConst2) + 686.42926f) / (fConst2 * fConst83)));
		iConst87 = int(((0.000116545154f * float(iConst0)) + 0.5f));
		fConst88 = ((((113496.523f / fConst2) + 645.46875f) / fConst2) + 1.0f);
		fConst89 = (0.0f - (453986.094f / (fConst3 * fConst88)));
		fConst90 = (0.0f - (((453986.094f / fConst2) + 1290.9375f) / (fConst2 * fConst88)));
		fConst91 = ((((142648.438f / fConst2) + 468.864471f) / fConst2) + 1.0f);
		fConst92 = (1.0f / (fConst88 * fConst91));
		fConst93 = (0.0f - (570593.75f / (fConst3 * fConst91)));
		fConst94 = (0.0f - (((570593.75f / fConst2) + 937.728943f) / (fConst2 * fConst91)));
		fConst95 = ((258.768829f / fConst2) + 1.0f);
		fConst96 = (0.0f - (517.537659f / (fConst2 * fConst95)));
		fConst97 = ((((80209.2578f / fConst2) + 409.831085f) / fConst2) + 1.0f);
		fConst98 = (1.0f / (fConst95 * fConst97));
		fConst99 = (0.0f - (320837.031f / (fConst3 * fConst97)));
		fConst100 = (0.0f - (((320837.031f / fConst2) + 819.66217f) / (fConst2 * fConst97)));
		fConst101 = ((111.433319f / fConst2) + 1.0f);
		fConst102 = (1.0f / fConst101);
		fConst103 = (0.0f - (222.866638f / (fConst2 * fConst101)));
		fConst104 = ((((37252.1523f / fConst2) + 334.299957f) / fConst2) + 1.0f);
		fConst105 = (1.0f / fConst104);
		fConst106 = (0.0f - (149008.609f / (fConst3 * fConst104)));
		fConst107 = (0.0f - (((149008.609f / fConst2) + 668.599915f) / (fConst2 * fConst104)));
		fConst108 = ((((147255.188f / fConst2) + 735.223267f) / fConst2) + 1.0f);
		fConst109 = (0.0f - (589020.75f / (fConst3 * fConst108)));
		fConst110 = (0.0f - (((589020.75f / fConst2) + 1470.44653f) / (fConst2 * fConst108)));
		fConst111 = ((((185078.125f / fConst2) + 534.061584f) / fConst2) + 1.0f);
		fConst112 = (1.0f / (fConst108 * fConst111));
		fConst113 = (0.0f - (740312.5f / (fConst3 * fConst111)));
		fConst114 = (0.0f - (((740312.5f / fConst2) + 1068.12317f) / (fConst2 * fConst111)));
		fConst115 = ((294.751465f / fConst2) + 1.0f);
		fConst116 = (0.0f - (589.50293f / (fConst2 * fConst115)));
		fConst117 = ((((104066.891f / fConst2) + 466.819427f) / fConst2) + 1.0f);
		fConst118 = (1.0f / (fConst115 * fConst117));
		fConst119 = (0.0f - (416267.562f / (fConst3 * fConst117)));
		fConst120 = (0.0f - (((416267.562f / fConst2) + 933.638855f) / (fConst2 * fConst117)));
		fConst121 = ((126.928482f / fConst2) + 1.0f);
		fConst122 = (1.0f / fConst121);
		fConst123 = (0.0f - (253.856964f / (fConst2 * fConst121)));
		fConst124 = ((((48332.5195f / fConst2) + 380.785461f) / fConst2) + 1.0f);
		fConst125 = (1.0f / fConst124);
		fConst126 = (0.0f - (193330.078f / (fConst3 * fConst124)));
		fConst127 = (0.0f - (((193330.078f / fConst2) + 761.570923f) / (fConst2 * fConst124)));
		iConst128 = int(((0.000547762203f * float(iConst0)) + 0.5f));
		fConst129 = ((((159026.469f / fConst2) + 764.044434f) / fConst2) + 1.0f);
		fConst130 = (0.0f - (636105.875f / (fConst3 * fConst129)));
		fConst131 = (0.0f - (((636105.875f / fConst2) + 1528.08887f) / (fConst2 * fConst129)));
		fConst132 = ((((199872.875f / fConst2) + 554.997131f) / fConst2) + 1.0f);
		fConst133 = (1.0f / (fConst129 * fConst132));
		fConst134 = (0.0f - (799491.5f / (fConst3 * fConst132)));
		fConst135 = (0.0f - (((799491.5f / fConst2) + 1109.99426f) / (fConst2 * fConst132)));
		fConst136 = ((306.305908f / fConst2) + 1.0f);
		fConst137 = (0.0f - (612.611816f / (fConst2 * fConst136)));
		fConst138 = ((((112385.781f / fConst2) + 485.119049f) / fConst2) + 1.0f);
		fConst139 = (1.0f / (fConst136 * fConst138));
		fConst140 = (0.0f - (449543.125f / (fConst3 * fConst138)));
		fConst141 = (0.0f - (((449543.125f / fConst2) + 970.238098f) / (fConst2 * fConst138)));
		fConst142 = ((131.90416f / fConst2) + 1.0f);
		fConst143 = (1.0f / fConst142);
		fConst144 = (0.0f - (263.808319f / (fConst2 * fConst142)));
		fConst145 = ((((52196.1211f / fConst2) + 395.712463f) / fConst2) + 1.0f);
		fConst146 = (1.0f / fConst145);
		fConst147 = (0.0f - (208784.484f / (fConst3 * fConst145)));
		fConst148 = (0.0f - (((208784.484f / fConst2) + 791.424927f) / (fConst2 * fConst145)));
		iConst149 = int(((0.000696357281f * float(iConst0)) + 0.5f));
		
	}
	
	virtual void instanceResetUserInterface() {
		fHslider0 = FAUSTFLOAT(-10.0f);
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider1 = FAUSTFLOAT(400.0f);
		fHslider2 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec1[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
			fRec2[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec3[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec16[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 3); l5 = (l5 + 1)) {
			fRec17[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 3); l6 = (l6 + 1)) {
			fRec18[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 3); l7 = (l7 + 1)) {
			fRec19[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec20[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 3); l9 = (l9 + 1)) {
			fRec21[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 3); l10 = (l10 + 1)) {
			fRec22[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 3); l11 = (l11 + 1)) {
			fRec23[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 3); l12 = (l12 + 1)) {
			fRec24[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 2); l13 = (l13 + 1)) {
			fRec15[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 2); l14 = (l14 + 1)) {
			fRec13[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 2); l15 = (l15 + 1)) {
			fRec12[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			fRec10[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec9[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 2); l18 = (l18 + 1)) {
			fRec7[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec6[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec4[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 3); l21 = (l21 + 1)) {
			fRec34[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 3); l22 = (l22 + 1)) {
			fRec35[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 3); l23 = (l23 + 1)) {
			fRec36[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 3); l24 = (l24 + 1)) {
			fRec37[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 3); l25 = (l25 + 1)) {
			fRec38[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 3); l26 = (l26 + 1)) {
			fRec39[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 3); l27 = (l27 + 1)) {
			fRec40[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 2); l28 = (l28 + 1)) {
			fRec33[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 2); l29 = (l29 + 1)) {
			fRec31[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 2); l30 = (l30 + 1)) {
			fRec30[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 2); l31 = (l31 + 1)) {
			fRec28[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 2); l32 = (l32 + 1)) {
			fRec27[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec25[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 3); l34 = (l34 + 1)) {
			fRec44[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 3); l35 = (l35 + 1)) {
			fRec45[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 3); l36 = (l36 + 1)) {
			fRec46[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec43[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec41[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 3); l39 = (l39 + 1)) {
			fRec53[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 3); l40 = (l40 + 1)) {
			fRec54[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 3); l41 = (l41 + 1)) {
			fRec55[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 3); l42 = (l42 + 1)) {
			fRec56[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 3); l43 = (l43 + 1)) {
			fRec57[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 2); l44 = (l44 + 1)) {
			fRec52[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 2); l45 = (l45 + 1)) {
			fRec50[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 2); l46 = (l46 + 1)) {
			fRec49[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 2); l47 = (l47 + 1)) {
			fRec47[l47] = 0.0f;
			
		}
		IOTA = 0;
		for (int l48 = 0; (l48 < 256); l48 = (l48 + 1)) {
			fVec0[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec69[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec67[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec66[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec64[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2); l53 = (l53 + 1)) {
			fRec63[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 2); l54 = (l54 + 1)) {
			fRec61[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 2); l55 = (l55 + 1)) {
			fRec60[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 2); l56 = (l56 + 1)) {
			fRec58[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 2); l57 = (l57 + 1)) {
			fRec78[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec76[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec75[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 2); l60 = (l60 + 1)) {
			fRec73[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 2); l61 = (l61 + 1)) {
			fRec72[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 2); l62 = (l62 + 1)) {
			fRec70[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 2); l63 = (l63 + 1)) {
			fRec81[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec79[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 2); l65 = (l65 + 1)) {
			fRec87[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec85[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 2); l67 = (l67 + 1)) {
			fRec84[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec82[l68] = 0.0f;
			
		}
		for (int l69 = 0; (l69 < 128); l69 = (l69 + 1)) {
			fVec1[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 2); l70 = (l70 + 1)) {
			fRec99[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec97[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec96[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 2); l73 = (l73 + 1)) {
			fRec94[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 2); l74 = (l74 + 1)) {
			fRec93[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec91[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec90[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec88[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec108[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec106[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec105[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec103[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec102[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 2); l83 = (l83 + 1)) {
			fRec100[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec111[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec109[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec117[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec115[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 2); l88 = (l88 + 1)) {
			fRec114[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec112[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 128); l90 = (l90 + 1)) {
			fVec2[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec129[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 2); l92 = (l92 + 1)) {
			fRec127[l92] = 0.0f;
			
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec126[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 2); l94 = (l94 + 1)) {
			fRec124[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 2); l95 = (l95 + 1)) {
			fRec123[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 2); l96 = (l96 + 1)) {
			fRec121[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec120[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 2); l98 = (l98 + 1)) {
			fRec118[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 2); l99 = (l99 + 1)) {
			fRec138[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 2); l100 = (l100 + 1)) {
			fRec136[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec135[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 2); l102 = (l102 + 1)) {
			fRec133[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 2); l103 = (l103 + 1)) {
			fRec132[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 2); l104 = (l104 + 1)) {
			fRec130[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fRec141[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 2); l106 = (l106 + 1)) {
			fRec139[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 2); l107 = (l107 + 1)) {
			fRec147[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 2); l108 = (l108 + 1)) {
			fRec145[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 2); l109 = (l109 + 1)) {
			fRec144[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec142[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 32); l111 = (l111 + 1)) {
			fVec3[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 2); l112 = (l112 + 1)) {
			fRec159[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 2); l113 = (l113 + 1)) {
			fRec157[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 2); l114 = (l114 + 1)) {
			fRec156[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 2); l115 = (l115 + 1)) {
			fRec154[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 2); l116 = (l116 + 1)) {
			fRec153[l116] = 0.0f;
			
		}
		for (int l117 = 0; (l117 < 2); l117 = (l117 + 1)) {
			fRec151[l117] = 0.0f;
			
		}
		for (int l118 = 0; (l118 < 2); l118 = (l118 + 1)) {
			fRec150[l118] = 0.0f;
			
		}
		for (int l119 = 0; (l119 < 2); l119 = (l119 + 1)) {
			fRec148[l119] = 0.0f;
			
		}
		for (int l120 = 0; (l120 < 2); l120 = (l120 + 1)) {
			fRec168[l120] = 0.0f;
			
		}
		for (int l121 = 0; (l121 < 2); l121 = (l121 + 1)) {
			fRec166[l121] = 0.0f;
			
		}
		for (int l122 = 0; (l122 < 2); l122 = (l122 + 1)) {
			fRec165[l122] = 0.0f;
			
		}
		for (int l123 = 0; (l123 < 2); l123 = (l123 + 1)) {
			fRec163[l123] = 0.0f;
			
		}
		for (int l124 = 0; (l124 < 2); l124 = (l124 + 1)) {
			fRec162[l124] = 0.0f;
			
		}
		for (int l125 = 0; (l125 < 2); l125 = (l125 + 1)) {
			fRec160[l125] = 0.0f;
			
		}
		for (int l126 = 0; (l126 < 2); l126 = (l126 + 1)) {
			fRec171[l126] = 0.0f;
			
		}
		for (int l127 = 0; (l127 < 2); l127 = (l127 + 1)) {
			fRec169[l127] = 0.0f;
			
		}
		for (int l128 = 0; (l128 < 2); l128 = (l128 + 1)) {
			fRec177[l128] = 0.0f;
			
		}
		for (int l129 = 0; (l129 < 2); l129 = (l129 + 1)) {
			fRec175[l129] = 0.0f;
			
		}
		for (int l130 = 0; (l130 < 2); l130 = (l130 + 1)) {
			fRec174[l130] = 0.0f;
			
		}
		for (int l131 = 0; (l131 < 2); l131 = (l131 + 1)) {
			fRec172[l131] = 0.0f;
			
		}
		for (int l132 = 0; (l132 < 2); l132 = (l132 + 1)) {
			fRec189[l132] = 0.0f;
			
		}
		for (int l133 = 0; (l133 < 2); l133 = (l133 + 1)) {
			fRec187[l133] = 0.0f;
			
		}
		for (int l134 = 0; (l134 < 2); l134 = (l134 + 1)) {
			fRec186[l134] = 0.0f;
			
		}
		for (int l135 = 0; (l135 < 2); l135 = (l135 + 1)) {
			fRec184[l135] = 0.0f;
			
		}
		for (int l136 = 0; (l136 < 2); l136 = (l136 + 1)) {
			fRec183[l136] = 0.0f;
			
		}
		for (int l137 = 0; (l137 < 2); l137 = (l137 + 1)) {
			fRec181[l137] = 0.0f;
			
		}
		for (int l138 = 0; (l138 < 2); l138 = (l138 + 1)) {
			fRec180[l138] = 0.0f;
			
		}
		for (int l139 = 0; (l139 < 2); l139 = (l139 + 1)) {
			fRec178[l139] = 0.0f;
			
		}
		for (int l140 = 0; (l140 < 2); l140 = (l140 + 1)) {
			fRec198[l140] = 0.0f;
			
		}
		for (int l141 = 0; (l141 < 2); l141 = (l141 + 1)) {
			fRec196[l141] = 0.0f;
			
		}
		for (int l142 = 0; (l142 < 2); l142 = (l142 + 1)) {
			fRec195[l142] = 0.0f;
			
		}
		for (int l143 = 0; (l143 < 2); l143 = (l143 + 1)) {
			fRec193[l143] = 0.0f;
			
		}
		for (int l144 = 0; (l144 < 2); l144 = (l144 + 1)) {
			fRec192[l144] = 0.0f;
			
		}
		for (int l145 = 0; (l145 < 2); l145 = (l145 + 1)) {
			fRec190[l145] = 0.0f;
			
		}
		for (int l146 = 0; (l146 < 2); l146 = (l146 + 1)) {
			fRec201[l146] = 0.0f;
			
		}
		for (int l147 = 0; (l147 < 2); l147 = (l147 + 1)) {
			fRec199[l147] = 0.0f;
			
		}
		for (int l148 = 0; (l148 < 2); l148 = (l148 + 1)) {
			fRec207[l148] = 0.0f;
			
		}
		for (int l149 = 0; (l149 < 2); l149 = (l149 + 1)) {
			fRec205[l149] = 0.0f;
			
		}
		for (int l150 = 0; (l150 < 2); l150 = (l150 + 1)) {
			fRec204[l150] = 0.0f;
			
		}
		for (int l151 = 0; (l151 < 2); l151 = (l151 + 1)) {
			fRec202[l151] = 0.0f;
			
		}
		for (int l152 = 0; (l152 < 128); l152 = (l152 + 1)) {
			fVec4[l152] = 0.0f;
			
		}
		for (int l153 = 0; (l153 < 2); l153 = (l153 + 1)) {
			fRec219[l153] = 0.0f;
			
		}
		for (int l154 = 0; (l154 < 2); l154 = (l154 + 1)) {
			fRec217[l154] = 0.0f;
			
		}
		for (int l155 = 0; (l155 < 2); l155 = (l155 + 1)) {
			fRec216[l155] = 0.0f;
			
		}
		for (int l156 = 0; (l156 < 2); l156 = (l156 + 1)) {
			fRec214[l156] = 0.0f;
			
		}
		for (int l157 = 0; (l157 < 2); l157 = (l157 + 1)) {
			fRec213[l157] = 0.0f;
			
		}
		for (int l158 = 0; (l158 < 2); l158 = (l158 + 1)) {
			fRec211[l158] = 0.0f;
			
		}
		for (int l159 = 0; (l159 < 2); l159 = (l159 + 1)) {
			fRec210[l159] = 0.0f;
			
		}
		for (int l160 = 0; (l160 < 2); l160 = (l160 + 1)) {
			fRec208[l160] = 0.0f;
			
		}
		for (int l161 = 0; (l161 < 2); l161 = (l161 + 1)) {
			fRec228[l161] = 0.0f;
			
		}
		for (int l162 = 0; (l162 < 2); l162 = (l162 + 1)) {
			fRec226[l162] = 0.0f;
			
		}
		for (int l163 = 0; (l163 < 2); l163 = (l163 + 1)) {
			fRec225[l163] = 0.0f;
			
		}
		for (int l164 = 0; (l164 < 2); l164 = (l164 + 1)) {
			fRec223[l164] = 0.0f;
			
		}
		for (int l165 = 0; (l165 < 2); l165 = (l165 + 1)) {
			fRec222[l165] = 0.0f;
			
		}
		for (int l166 = 0; (l166 < 2); l166 = (l166 + 1)) {
			fRec220[l166] = 0.0f;
			
		}
		for (int l167 = 0; (l167 < 2); l167 = (l167 + 1)) {
			fRec231[l167] = 0.0f;
			
		}
		for (int l168 = 0; (l168 < 2); l168 = (l168 + 1)) {
			fRec229[l168] = 0.0f;
			
		}
		for (int l169 = 0; (l169 < 2); l169 = (l169 + 1)) {
			fRec237[l169] = 0.0f;
			
		}
		for (int l170 = 0; (l170 < 2); l170 = (l170 + 1)) {
			fRec235[l170] = 0.0f;
			
		}
		for (int l171 = 0; (l171 < 2); l171 = (l171 + 1)) {
			fRec234[l171] = 0.0f;
			
		}
		for (int l172 = 0; (l172 < 2); l172 = (l172 + 1)) {
			fRec232[l172] = 0.0f;
			
		}
		for (int l173 = 0; (l173 < 256); l173 = (l173 + 1)) {
			fVec5[l173] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("Haptic20004");
		ui_interface->declare(&fHslider0, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider0, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider2, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider1, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider1, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* input14 = inputs[14];
		FAUSTFLOAT* input15 = inputs[15];
		FAUSTFLOAT* input16 = inputs[16];
		FAUSTFLOAT* input17 = inputs[17];
		FAUSTFLOAT* input18 = inputs[18];
		FAUSTFLOAT* input19 = inputs[19];
		FAUSTFLOAT* input20 = inputs[20];
		FAUSTFLOAT* input21 = inputs[21];
		FAUSTFLOAT* input22 = inputs[22];
		FAUSTFLOAT* input23 = inputs[23];
		FAUSTFLOAT* input24 = inputs[24];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		float fSlow0 = (0.00100000005f * (powf(10.0f, (0.0500000007f * float(fHslider0))) * float((float(fCheckbox0) < 0.5f))));
		float fSlow1 = (0.00100000005f * float(fHslider1));
		float fSlow2 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider2))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			fRec1[0] = (fSlow1 + (0.999000013f * fRec1[1]));
			float fTemp0 = tanf((fConst1 * fRec1[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = ((fTemp0 * (fTemp0 + -2.0f)) + 1.0f);
			float fTemp3 = (fTemp1 + -1.0f);
			float fTemp4 = ((fTemp0 * (fTemp0 + 2.0f)) + 1.0f);
			fRec2[0] = (float(input0[i]) - (((fRec2[2] * fTemp2) + (2.0f * (fRec2[1] * fTemp3))) / fTemp4));
			fRec3[0] = (fSlow2 + (0.999000013f * fRec3[1]));
			float fTemp5 = (fRec3[0] * fTemp4);
			float fTemp6 = (0.0f - (2.0f / fTemp4));
			float fTemp7 = (((fTemp1 * (fRec2[2] + (fRec2[0] + (2.0f * fRec2[1])))) / fTemp5) + (fRec3[0] * (0.0f - ((fRec2[1] * fTemp6) + ((fRec2[0] + fRec2[2]) / fTemp4)))));
			float fTemp8 = (fConst5 * fRec4[1]);
			float fTemp9 = (fConst6 * fRec7[1]);
			fRec16[0] = (float(input16[i]) - (((fTemp2 * fRec16[2]) + (2.0f * (fTemp3 * fRec16[1]))) / fTemp4));
			float fTemp10 = (((fTemp1 * (fRec16[2] + (fRec16[0] + (2.0f * fRec16[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec16[1]) + ((fRec16[0] + fRec16[2]) / fTemp4))))));
			fRec17[0] = (float(input17[i]) - (((fTemp2 * fRec17[2]) + (2.0f * (fTemp3 * fRec17[1]))) / fTemp4));
			float fTemp11 = (((fTemp1 * (fRec17[2] + (fRec17[0] + (2.0f * fRec17[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec17[1]) + ((fRec17[0] + fRec17[2]) / fTemp4))))));
			fRec18[0] = (float(input18[i]) - (((fTemp2 * fRec18[2]) + (2.0f * (fTemp3 * fRec18[1]))) / fTemp4));
			float fTemp12 = (((fTemp1 * (fRec18[2] + (fRec18[0] + (2.0f * fRec18[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec18[1]) + ((fRec18[0] + fRec18[2]) / fTemp4))))));
			fRec19[0] = (float(input20[i]) - (((fTemp2 * fRec19[2]) + (2.0f * (fTemp3 * fRec19[1]))) / fTemp4));
			float fTemp13 = (((fTemp1 * (fRec19[2] + (fRec19[0] + (2.0f * fRec19[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec19[1]) + ((fRec19[0] + fRec19[2]) / fTemp4))))));
			fRec20[0] = (float(input21[i]) - (((fTemp2 * fRec20[2]) + (2.0f * (fTemp3 * fRec20[1]))) / fTemp4));
			float fTemp14 = (((fTemp1 * (fRec20[2] + (fRec20[0] + (2.0f * fRec20[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec20[1]) + ((fRec20[0] + fRec20[2]) / fTemp4))))));
			fRec21[0] = (float(input22[i]) - (((fTemp2 * fRec21[2]) + (2.0f * (fTemp3 * fRec21[1]))) / fTemp4));
			float fTemp15 = (((fTemp1 * (fRec21[2] + (fRec21[0] + (2.0f * fRec21[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec21[1]) + ((fRec21[0] + fRec21[2]) / fTemp4))))));
			fRec22[0] = (float(input23[i]) - (((fTemp2 * fRec22[2]) + (2.0f * (fTemp3 * fRec22[1]))) / fTemp4));
			float fTemp16 = (((fTemp1 * (fRec22[2] + (fRec22[0] + (2.0f * fRec22[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec22[1]) + ((fRec22[0] + fRec22[2]) / fTemp4))))));
			fRec23[0] = (float(input24[i]) - (((fTemp2 * fRec23[2]) + (2.0f * (fTemp3 * fRec23[1]))) / fTemp4));
			float fTemp17 = (((fTemp1 * (fRec23[2] + (fRec23[0] + (2.0f * fRec23[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec23[1]) + ((fRec23[0] + fRec23[2]) / fTemp4))))));
			fRec24[0] = (float(input19[i]) - (((fTemp2 * fRec24[2]) + (2.0f * (fTemp3 * fRec24[1]))) / fTemp4));
			float fTemp18 = (((fTemp1 * (fRec24[2] + (fRec24[0] + (2.0f * fRec24[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec24[1]) + ((fRec24[0] + fRec24[2]) / fTemp4))))));
			float fTemp19 = (fConst8 * (((((((((0.000452356093f * fTemp10) + (0.00179918972f * fTemp11)) + (0.00120926928f * fTemp12)) + (0.0688572228f * fTemp13)) + (0.0147536509f * fTemp14)) + (0.000720839889f * fTemp15)) + (2.27966993e-05f * fTemp16)) + (0.000527242606f * fTemp17)) - (0.0137249175f * fTemp18)));
			float fTemp20 = (fConst9 * fRec10[1]);
			float fTemp21 = (fConst10 * fRec13[1]);
			fRec15[0] = (fTemp19 + (fTemp20 + (fRec15[1] + fTemp21)));
			fRec13[0] = fRec15[0];
			float fRec14 = ((fTemp21 + fTemp20) + fTemp19);
			fRec12[0] = (fRec13[0] + fRec12[1]);
			fRec10[0] = fRec12[0];
			float fRec11 = fRec14;
			fRec9[0] = (fTemp8 + (fTemp9 + (fRec11 + fRec9[1])));
			fRec7[0] = fRec9[0];
			float fRec8 = (fTemp8 + (fRec11 + fTemp9));
			fRec6[0] = (fRec7[0] + fRec6[1]);
			fRec4[0] = fRec6[0];
			float fRec5 = fRec8;
			float fTemp22 = (fConst12 * fRec25[1]);
			fRec34[0] = (float(input9[i]) - (((fTemp2 * fRec34[2]) + (2.0f * (fTemp3 * fRec34[1]))) / fTemp4));
			float fTemp23 = (((fTemp1 * (fRec34[2] + (fRec34[0] + (2.0f * fRec34[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fTemp6 * fRec34[1]) + ((fRec34[0] + fRec34[2]) / fTemp4))))));
			fRec35[0] = (float(input10[i]) - (((fTemp2 * fRec35[2]) + (2.0f * (fTemp3 * fRec35[1]))) / fTemp4));
			float fTemp24 = (((fTemp1 * (fRec35[2] + (fRec35[0] + (2.0f * fRec35[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fTemp6 * fRec35[1]) + ((fRec35[0] + fRec35[2]) / fTemp4))))));
			fRec36[0] = (float(input12[i]) - (((fTemp2 * fRec36[2]) + (2.0f * (fTemp3 * fRec36[1]))) / fTemp4));
			float fTemp25 = (((fTemp1 * (fRec36[2] + (fRec36[0] + (2.0f * fRec36[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fTemp6 * fRec36[1]) + ((fRec36[0] + fRec36[2]) / fTemp4))))));
			fRec37[0] = (float(input13[i]) - (((fTemp2 * fRec37[2]) + (2.0f * (fTemp3 * fRec37[1]))) / fTemp4));
			float fTemp26 = (((fTemp1 * (fRec37[2] + (fRec37[0] + (2.0f * fRec37[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fTemp6 * fRec37[1]) + ((fRec37[0] + fRec37[2]) / fTemp4))))));
			fRec38[0] = (float(input14[i]) - (((fTemp2 * fRec38[2]) + (2.0f * (fTemp3 * fRec38[1]))) / fTemp4));
			float fTemp27 = (((fTemp1 * (fRec38[2] + (fRec38[0] + (2.0f * fRec38[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fTemp6 * fRec38[1]) + ((fRec38[0] + fRec38[2]) / fTemp4))))));
			fRec39[0] = (float(input15[i]) - (((fTemp2 * fRec39[2]) + (2.0f * (fTemp3 * fRec39[1]))) / fTemp4));
			float fTemp28 = (((fTemp1 * (fRec39[2] + (fRec39[0] + (2.0f * fRec39[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fTemp6 * fRec39[1]) + ((fRec39[0] + fRec39[2]) / fTemp4))))));
			fRec40[0] = (float(input11[i]) - (((fTemp2 * fRec40[2]) + (2.0f * (fTemp3 * fRec40[1]))) / fTemp4));
			float fTemp29 = (((fTemp1 * (fRec40[2] + (fRec40[0] + (2.0f * fRec40[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fTemp6 * fRec40[1]) + ((fRec40[0] + fRec40[2]) / fTemp4))))));
			float fTemp30 = (fConst14 * (((((((0.000781867187f * fTemp23) + (0.000857524981f * fTemp24)) + (0.0959466398f * fTemp25)) + (0.0149955805f * fTemp26)) + (0.000476720888f * fTemp27)) + (1.64839003e-05f * fTemp28)) - (0.0132319108f * fTemp29)));
			float fTemp31 = (fConst15 * fRec28[1]);
			float fTemp32 = (fConst16 * fRec31[1]);
			fRec33[0] = (fTemp30 + (fTemp31 + (fRec33[1] + fTemp32)));
			fRec31[0] = fRec33[0];
			float fRec32 = ((fTemp32 + fTemp31) + fTemp30);
			fRec30[0] = (fRec31[0] + fRec30[1]);
			fRec28[0] = fRec30[0];
			float fRec29 = fRec32;
			fRec27[0] = (fTemp22 + (fRec29 + fRec27[1]));
			fRec25[0] = fRec27[0];
			float fRec26 = (fRec29 + fTemp22);
			fRec44[0] = (float(input2[i]) - (((fTemp2 * fRec44[2]) + (2.0f * (fTemp3 * fRec44[1]))) / fTemp4));
			float fTemp33 = (((fTemp1 * (fRec44[2] + (fRec44[0] + (2.0f * fRec44[1])))) / fTemp5) + (0.906179845f * (fRec3[0] * (0.0f - ((fTemp6 * fRec44[1]) + ((fRec44[0] + fRec44[2]) / fTemp4))))));
			fRec45[0] = (float(input3[i]) - (((fTemp2 * fRec45[2]) + (2.0f * (fTemp3 * fRec45[1]))) / fTemp4));
			float fTemp34 = (((fTemp1 * (fRec45[2] + (fRec45[0] + (2.0f * fRec45[1])))) / fTemp5) + (0.906179845f * (fRec3[0] * (0.0f - ((fTemp6 * fRec45[1]) + ((fRec45[0] + fRec45[2]) / fTemp4))))));
			fRec46[0] = (float(input1[i]) - (((fTemp2 * fRec46[2]) + (2.0f * (fTemp3 * fRec46[1]))) / fTemp4));
			float fTemp35 = (((fTemp1 * (fRec46[2] + (fRec46[0] + (2.0f * fRec46[1])))) / fTemp5) + (0.906179845f * (fRec3[0] * (0.0f - ((fTemp6 * fRec46[1]) + ((fRec46[0] + fRec46[2]) / fTemp4))))));
			float fTemp36 = (fConst18 * (((0.103525728f * fTemp33) + (0.00626889663f * fTemp34)) - (0.00526397908f * fTemp35)));
			float fTemp37 = (fConst19 * fRec41[1]);
			fRec43[0] = (fTemp36 + (fRec43[1] + fTemp37));
			fRec41[0] = fRec43[0];
			float fRec42 = (fTemp37 + fTemp36);
			fRec53[0] = (float(input4[i]) - (((fTemp2 * fRec53[2]) + (2.0f * (fTemp3 * fRec53[1]))) / fTemp4));
			float fTemp38 = (((fTemp1 * (fRec53[2] + (fRec53[0] + (2.0f * fRec53[1])))) / fTemp5) + (0.731742859f * (fRec3[0] * (0.0f - ((fTemp6 * fRec53[1]) + ((fRec53[0] + fRec53[2]) / fTemp4))))));
			fRec54[0] = (float(input6[i]) - (((fTemp2 * fRec54[2]) + (2.0f * (fTemp3 * fRec54[1]))) / fTemp4));
			float fTemp39 = (((fTemp1 * (fRec54[2] + (fRec54[0] + (2.0f * fRec54[1])))) / fTemp5) + (0.731742859f * (fRec3[0] * (0.0f - ((fTemp6 * fRec54[1]) + ((fRec54[0] + fRec54[2]) / fTemp4))))));
			fRec55[0] = (float(input7[i]) - (((fTemp2 * fRec55[2]) + (2.0f * (fTemp3 * fRec55[1]))) / fTemp4));
			float fTemp40 = (((fTemp1 * (fRec55[2] + (fRec55[0] + (2.0f * fRec55[1])))) / fTemp5) + (0.731742859f * (fRec3[0] * (0.0f - ((fTemp6 * fRec55[1]) + ((fRec55[0] + fRec55[2]) / fTemp4))))));
			fRec56[0] = (float(input8[i]) - (((fTemp2 * fRec56[2]) + (2.0f * (fTemp3 * fRec56[1]))) / fTemp4));
			float fTemp41 = (((fTemp1 * (fRec56[2] + (fRec56[0] + (2.0f * fRec56[1])))) / fTemp5) + (0.731742859f * (fRec3[0] * (0.0f - ((fTemp6 * fRec56[1]) + ((fRec56[0] + fRec56[2]) / fTemp4))))));
			fRec57[0] = (float(input5[i]) - (((fTemp2 * fRec57[2]) + (2.0f * (fTemp3 * fRec57[1]))) / fTemp4));
			float fTemp42 = (((fTemp1 * (fRec57[2] + (fRec57[0] + (2.0f * fRec57[1])))) / fTemp5) + (0.731742859f * (fRec3[0] * (0.0f - ((fTemp6 * fRec57[1]) + ((fRec57[0] + fRec57[2]) / fTemp4))))));
			float fTemp43 = (fConst21 * (((((0.000408570602f * fTemp38) + (0.110278629f * fTemp39)) + (0.0117853126f * fTemp40)) + (0.000216953005f * fTemp41)) - (0.0100772418f * fTemp42)));
			float fTemp44 = (fConst22 * fRec47[1]);
			float fTemp45 = (fConst23 * fRec50[1]);
			fRec52[0] = (fTemp43 + (fTemp44 + (fRec52[1] + fTemp45)));
			fRec50[0] = fRec52[0];
			float fRec51 = ((fTemp45 + fTemp44) + fTemp43);
			fRec49[0] = (fRec50[0] + fRec49[1]);
			fRec47[0] = fRec49[0];
			float fRec48 = fRec51;
			fVec0[(IOTA & 255)] = ((0.0655872375f * fTemp7) + (fRec5 + (fRec26 + (fRec42 + fRec48))));
			output0[i] = FAUSTFLOAT((0.775974035f * (fRec0[0] * fVec0[((IOTA - iConst24) & 255)])));
			float fTemp46 = (fConst26 * fRec58[1]);
			float fTemp47 = (fConst27 * fRec61[1]);
			float fTemp48 = (fConst29 * (((((0.0204185769f * fTemp10) + (0.0151314354f * fTemp18)) + (0.011729463f * fTemp16)) + (0.000503460586f * fTemp17)) - (((((0.00573151466f * fTemp11) + (0.0291441549f * fTemp12)) + (0.00676874258f * fTemp13)) + (0.00451927865f * fTemp14)) + (0.0313781276f * fTemp15))));
			float fTemp49 = (fConst30 * fRec64[1]);
			float fTemp50 = (fConst31 * fRec67[1]);
			fRec69[0] = (fTemp48 + (fTemp49 + (fRec69[1] + fTemp50)));
			fRec67[0] = fRec69[0];
			float fRec68 = ((fTemp50 + fTemp49) + fTemp48);
			fRec66[0] = (fRec67[0] + fRec66[1]);
			fRec64[0] = fRec66[0];
			float fRec65 = fRec68;
			fRec63[0] = (fTemp46 + (fTemp47 + (fRec65 + fRec63[1])));
			fRec61[0] = fRec63[0];
			float fRec62 = (fTemp46 + (fRec65 + fTemp47));
			fRec60[0] = (fRec61[0] + fRec60[1]);
			fRec58[0] = fRec60[0];
			float fRec59 = fRec62;
			float fTemp51 = (fConst33 * fRec70[1]);
			float fTemp52 = (fConst35 * ((((0.0374376029f * fTemp29) + (0.0020169632f * fTemp25)) + (0.0626121685f * fTemp28)) - ((((0.0284145605f * fTemp23) + (0.0197730698f * fTemp24)) + (0.0138366157f * fTemp26)) + (0.0218312722f * fTemp27))));
			float fTemp53 = (fConst36 * fRec73[1]);
			float fTemp54 = (fConst37 * fRec76[1]);
			fRec78[0] = (fTemp52 + (fTemp53 + (fRec78[1] + fTemp54)));
			fRec76[0] = fRec78[0];
			float fRec77 = ((fTemp54 + fTemp53) + fTemp52);
			fRec75[0] = (fRec76[0] + fRec75[1]);
			fRec73[0] = fRec75[0];
			float fRec74 = fRec77;
			fRec72[0] = (fTemp51 + (fRec74 + fRec72[1]));
			fRec70[0] = fRec72[0];
			float fRec71 = (fRec74 + fTemp51);
			float fTemp55 = (fConst39 * (((0.178464279f * fTemp35) + (0.0418821983f * fTemp33)) - (0.0710267648f * fTemp34)));
			float fTemp56 = (fConst40 * fRec79[1]);
			fRec81[0] = (fTemp55 + (fRec81[1] + fTemp56));
			fRec79[0] = fRec81[0];
			float fRec80 = (fTemp56 + fTemp55);
			float fTemp57 = (fConst42 * ((0.0446497388f * fTemp42) - ((((0.0914057717f * fTemp38) + (0.00601211097f * fTemp39)) + (0.0167624969f * fTemp40)) + (0.0960628092f * fTemp41))));
			float fTemp58 = (fConst43 * fRec82[1]);
			float fTemp59 = (fConst44 * fRec85[1]);
			fRec87[0] = (fTemp57 + (fTemp58 + (fRec87[1] + fTemp59)));
			fRec85[0] = fRec87[0];
			float fRec86 = ((fTemp59 + fTemp58) + fTemp57);
			fRec84[0] = (fRec85[0] + fRec84[1]);
			fRec82[0] = fRec84[0];
			float fRec83 = fRec86;
			fVec1[(IOTA & 127)] = ((0.154614002f * fTemp7) + (fRec59 + (fRec71 + (fRec80 + fRec83))));
			output1[i] = FAUSTFLOAT((0.855194807f * (fRec0[0] * fVec1[((IOTA - iConst45) & 127)])));
			float fTemp60 = (fConst47 * fRec88[1]);
			float fTemp61 = (fConst48 * fRec91[1]);
			float fTemp62 = (fConst50 * (((((0.00603558356f * fTemp11) + (0.0384779051f * fTemp12)) + (0.00469165808f * fTemp18)) + (0.00750146108f * fTemp14)) - (((((0.0146305803f * fTemp10) + (0.013191767f * fTemp13)) + (0.0107597187f * fTemp15)) + (0.014120413f * fTemp16)) + (0.0274924524f * fTemp17))));
			float fTemp63 = (fConst51 * fRec94[1]);
			float fTemp64 = (fConst52 * fRec97[1]);
			fRec99[0] = (fTemp62 + (fTemp63 + (fRec99[1] + fTemp64)));
			fRec97[0] = fRec99[0];
			float fRec98 = ((fTemp64 + fTemp63) + fTemp62);
			fRec96[0] = (fRec97[0] + fRec96[1]);
			fRec94[0] = fRec96[0];
			float fRec95 = fRec98;
			fRec93[0] = (fTemp60 + (fTemp61 + (fRec95 + fRec93[1])));
			fRec91[0] = fRec93[0];
			float fRec92 = (fTemp60 + (fRec95 + fTemp61));
			fRec90[0] = (fRec91[0] + fRec90[1]);
			fRec88[0] = fRec90[0];
			float fRec89 = fRec92;
			float fTemp65 = (fConst54 * fRec100[1]);
			float fTemp66 = (fConst56 * (((((0.0246870499f * fTemp23) + (0.0285582226f * fTemp24)) + (0.0237993617f * fTemp29)) + (0.0201961137f * fTemp26)) - (((0.00653513242f * fTemp25) + (0.00741897058f * fTemp27)) + (0.07245332f * fTemp28))));
			float fTemp67 = (fConst57 * fRec103[1]);
			float fTemp68 = (fConst58 * fRec106[1]);
			fRec108[0] = (fTemp66 + (fTemp67 + (fRec108[1] + fTemp68)));
			fRec106[0] = fRec108[0];
			float fRec107 = ((fTemp68 + fTemp67) + fTemp66);
			fRec105[0] = (fRec106[0] + fRec105[1]);
			fRec103[0] = fRec105[0];
			float fRec104 = fRec107;
			fRec102[0] = (fTemp65 + (fRec104 + fRec102[1]));
			fRec100[0] = fRec102[0];
			float fRec101 = (fRec104 + fTemp65);
			float fTemp69 = (fConst60 * (((0.146832123f * fTemp35) + (0.0330515839f * fTemp33)) + (0.105333939f * fTemp34)));
			float fTemp70 = (fConst61 * fRec109[1]);
			fRec111[0] = (fTemp69 + (fRec111[1] + fTemp70));
			fRec109[0] = fRec111[0];
			float fRec110 = (fTemp70 + fTemp69);
			float fTemp71 = (fConst63 * ((((0.125141412f * fTemp38) + (0.0335854292f * fTemp42)) + (0.0268673711f * fTemp40)) - ((0.0147995166f * fTemp39) + (0.0419319756f * fTemp41))));
			float fTemp72 = (fConst64 * fRec112[1]);
			float fTemp73 = (fConst65 * fRec115[1]);
			fRec117[0] = (fTemp71 + (fTemp72 + (fRec117[1] + fTemp73)));
			fRec115[0] = fRec117[0];
			float fRec116 = ((fTemp73 + fTemp72) + fTemp71);
			fRec114[0] = (fRec115[0] + fRec114[1]);
			fRec112[0] = fRec114[0];
			float fRec113 = fRec116;
			fVec2[(IOTA & 127)] = ((0.140189067f * fTemp7) + (fRec89 + (fRec101 + (fRec110 + fRec113))));
			output2[i] = FAUSTFLOAT((0.922727287f * (fRec0[0] * fVec2[((IOTA - iConst66) & 127)])));
			float fTemp74 = (fConst68 * fRec118[1]);
			float fTemp75 = (fConst69 * fRec121[1]);
			float fTemp76 = (fConst71 * (((((0.0073975292f * fTemp10) + (0.0336818695f * fTemp15)) + (0.00974866655f * fTemp16)) + (0.0143417958f * fTemp17)) - (((((0.00456190063f * fTemp11) + (0.0161853041f * fTemp12)) + (0.00107440085f * fTemp18)) + (0.0225187f * fTemp13)) + (0.00382086844f * fTemp14))));
			float fTemp77 = (fConst72 * fRec124[1]);
			float fTemp78 = (fConst73 * fRec127[1]);
			fRec129[0] = (fTemp76 + (fTemp77 + (fRec129[1] + fTemp78)));
			fRec127[0] = fRec129[0];
			float fRec128 = ((fTemp78 + fTemp77) + fTemp76);
			fRec126[0] = (fRec127[0] + fRec126[1]);
			fRec124[0] = fRec126[0];
			float fRec125 = fRec128;
			fRec123[0] = (fTemp74 + (fTemp75 + (fRec125 + fRec123[1])));
			fRec121[0] = fRec123[0];
			float fRec122 = (fTemp74 + (fRec125 + fTemp75));
			fRec120[0] = (fRec121[0] + fRec120[1]);
			fRec118[0] = fRec120[0];
			float fRec119 = fRec122;
			float fTemp79 = (fConst75 * fRec130[1]);
			float fTemp80 = (fConst77 * ((((0.0232535545f * fTemp26) + (0.0251043458f * fTemp27)) + (0.0517983586f * fTemp28)) - ((((0.0225795712f * fTemp23) + (0.0105120596f * fTemp24)) + (0.00682708621f * fTemp29)) + (0.0182650927f * fTemp25))));
			float fTemp81 = (fConst78 * fRec133[1]);
			float fTemp82 = (fConst79 * fRec136[1]);
			fRec138[0] = (fTemp80 + (fTemp81 + (fRec138[1] + fTemp82)));
			fRec136[0] = fRec138[0];
			float fRec137 = ((fTemp82 + fTemp81) + fTemp80);
			fRec135[0] = (fRec136[0] + fRec135[1]);
			fRec133[0] = fRec135[0];
			float fRec134 = fRec137;
			fRec132[0] = (fTemp79 + (fRec134 + fRec132[1]));
			fRec130[0] = fRec132[0];
			float fRec131 = (fRec134 + fTemp79);
			float fTemp83 = (fConst81 * (((0.0272701606f * fTemp33) + (0.204333559f * fTemp34)) - (0.0425000452f * fTemp35)));
			float fTemp84 = (fConst82 * fRec139[1]);
			fRec141[0] = (fTemp83 + (fRec141[1] + fTemp84));
			fRec139[0] = fRec141[0];
			float fRec140 = (fTemp84 + fTemp83);
			float fTemp85 = (fConst84 * (((0.0397926383f * fTemp40) + (0.124420598f * fTemp41)) - (((0.0492154211f * fTemp38) + (0.00888533425f * fTemp42)) + (0.0316267349f * fTemp39))));
			float fTemp86 = (fConst85 * fRec142[1]);
			float fTemp87 = (fConst86 * fRec145[1]);
			fRec147[0] = (fTemp85 + (fTemp86 + (fRec147[1] + fTemp87)));
			fRec145[0] = fRec147[0];
			float fRec146 = ((fTemp87 + fTemp86) + fTemp85);
			fRec144[0] = (fRec145[0] + fRec144[1]);
			fRec142[0] = fRec144[0];
			float fRec143 = fRec146;
			fVec3[(IOTA & 31)] = ((0.164402053f * fTemp7) + (fRec119 + (fRec131 + (fRec140 + fRec143))));
			output3[i] = FAUSTFLOAT((0.974025965f * (fRec0[0] * fVec3[((IOTA - iConst87) & 31)])));
			float fTemp88 = (fConst89 * fRec148[1]);
			float fTemp89 = (fConst90 * fRec151[1]);
			float fTemp90 = (fConst92 * (((0.0055433861f * fTemp11) + (0.00335547631f * fTemp18)) - (((((((0.00804608408f * fTemp10) + (0.0136376573f * fTemp12)) + (0.0260490403f * fTemp13)) + (0.000853162026f * fTemp14)) + (0.0324630663f * fTemp15)) + (0.00283613242f * fTemp16)) + (0.000433237088f * fTemp17))));
			float fTemp91 = (fConst93 * fRec154[1]);
			float fTemp92 = (fConst94 * fRec157[1]);
			fRec159[0] = (fTemp90 + (fTemp91 + (fRec159[1] + fTemp92)));
			fRec157[0] = fRec159[0];
			float fRec158 = ((fTemp92 + fTemp91) + fTemp90);
			fRec156[0] = (fRec157[0] + fRec156[1]);
			fRec154[0] = fRec156[0];
			float fRec155 = fRec158;
			fRec153[0] = (fTemp88 + (fTemp89 + (fRec155 + fRec153[1])));
			fRec151[0] = fRec153[0];
			float fRec152 = (fTemp88 + (fRec155 + fTemp89));
			fRec150[0] = (fRec151[0] + fRec150[1]);
			fRec148[0] = fRec150[0];
			float fRec149 = fRec152;
			float fTemp93 = (fConst96 * fRec160[1]);
			float fTemp94 = (fConst98 * (((0.0312141068f * fTemp23) + (0.00528814644f * fTemp26)) - (((((0.00958592724f * fTemp24) + (0.0265802797f * fTemp29)) + (0.0206528027f * fTemp25)) + (0.0236536935f * fTemp27)) + (0.0172249489f * fTemp28))));
			float fTemp95 = (fConst99 * fRec163[1]);
			float fTemp96 = (fConst100 * fRec166[1]);
			fRec168[0] = (fTemp94 + (fTemp95 + (fRec168[1] + fTemp96)));
			fRec166[0] = fRec168[0];
			float fRec167 = ((fTemp96 + fTemp95) + fTemp94);
			fRec165[0] = (fRec166[0] + fRec165[1]);
			fRec163[0] = fRec165[0];
			float fRec164 = fRec167;
			fRec162[0] = (fTemp93 + (fRec164 + fRec162[1]));
			fRec160[0] = fRec162[0];
			float fRec161 = (fRec164 + fTemp93);
			float fTemp97 = (fConst102 * (((0.0307844132f * fTemp33) + (0.0467489548f * fTemp34)) - (0.223771259f * fTemp35)));
			float fTemp98 = (fConst103 * fRec169[1]);
			fRec171[0] = (fTemp97 + (fRec171[1] + fTemp98));
			fRec169[0] = fRec171[0];
			float fRec170 = (fTemp98 + fTemp97);
			float fTemp99 = (fConst105 * ((0.00867377967f * fTemp40) - ((((0.0492974669f * fTemp38) + (0.0434436277f * fTemp42)) + (0.0355822034f * fTemp39)) + (0.116754435f * fTemp41))));
			float fTemp100 = (fConst106 * fRec172[1]);
			float fTemp101 = (fConst107 * fRec175[1]);
			fRec177[0] = (fTemp99 + (fTemp100 + (fRec177[1] + fTemp101)));
			fRec175[0] = fRec177[0];
			float fRec176 = ((fTemp101 + fTemp100) + fTemp99);
			fRec174[0] = (fRec175[0] + fRec174[1]);
			fRec172[0] = fRec174[0];
			float fRec173 = fRec176;
			output4[i] = FAUSTFLOAT((fRec0[0] * ((0.187742293f * fTemp7) + (fRec149 + (fRec161 + (fRec170 + fRec173))))));
			float fTemp102 = (fConst109 * fRec178[1]);
			float fTemp103 = (fConst110 * fRec181[1]);
			float fTemp104 = (fConst112 * (((((0.0238492079f * fTemp10) + (0.0393353142f * fTemp12)) + (0.00476471055f * fTemp15)) + (0.00751847215f * fTemp16)) - (((((0.0137709212f * fTemp11) + (0.0063038012f * fTemp18)) + (0.0147041222f * fTemp13)) + (0.00206293561f * fTemp14)) + (0.0265560169f * fTemp17))));
			float fTemp105 = (fConst113 * fRec184[1]);
			float fTemp106 = (fConst114 * fRec187[1]);
			fRec189[0] = (fTemp104 + (fTemp105 + (fRec189[1] + fTemp106)));
			fRec187[0] = fRec189[0];
			float fRec188 = ((fTemp106 + fTemp105) + fTemp104);
			fRec186[0] = (fRec187[0] + fRec186[1]);
			fRec184[0] = fRec186[0];
			float fRec185 = fRec188;
			fRec183[0] = (fTemp102 + (fTemp103 + (fRec185 + fRec183[1])));
			fRec181[0] = fRec183[0];
			float fRec182 = (fTemp102 + (fRec185 + fTemp103));
			fRec180[0] = (fRec181[0] + fRec180[1]);
			fRec178[0] = fRec180[0];
			float fRec179 = fRec182;
			float fTemp107 = (fConst116 * fRec190[1]);
			float fTemp108 = (fConst118 * ((((0.0278805941f * fTemp24) + (0.00353613659f * fTemp27)) + (0.0318443961f * fTemp28)) - ((((0.0715131387f * fTemp23) + (0.0205610227f * fTemp29)) + (0.00850819983f * fTemp25)) + (0.0199216101f * fTemp26))));
			float fTemp109 = (fConst119 * fRec193[1]);
			float fTemp110 = (fConst120 * fRec196[1]);
			fRec198[0] = (fTemp108 + (fTemp109 + (fRec198[1] + fTemp110)));
			fRec196[0] = fRec198[0];
			float fRec197 = ((fTemp110 + fTemp109) + fTemp108);
			fRec195[0] = (fRec196[0] + fRec195[1]);
			fRec193[0] = fRec195[0];
			float fRec194 = fRec197;
			fRec192[0] = (fTemp107 + (fRec194 + fRec192[1]));
			fRec190[0] = fRec192[0];
			float fRec191 = (fRec194 + fTemp107);
			float fTemp111 = (fConst122 * ((0.028236473f * fTemp33) - ((0.110709824f * fTemp35) + (0.131656095f * fTemp34))));
			float fTemp112 = (fConst123 * fRec199[1]);
			fRec201[0] = (fTemp111 + (fRec201[1] + fTemp112));
			fRec199[0] = fRec201[0];
			float fRec200 = (fTemp112 + fTemp111);
			float fTemp113 = (fConst125 * (((0.126431569f * fTemp38) + (0.0252631698f * fTemp41)) - (((0.0265602339f * fTemp42) + (0.0163341369f * fTemp39)) + (0.0283783656f * fTemp40))));
			float fTemp114 = (fConst126 * fRec202[1]);
			float fTemp115 = (fConst127 * fRec205[1]);
			fRec207[0] = (fTemp113 + (fTemp114 + (fRec207[1] + fTemp115)));
			fRec205[0] = fRec207[0];
			float fRec206 = ((fTemp115 + fTemp114) + fTemp113);
			fRec204[0] = (fRec205[0] + fRec204[1]);
			fRec202[0] = fRec204[0];
			float fRec203 = fRec206;
			fVec4[(IOTA & 127)] = ((0.131561235f * fTemp7) + (fRec179 + (fRec191 + (fRec200 + fRec203))));
			output5[i] = FAUSTFLOAT((0.877922058f * (fRec0[0] * fVec4[((IOTA - iConst128) & 127)])));
			float fTemp116 = (fConst130 * fRec208[1]);
			float fTemp117 = (fConst131 * fRec211[1]);
			float fTemp118 = (fConst133 * (((((0.0124552334f * fTemp11) + (0.000854959595f * fTemp18)) + (0.0339042544f * fTemp15)) + (0.0253450647f * fTemp17)) - (((((0.0419839211f * fTemp10) + (0.0200452134f * fTemp12)) + (0.00606057374f * fTemp13)) + (0.0126966843f * fTemp14)) + (0.0121027157f * fTemp16))));
			float fTemp119 = (fConst134 * fRec214[1]);
			float fTemp120 = (fConst135 * fRec217[1]);
			fRec219[0] = (fTemp118 + (fTemp119 + (fRec219[1] + fTemp120)));
			fRec217[0] = fRec219[0];
			float fRec218 = ((fTemp120 + fTemp119) + fTemp118);
			fRec216[0] = (fRec217[0] + fRec216[1]);
			fRec214[0] = fRec216[0];
			float fRec215 = fRec218;
			fRec213[0] = (fTemp116 + (fTemp117 + (fRec215 + fRec213[1])));
			fRec211[0] = fRec213[0];
			float fRec212 = (fTemp116 + (fRec215 + fTemp117));
			fRec210[0] = (fRec211[0] + fRec210[1]);
			fRec208[0] = fRec210[0];
			float fRec209 = fRec212;
			float fTemp121 = (fConst137 * fRec220[1]);
			float fTemp122 = (fConst139 * (((((0.0615413934f * fTemp23) + (0.00693626096f * fTemp29)) + (0.000571844517f * fTemp25)) + (0.0236705095f * fTemp27)) - (((0.0139523977f * fTemp24) + (0.0306160189f * fTemp26)) + (0.0557824783f * fTemp28))));
			float fTemp123 = (fConst140 * fRec223[1]);
			float fTemp124 = (fConst141 * fRec226[1]);
			fRec228[0] = (fTemp122 + (fTemp123 + (fRec228[1] + fTemp124)));
			fRec226[0] = fRec228[0];
			float fRec227 = ((fTemp124 + fTemp123) + fTemp122);
			fRec225[0] = (fRec226[0] + fRec225[1]);
			fRec223[0] = fRec225[0];
			float fRec224 = fRec227;
			fRec222[0] = (fTemp121 + (fRec224 + fRec222[1]));
			fRec220[0] = fRec222[0];
			float fRec221 = (fRec224 + fTemp121);
			float fTemp125 = (fConst143 * (((0.042532295f * fTemp35) + (0.0308437794f * fTemp33)) - (0.142909408f * fTemp34)));
			float fTemp126 = (fConst144 * fRec229[1]);
			fRec231[0] = (fTemp125 + (fRec231[1] + fTemp126));
			fRec229[0] = fRec231[0];
			float fRec230 = (fTemp126 + fTemp125);
			float fTemp127 = (fConst146 * (((0.00930338632f * fTemp42) + (0.0998487398f * fTemp41)) - (((0.0646818727f * fTemp38) + (0.00509015517f * fTemp39)) + (0.0364908017f * fTemp40))));
			float fTemp128 = (fConst147 * fRec232[1]);
			float fTemp129 = (fConst148 * fRec235[1]);
			fRec237[0] = (fTemp127 + (fTemp128 + (fRec237[1] + fTemp129)));
			fRec235[0] = fRec237[0];
			float fRec236 = ((fTemp129 + fTemp128) + fTemp127);
			fRec234[0] = (fRec235[0] + fRec234[1]);
			fRec232[0] = fRec234[0];
			float fRec233 = fRec236;
			fVec5[(IOTA & 255)] = ((0.114496559f * fTemp7) + (fRec209 + (fRec221 + (fRec230 + fRec233))));
			output6[i] = FAUSTFLOAT((0.844805181f * (fRec0[0] * fVec5[((IOTA - iConst149) & 255)])));
			fRec0[1] = fRec0[0];
			fRec1[1] = fRec1[0];
			fRec2[2] = fRec2[1];
			fRec2[1] = fRec2[0];
			fRec3[1] = fRec3[0];
			fRec16[2] = fRec16[1];
			fRec16[1] = fRec16[0];
			fRec17[2] = fRec17[1];
			fRec17[1] = fRec17[0];
			fRec18[2] = fRec18[1];
			fRec18[1] = fRec18[0];
			fRec19[2] = fRec19[1];
			fRec19[1] = fRec19[0];
			fRec20[2] = fRec20[1];
			fRec20[1] = fRec20[0];
			fRec21[2] = fRec21[1];
			fRec21[1] = fRec21[0];
			fRec22[2] = fRec22[1];
			fRec22[1] = fRec22[0];
			fRec23[2] = fRec23[1];
			fRec23[1] = fRec23[0];
			fRec24[2] = fRec24[1];
			fRec24[1] = fRec24[0];
			fRec15[1] = fRec15[0];
			fRec13[1] = fRec13[0];
			fRec12[1] = fRec12[0];
			fRec10[1] = fRec10[0];
			fRec9[1] = fRec9[0];
			fRec7[1] = fRec7[0];
			fRec6[1] = fRec6[0];
			fRec4[1] = fRec4[0];
			fRec34[2] = fRec34[1];
			fRec34[1] = fRec34[0];
			fRec35[2] = fRec35[1];
			fRec35[1] = fRec35[0];
			fRec36[2] = fRec36[1];
			fRec36[1] = fRec36[0];
			fRec37[2] = fRec37[1];
			fRec37[1] = fRec37[0];
			fRec38[2] = fRec38[1];
			fRec38[1] = fRec38[0];
			fRec39[2] = fRec39[1];
			fRec39[1] = fRec39[0];
			fRec40[2] = fRec40[1];
			fRec40[1] = fRec40[0];
			fRec33[1] = fRec33[0];
			fRec31[1] = fRec31[0];
			fRec30[1] = fRec30[0];
			fRec28[1] = fRec28[0];
			fRec27[1] = fRec27[0];
			fRec25[1] = fRec25[0];
			fRec44[2] = fRec44[1];
			fRec44[1] = fRec44[0];
			fRec45[2] = fRec45[1];
			fRec45[1] = fRec45[0];
			fRec46[2] = fRec46[1];
			fRec46[1] = fRec46[0];
			fRec43[1] = fRec43[0];
			fRec41[1] = fRec41[0];
			fRec53[2] = fRec53[1];
			fRec53[1] = fRec53[0];
			fRec54[2] = fRec54[1];
			fRec54[1] = fRec54[0];
			fRec55[2] = fRec55[1];
			fRec55[1] = fRec55[0];
			fRec56[2] = fRec56[1];
			fRec56[1] = fRec56[0];
			fRec57[2] = fRec57[1];
			fRec57[1] = fRec57[0];
			fRec52[1] = fRec52[0];
			fRec50[1] = fRec50[0];
			fRec49[1] = fRec49[0];
			fRec47[1] = fRec47[0];
			IOTA = (IOTA + 1);
			fRec69[1] = fRec69[0];
			fRec67[1] = fRec67[0];
			fRec66[1] = fRec66[0];
			fRec64[1] = fRec64[0];
			fRec63[1] = fRec63[0];
			fRec61[1] = fRec61[0];
			fRec60[1] = fRec60[0];
			fRec58[1] = fRec58[0];
			fRec78[1] = fRec78[0];
			fRec76[1] = fRec76[0];
			fRec75[1] = fRec75[0];
			fRec73[1] = fRec73[0];
			fRec72[1] = fRec72[0];
			fRec70[1] = fRec70[0];
			fRec81[1] = fRec81[0];
			fRec79[1] = fRec79[0];
			fRec87[1] = fRec87[0];
			fRec85[1] = fRec85[0];
			fRec84[1] = fRec84[0];
			fRec82[1] = fRec82[0];
			fRec99[1] = fRec99[0];
			fRec97[1] = fRec97[0];
			fRec96[1] = fRec96[0];
			fRec94[1] = fRec94[0];
			fRec93[1] = fRec93[0];
			fRec91[1] = fRec91[0];
			fRec90[1] = fRec90[0];
			fRec88[1] = fRec88[0];
			fRec108[1] = fRec108[0];
			fRec106[1] = fRec106[0];
			fRec105[1] = fRec105[0];
			fRec103[1] = fRec103[0];
			fRec102[1] = fRec102[0];
			fRec100[1] = fRec100[0];
			fRec111[1] = fRec111[0];
			fRec109[1] = fRec109[0];
			fRec117[1] = fRec117[0];
			fRec115[1] = fRec115[0];
			fRec114[1] = fRec114[0];
			fRec112[1] = fRec112[0];
			fRec129[1] = fRec129[0];
			fRec127[1] = fRec127[0];
			fRec126[1] = fRec126[0];
			fRec124[1] = fRec124[0];
			fRec123[1] = fRec123[0];
			fRec121[1] = fRec121[0];
			fRec120[1] = fRec120[0];
			fRec118[1] = fRec118[0];
			fRec138[1] = fRec138[0];
			fRec136[1] = fRec136[0];
			fRec135[1] = fRec135[0];
			fRec133[1] = fRec133[0];
			fRec132[1] = fRec132[0];
			fRec130[1] = fRec130[0];
			fRec141[1] = fRec141[0];
			fRec139[1] = fRec139[0];
			fRec147[1] = fRec147[0];
			fRec145[1] = fRec145[0];
			fRec144[1] = fRec144[0];
			fRec142[1] = fRec142[0];
			fRec159[1] = fRec159[0];
			fRec157[1] = fRec157[0];
			fRec156[1] = fRec156[0];
			fRec154[1] = fRec154[0];
			fRec153[1] = fRec153[0];
			fRec151[1] = fRec151[0];
			fRec150[1] = fRec150[0];
			fRec148[1] = fRec148[0];
			fRec168[1] = fRec168[0];
			fRec166[1] = fRec166[0];
			fRec165[1] = fRec165[0];
			fRec163[1] = fRec163[0];
			fRec162[1] = fRec162[0];
			fRec160[1] = fRec160[0];
			fRec171[1] = fRec171[0];
			fRec169[1] = fRec169[0];
			fRec177[1] = fRec177[0];
			fRec175[1] = fRec175[0];
			fRec174[1] = fRec174[0];
			fRec172[1] = fRec172[0];
			fRec189[1] = fRec189[0];
			fRec187[1] = fRec187[0];
			fRec186[1] = fRec186[0];
			fRec184[1] = fRec184[0];
			fRec183[1] = fRec183[0];
			fRec181[1] = fRec181[0];
			fRec180[1] = fRec180[0];
			fRec178[1] = fRec178[0];
			fRec198[1] = fRec198[0];
			fRec196[1] = fRec196[0];
			fRec195[1] = fRec195[0];
			fRec193[1] = fRec193[0];
			fRec192[1] = fRec192[0];
			fRec190[1] = fRec190[0];
			fRec201[1] = fRec201[0];
			fRec199[1] = fRec199[0];
			fRec207[1] = fRec207[0];
			fRec205[1] = fRec205[0];
			fRec204[1] = fRec204[0];
			fRec202[1] = fRec202[0];
			fRec219[1] = fRec219[0];
			fRec217[1] = fRec217[0];
			fRec216[1] = fRec216[0];
			fRec214[1] = fRec214[0];
			fRec213[1] = fRec213[0];
			fRec211[1] = fRec211[0];
			fRec210[1] = fRec210[0];
			fRec208[1] = fRec208[0];
			fRec228[1] = fRec228[0];
			fRec226[1] = fRec226[0];
			fRec225[1] = fRec225[0];
			fRec223[1] = fRec223[0];
			fRec222[1] = fRec222[0];
			fRec220[1] = fRec220[0];
			fRec231[1] = fRec231[0];
			fRec229[1] = fRec229[0];
			fRec237[1] = fRec237[0];
			fRec235[1] = fRec235[0];
			fRec234[1] = fRec234[0];
			fRec232[1] = fRec232[0];
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if defined(F2SC_DEBUG_MES)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if defined(F2SC_DEBUG_MES) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if defined(F2SC_DEBUG_MES)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // F2SC_DEBUG_MES
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
