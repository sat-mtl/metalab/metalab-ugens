/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "Haptic20005"
version: "1.2"
Code generated with Faust 2.5.23 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// The prefix is set to "Faust" in the faust2supercollider script, otherwise set empty
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX ""
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fHslider0;
	FAUSTFLOAT fCheckbox0;
	float fRec0[2];
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	FAUSTFLOAT fHslider1;
	float fRec1[2];
	float fRec2[3];
	FAUSTFLOAT fHslider2;
	float fRec3[2];
	float fConst2;
	float fConst3;
	float fConst4;
	float fConst5;
	float fConst6;
	float fConst7;
	float fConst8;
	float fConst9;
	float fConst10;
	float fRec19[3];
	float fRec20[3];
	float fRec21[3];
	float fRec22[3];
	float fRec23[3];
	float fRec24[3];
	float fRec25[3];
	float fRec26[3];
	float fRec27[3];
	float fRec28[3];
	float fRec29[3];
	float fConst11;
	float fConst12;
	float fRec18[2];
	float fRec16[2];
	float fRec15[2];
	float fRec13[2];
	float fRec12[2];
	float fRec10[2];
	float fRec9[2];
	float fRec7[2];
	float fRec6[2];
	float fRec4[2];
	float fConst13;
	float fConst14;
	float fConst15;
	float fConst16;
	float fConst17;
	float fRec42[3];
	float fRec43[3];
	float fRec44[3];
	float fRec45[3];
	float fRec46[3];
	float fRec47[3];
	float fRec48[3];
	float fRec49[3];
	float fRec50[3];
	float fConst18;
	float fConst19;
	float fRec41[2];
	float fRec39[2];
	float fRec38[2];
	float fRec36[2];
	float fRec35[2];
	float fRec33[2];
	float fRec32[2];
	float fRec30[2];
	float fConst20;
	float fConst21;
	float fConst22;
	float fConst23;
	float fRec60[3];
	float fRec61[3];
	float fRec62[3];
	float fRec63[3];
	float fRec64[3];
	float fRec65[3];
	float fRec66[3];
	float fConst24;
	float fConst25;
	float fRec59[2];
	float fRec57[2];
	float fRec56[2];
	float fRec54[2];
	float fRec53[2];
	float fRec51[2];
	float fConst26;
	float fConst27;
	float fRec70[3];
	float fRec71[3];
	float fRec72[3];
	float fConst28;
	float fRec69[2];
	float fRec67[2];
	float fConst29;
	float fConst30;
	float fRec79[3];
	float fRec80[3];
	float fRec81[3];
	float fRec82[3];
	float fRec83[3];
	float fConst31;
	float fConst32;
	float fRec78[2];
	float fRec76[2];
	float fRec75[2];
	float fRec73[2];
	int IOTA;
	float fVec0[256];
	int iConst33;
	float fConst34;
	float fConst35;
	float fConst36;
	float fConst37;
	float fConst38;
	float fConst39;
	float fConst40;
	float fConst41;
	float fConst42;
	float fRec98[2];
	float fRec96[2];
	float fRec95[2];
	float fRec93[2];
	float fRec92[2];
	float fRec90[2];
	float fRec89[2];
	float fRec87[2];
	float fRec86[2];
	float fRec84[2];
	float fConst43;
	float fConst44;
	float fConst45;
	float fConst46;
	float fConst47;
	float fConst48;
	float fConst49;
	float fRec110[2];
	float fRec108[2];
	float fRec107[2];
	float fRec105[2];
	float fRec104[2];
	float fRec102[2];
	float fRec101[2];
	float fRec99[2];
	float fConst50;
	float fConst51;
	float fConst52;
	float fConst53;
	float fConst54;
	float fConst55;
	float fRec119[2];
	float fRec117[2];
	float fRec116[2];
	float fRec114[2];
	float fRec113[2];
	float fRec111[2];
	float fConst56;
	float fConst57;
	float fConst58;
	float fRec122[2];
	float fRec120[2];
	float fConst59;
	float fConst60;
	float fConst61;
	float fConst62;
	float fRec128[2];
	float fRec126[2];
	float fRec125[2];
	float fRec123[2];
	float fVec1[128];
	int iConst63;
	float fConst64;
	float fConst65;
	float fConst66;
	float fConst67;
	float fConst68;
	float fConst69;
	float fConst70;
	float fConst71;
	float fConst72;
	float fRec143[2];
	float fRec141[2];
	float fRec140[2];
	float fRec138[2];
	float fRec137[2];
	float fRec135[2];
	float fRec134[2];
	float fRec132[2];
	float fRec131[2];
	float fRec129[2];
	float fConst73;
	float fConst74;
	float fConst75;
	float fConst76;
	float fConst77;
	float fConst78;
	float fConst79;
	float fRec155[2];
	float fRec153[2];
	float fRec152[2];
	float fRec150[2];
	float fRec149[2];
	float fRec147[2];
	float fRec146[2];
	float fRec144[2];
	float fConst80;
	float fConst81;
	float fConst82;
	float fConst83;
	float fConst84;
	float fConst85;
	float fRec164[2];
	float fRec162[2];
	float fRec161[2];
	float fRec159[2];
	float fRec158[2];
	float fRec156[2];
	float fConst86;
	float fConst87;
	float fConst88;
	float fRec167[2];
	float fRec165[2];
	float fConst89;
	float fConst90;
	float fConst91;
	float fConst92;
	float fRec173[2];
	float fRec171[2];
	float fRec170[2];
	float fRec168[2];
	float fVec2[128];
	int iConst93;
	float fConst94;
	float fConst95;
	float fConst96;
	float fConst97;
	float fConst98;
	float fConst99;
	float fConst100;
	float fConst101;
	float fConst102;
	float fRec188[2];
	float fRec186[2];
	float fRec185[2];
	float fRec183[2];
	float fRec182[2];
	float fRec180[2];
	float fRec179[2];
	float fRec177[2];
	float fRec176[2];
	float fRec174[2];
	float fConst103;
	float fConst104;
	float fConst105;
	float fConst106;
	float fConst107;
	float fConst108;
	float fConst109;
	float fRec200[2];
	float fRec198[2];
	float fRec197[2];
	float fRec195[2];
	float fRec194[2];
	float fRec192[2];
	float fRec191[2];
	float fRec189[2];
	float fConst110;
	float fConst111;
	float fConst112;
	float fConst113;
	float fConst114;
	float fConst115;
	float fRec209[2];
	float fRec207[2];
	float fRec206[2];
	float fRec204[2];
	float fRec203[2];
	float fRec201[2];
	float fConst116;
	float fConst117;
	float fConst118;
	float fRec212[2];
	float fRec210[2];
	float fConst119;
	float fConst120;
	float fConst121;
	float fConst122;
	float fRec218[2];
	float fRec216[2];
	float fRec215[2];
	float fRec213[2];
	float fVec3[32];
	int iConst123;
	float fConst124;
	float fConst125;
	float fConst126;
	float fConst127;
	float fConst128;
	float fConst129;
	float fConst130;
	float fConst131;
	float fConst132;
	float fRec233[2];
	float fRec231[2];
	float fRec230[2];
	float fRec228[2];
	float fRec227[2];
	float fRec225[2];
	float fRec224[2];
	float fRec222[2];
	float fRec221[2];
	float fRec219[2];
	float fConst133;
	float fConst134;
	float fConst135;
	float fConst136;
	float fConst137;
	float fConst138;
	float fConst139;
	float fRec245[2];
	float fRec243[2];
	float fRec242[2];
	float fRec240[2];
	float fRec239[2];
	float fRec237[2];
	float fRec236[2];
	float fRec234[2];
	float fConst140;
	float fConst141;
	float fConst142;
	float fConst143;
	float fConst144;
	float fConst145;
	float fRec254[2];
	float fRec252[2];
	float fRec251[2];
	float fRec249[2];
	float fRec248[2];
	float fRec246[2];
	float fConst146;
	float fConst147;
	float fConst148;
	float fRec257[2];
	float fRec255[2];
	float fConst149;
	float fConst150;
	float fConst151;
	float fConst152;
	float fRec263[2];
	float fRec261[2];
	float fRec260[2];
	float fRec258[2];
	float fConst153;
	float fConst154;
	float fConst155;
	float fConst156;
	float fConst157;
	float fConst158;
	float fConst159;
	float fConst160;
	float fConst161;
	float fRec278[2];
	float fRec276[2];
	float fRec275[2];
	float fRec273[2];
	float fRec272[2];
	float fRec270[2];
	float fRec269[2];
	float fRec267[2];
	float fRec266[2];
	float fRec264[2];
	float fConst162;
	float fConst163;
	float fConst164;
	float fConst165;
	float fConst166;
	float fConst167;
	float fConst168;
	float fRec290[2];
	float fRec288[2];
	float fRec287[2];
	float fRec285[2];
	float fRec284[2];
	float fRec282[2];
	float fRec281[2];
	float fRec279[2];
	float fConst169;
	float fConst170;
	float fConst171;
	float fConst172;
	float fConst173;
	float fConst174;
	float fRec299[2];
	float fRec297[2];
	float fRec296[2];
	float fRec294[2];
	float fRec293[2];
	float fRec291[2];
	float fConst175;
	float fConst176;
	float fConst177;
	float fRec302[2];
	float fRec300[2];
	float fConst178;
	float fConst179;
	float fConst180;
	float fConst181;
	float fRec308[2];
	float fRec306[2];
	float fRec305[2];
	float fRec303[2];
	float fVec4[128];
	int iConst182;
	float fConst183;
	float fConst184;
	float fConst185;
	float fConst186;
	float fConst187;
	float fConst188;
	float fConst189;
	float fConst190;
	float fConst191;
	float fRec323[2];
	float fRec321[2];
	float fRec320[2];
	float fRec318[2];
	float fRec317[2];
	float fRec315[2];
	float fRec314[2];
	float fRec312[2];
	float fRec311[2];
	float fRec309[2];
	float fConst192;
	float fConst193;
	float fConst194;
	float fConst195;
	float fConst196;
	float fConst197;
	float fConst198;
	float fRec335[2];
	float fRec333[2];
	float fRec332[2];
	float fRec330[2];
	float fRec329[2];
	float fRec327[2];
	float fRec326[2];
	float fRec324[2];
	float fConst199;
	float fConst200;
	float fConst201;
	float fConst202;
	float fConst203;
	float fConst204;
	float fRec344[2];
	float fRec342[2];
	float fRec341[2];
	float fRec339[2];
	float fRec338[2];
	float fRec336[2];
	float fConst205;
	float fConst206;
	float fConst207;
	float fRec347[2];
	float fRec345[2];
	float fConst208;
	float fConst209;
	float fConst210;
	float fConst211;
	float fRec353[2];
	float fRec351[2];
	float fRec350[2];
	float fRec348[2];
	float fVec5[256];
	int iConst212;
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("filename", "Haptic20005");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "Haptic20005");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 36;
		
	}
	virtual int getNumOutputs() {
		return 7;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			case 31: {
				rate = 1;
				break;
			}
			case 32: {
				rate = 1;
				break;
			}
			case 33: {
				rate = 1;
				break;
			}
			case 34: {
				rate = 1;
				break;
			}
			case 35: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = (3.14159274f / float(iConst0));
		fConst2 = float(iConst0);
		fConst3 = ((523.687866f / fConst2) + 1.0f);
		fConst4 = (0.0f - (1047.37573f / (fConst2 * fConst3)));
		fConst5 = mydsp_faustpower2_f(fConst2);
		fConst6 = ((((294330.469f / fConst2) + 962.71167f) / fConst2) + 1.0f);
		fConst7 = (0.0f - (1177321.88f / (fConst5 * fConst6)));
		fConst8 = (0.0f - (((1177321.88f / fConst2) + 1925.42334f) / (fConst2 * fConst6)));
		fConst9 = ((((374423.844f / fConst2) + 667.667114f) / fConst2) + 1.0f);
		fConst10 = (1.0f / ((fConst3 * fConst6) * fConst9));
		fConst11 = (0.0f - (1497695.38f / (fConst5 * fConst9)));
		fConst12 = (0.0f - (((1497695.38f / fConst2) + 1335.33423f) / (fConst2 * fConst9)));
		fConst13 = ((((188489.938f / fConst2) + 831.817444f) / fConst2) + 1.0f);
		fConst14 = (0.0f - (753959.75f / (fConst5 * fConst13)));
		fConst15 = (0.0f - (((753959.75f / fConst2) + 1663.63489f) / (fConst2 * fConst13)));
		fConst16 = ((((236904.141f / fConst2) + 604.22699f) / fConst2) + 1.0f);
		fConst17 = (1.0f / (fConst13 * fConst16));
		fConst18 = (0.0f - (947616.562f / (fConst5 * fConst16)));
		fConst19 = (0.0f - (((947616.562f / fConst2) + 1208.45398f) / (fConst2 * fConst16)));
		fConst20 = ((333.476135f / fConst2) + 1.0f);
		fConst21 = (0.0f - (666.952271f / (fConst2 * fConst20)));
		fConst22 = ((((133207.953f / fConst2) + 528.150513f) / fConst2) + 1.0f);
		fConst23 = (1.0f / (fConst20 * fConst22));
		fConst24 = (0.0f - (532831.812f / (fConst5 * fConst22)));
		fConst25 = (0.0f - (((532831.812f / fConst2) + 1056.30103f) / (fConst2 * fConst22)));
		fConst26 = ((143.604446f / fConst2) + 1.0f);
		fConst27 = (1.0f / fConst26);
		fConst28 = (0.0f - (287.208893f / (fConst2 * fConst26)));
		fConst29 = ((((61866.7109f / fConst2) + 430.813324f) / fConst2) + 1.0f);
		fConst30 = (1.0f / fConst29);
		fConst31 = (0.0f - (247466.844f / (fConst5 * fConst29)));
		fConst32 = (0.0f - (((247466.844f / fConst2) + 861.626648f) / (fConst2 * fConst29)));
		iConst33 = int(((0.00100520195f * float(iConst0)) + 0.5f));
		fConst34 = ((475.176147f / fConst2) + 1.0f);
		fConst35 = (0.0f - (950.352295f / (fConst2 * fConst34)));
		fConst36 = ((((242325.703f / fConst2) + 873.531067f) / fConst2) + 1.0f);
		fConst37 = (0.0f - (969302.812f / (fConst5 * fConst36)));
		fConst38 = (0.0f - (((969302.812f / fConst2) + 1747.06213f) / (fConst2 * fConst36)));
		fConst39 = ((((308267.5f / fConst2) + 605.817932f) / fConst2) + 1.0f);
		fConst40 = (1.0f / ((fConst34 * fConst36) * fConst39));
		fConst41 = (0.0f - (1233070.0f / (fConst5 * fConst39)));
		fConst42 = (0.0f - (((1233070.0f / fConst2) + 1211.63586f) / (fConst2 * fConst39)));
		fConst43 = ((((155185.969f / fConst2) + 754.762207f) / fConst2) + 1.0f);
		fConst44 = (0.0f - (620743.875f / (fConst5 * fConst43)));
		fConst45 = (0.0f - (((620743.875f / fConst2) + 1509.52441f) / (fConst2 * fConst43)));
		fConst46 = ((((195045.938f / fConst2) + 548.254578f) / fConst2) + 1.0f);
		fConst47 = (1.0f / (fConst43 * fConst46));
		fConst48 = (0.0f - (780183.75f / (fConst5 * fConst46)));
		fConst49 = (0.0f - (((780183.75f / fConst2) + 1096.50916f) / (fConst2 * fConst46)));
		fConst50 = ((302.584656f / fConst2) + 1.0f);
		fConst51 = (0.0f - (605.169312f / (fConst2 * fConst50)));
		fConst52 = ((((109671.656f / fConst2) + 479.225433f) / fConst2) + 1.0f);
		fConst53 = (1.0f / (fConst50 * fConst52));
		fConst54 = (0.0f - (438686.625f / (fConst5 * fConst52)));
		fConst55 = (0.0f - (((438686.625f / fConst2) + 958.450867f) / (fConst2 * fConst52)));
		fConst56 = ((130.301682f / fConst2) + 1.0f);
		fConst57 = (1.0f / fConst56);
		fConst58 = (0.0f - (260.603363f / (fConst2 * fConst56)));
		fConst59 = ((((50935.582f / fConst2) + 390.905029f) / fConst2) + 1.0f);
		fConst60 = (1.0f / fConst59);
		fConst61 = (0.0f - (203742.328f / (fConst5 * fConst59)));
		fConst62 = (0.0f - (((203742.328f / fConst2) + 781.810059f) / (fConst2 * fConst59)));
		iConst63 = int(((0.000649739231f * float(iConst0)) + 0.5f));
		fConst64 = ((440.399017f / fConst2) + 1.0f);
		fConst65 = (0.0f - (880.798035f / (fConst2 * fConst64)));
		fConst66 = ((((208153.094f / fConst2) + 809.599182f) / fConst2) + 1.0f);
		fConst67 = (0.0f - (832612.375f / (fConst5 * fConst66)));
		fConst68 = (0.0f - (((832612.375f / fConst2) + 1619.19836f) / (fConst2 * fConst66)));
		fConst69 = ((((264795.812f / fConst2) + 561.47937f) / fConst2) + 1.0f);
		fConst70 = (1.0f / ((fConst64 * fConst66) * fConst69));
		fConst71 = (0.0f - (1059183.25f / (fConst5 * fConst69)));
		fConst72 = (0.0f - (((1059183.25f / fConst2) + 1122.95874f) / (fConst2 * fConst69)));
		fConst73 = ((((133301.75f / fConst2) + 699.522766f) / fConst2) + 1.0f);
		fConst74 = (0.0f - (533207.0f / (fConst5 * fConst73)));
		fConst75 = (0.0f - (((533207.0f / fConst2) + 1399.04553f) / (fConst2 * fConst73)));
		fConst76 = ((((167540.688f / fConst2) + 508.128967f) / fConst2) + 1.0f);
		fConst77 = (1.0f / (fConst73 * fConst76));
		fConst78 = (0.0f - (670162.75f / (fConst5 * fConst76)));
		fConst79 = (0.0f - (((670162.75f / fConst2) + 1016.25793f) / (fConst2 * fConst76)));
		fConst80 = ((280.439117f / fConst2) + 1.0f);
		fConst81 = (0.0f - (560.878235f / (fConst2 * fConst80)));
		fConst82 = ((((94205.8359f / fConst2) + 444.151917f) / fConst2) + 1.0f);
		fConst83 = (1.0f / (fConst80 * fConst82));
		fConst84 = (0.0f - (376823.344f / (fConst5 * fConst82)));
		fConst85 = (0.0f - (((376823.344f / fConst2) + 888.303833f) / (fConst2 * fConst82)));
		fConst86 = ((120.765175f / fConst2) + 1.0f);
		fConst87 = (1.0f / fConst86);
		fConst88 = (0.0f - (241.53035f / (fConst2 * fConst86)));
		fConst89 = ((((43752.6797f / fConst2) + 362.295532f) / fConst2) + 1.0f);
		fConst90 = (1.0f / fConst89);
		fConst91 = (0.0f - (175010.719f / (fConst5 * fConst89)));
		fConst92 = (0.0f - (((175010.719f / fConst2) + 724.591064f) / (fConst2 * fConst89)));
		iConst93 = int(((0.000346721819f * float(iConst0)) + 0.5f));
		fConst94 = ((417.204681f / fConst2) + 1.0f);
		fConst95 = (0.0f - (834.409363f / (fConst2 * fConst94)));
		fConst96 = ((((186805.016f / fConst2) + 766.960327f) / fConst2) + 1.0f);
		fConst97 = (0.0f - (747220.062f / (fConst5 * fConst96)));
		fConst98 = (0.0f - (((747220.062f / fConst2) + 1533.92065f) / (fConst2 * fConst96)));
		fConst99 = ((((237638.484f / fConst2) + 531.908142f) / fConst2) + 1.0f);
		fConst100 = (1.0f / ((fConst94 * fConst96) * fConst99));
		fConst101 = (0.0f - (950553.938f / (fConst5 * fConst99)));
		fConst102 = (0.0f - (((950553.938f / fConst2) + 1063.81628f) / (fConst2 * fConst99)));
		fConst103 = ((((119630.375f / fConst2) + 662.681213f) / fConst2) + 1.0f);
		fConst104 = (0.0f - (478521.5f / (fConst5 * fConst103)));
		fConst105 = (0.0f - (((478521.5f / fConst2) + 1325.36243f) / (fConst2 * fConst103)));
		fConst106 = ((((150357.797f / fConst2) + 481.367523f) / fConst2) + 1.0f);
		fConst107 = (1.0f / (fConst103 * fConst106));
		fConst108 = (0.0f - (601431.188f / (fConst5 * fConst106)));
		fConst109 = (0.0f - (((601431.188f / fConst2) + 962.735046f) / (fConst2 * fConst106)));
		fConst110 = ((265.669312f / fConst2) + 1.0f);
		fConst111 = (0.0f - (531.338623f / (fConst2 * fConst110)));
		fConst112 = ((((84544.125f / fConst2) + 420.759918f) / fConst2) + 1.0f);
		fConst113 = (1.0f / (fConst110 * fConst112));
		fConst114 = (0.0f - (338176.5f / (fConst5 * fConst112)));
		fConst115 = (0.0f - (((338176.5f / fConst2) + 841.519836f) / (fConst2 * fConst112)));
		fConst116 = ((114.404877f / fConst2) + 1.0f);
		fConst117 = (1.0f / fConst116);
		fConst118 = (0.0f - (228.809753f / (fConst2 * fConst116)));
		fConst119 = ((((39265.4258f / fConst2) + 343.21463f) / fConst2) + 1.0f);
		fConst120 = (1.0f / fConst119);
		fConst121 = (0.0f - (157061.703f / (fConst5 * fConst119)));
		fConst122 = (0.0f - (((157061.703f / fConst2) + 686.42926f) / (fConst2 * fConst119)));
		iConst123 = int(((0.000116545154f * float(iConst0)) + 0.5f));
		fConst124 = ((406.368195f / fConst2) + 1.0f);
		fConst125 = (0.0f - (812.736389f / (fConst2 * fConst124)));
		fConst126 = ((((177226.875f / fConst2) + 747.039246f) / fConst2) + 1.0f);
		fConst127 = (0.0f - (708907.5f / (fConst5 * fConst126)));
		fConst128 = (0.0f - (((708907.5f / fConst2) + 1494.07849f) / (fConst2 * fConst126)));
		fConst129 = ((((225453.953f / fConst2) + 518.092346f) / fConst2) + 1.0f);
		fConst130 = (1.0f / ((fConst124 * fConst126) * fConst129));
		fConst131 = (0.0f - (901815.812f / (fConst5 * fConst129)));
		fConst132 = (0.0f - (((901815.812f / fConst2) + 1036.18469f) / (fConst2 * fConst129)));
		fConst133 = ((((113496.523f / fConst2) + 645.46875f) / fConst2) + 1.0f);
		fConst134 = (0.0f - (453986.094f / (fConst5 * fConst133)));
		fConst135 = (0.0f - (((453986.094f / fConst2) + 1290.9375f) / (fConst2 * fConst133)));
		fConst136 = ((((142648.438f / fConst2) + 468.864471f) / fConst2) + 1.0f);
		fConst137 = (1.0f / (fConst133 * fConst136));
		fConst138 = (0.0f - (570593.75f / (fConst5 * fConst136)));
		fConst139 = (0.0f - (((570593.75f / fConst2) + 937.728943f) / (fConst2 * fConst136)));
		fConst140 = ((258.768829f / fConst2) + 1.0f);
		fConst141 = (0.0f - (517.537659f / (fConst2 * fConst140)));
		fConst142 = ((((80209.2578f / fConst2) + 409.831085f) / fConst2) + 1.0f);
		fConst143 = (1.0f / (fConst140 * fConst142));
		fConst144 = (0.0f - (320837.031f / (fConst5 * fConst142)));
		fConst145 = (0.0f - (((320837.031f / fConst2) + 819.66217f) / (fConst2 * fConst142)));
		fConst146 = ((111.433319f / fConst2) + 1.0f);
		fConst147 = (1.0f / fConst146);
		fConst148 = (0.0f - (222.866638f / (fConst2 * fConst146)));
		fConst149 = ((((37252.1523f / fConst2) + 334.299957f) / fConst2) + 1.0f);
		fConst150 = (1.0f / fConst149);
		fConst151 = (0.0f - (149008.609f / (fConst5 * fConst149)));
		fConst152 = (0.0f - (((149008.609f / fConst2) + 668.599915f) / (fConst2 * fConst149)));
		fConst153 = ((462.875f / fConst2) + 1.0f);
		fConst154 = (0.0f - (925.75f / (fConst2 * fConst153)));
		fConst155 = ((((229941.656f / fConst2) + 850.91748f) / fConst2) + 1.0f);
		fConst156 = (0.0f - (919766.625f / (fConst5 * fConst155)));
		fConst157 = (0.0f - (((919766.625f / fConst2) + 1701.83496f) / (fConst2 * fConst155)));
		fConst158 = ((((292513.5f / fConst2) + 590.134766f) / fConst2) + 1.0f);
		fConst159 = (1.0f / ((fConst153 * fConst155) * fConst158));
		fConst160 = (0.0f - (1170054.0f / (fConst5 * fConst158)));
		fConst161 = (0.0f - (((1170054.0f / fConst2) + 1180.26953f) / (fConst2 * fConst158)));
		fConst162 = ((((147255.188f / fConst2) + 735.223267f) / fConst2) + 1.0f);
		fConst163 = (0.0f - (589020.75f / (fConst5 * fConst162)));
		fConst164 = (0.0f - (((589020.75f / fConst2) + 1470.44653f) / (fConst2 * fConst162)));
		fConst165 = ((((185078.125f / fConst2) + 534.061584f) / fConst2) + 1.0f);
		fConst166 = (1.0f / (fConst162 * fConst165));
		fConst167 = (0.0f - (740312.5f / (fConst5 * fConst165)));
		fConst168 = (0.0f - (((740312.5f / fConst2) + 1068.12317f) / (fConst2 * fConst165)));
		fConst169 = ((294.751465f / fConst2) + 1.0f);
		fConst170 = (0.0f - (589.50293f / (fConst2 * fConst169)));
		fConst171 = ((((104066.891f / fConst2) + 466.819427f) / fConst2) + 1.0f);
		fConst172 = (1.0f / (fConst169 * fConst171));
		fConst173 = (0.0f - (416267.562f / (fConst5 * fConst171)));
		fConst174 = (0.0f - (((416267.562f / fConst2) + 933.638855f) / (fConst2 * fConst171)));
		fConst175 = ((126.928482f / fConst2) + 1.0f);
		fConst176 = (1.0f / fConst175);
		fConst177 = (0.0f - (253.856964f / (fConst2 * fConst175)));
		fConst178 = ((((48332.5195f / fConst2) + 380.785461f) / fConst2) + 1.0f);
		fConst179 = (1.0f / fConst178);
		fConst180 = (0.0f - (193330.078f / (fConst5 * fConst178)));
		fConst181 = (0.0f - (((193330.078f / fConst2) + 761.570923f) / (fConst2 * fConst178)));
		iConst182 = int(((0.000547762203f * float(iConst0)) + 0.5f));
		fConst183 = ((481.019989f / fConst2) + 1.0f);
		fConst184 = (0.0f - (962.039978f / (fConst2 * fConst183)));
		fConst185 = ((((248322.703f / fConst2) + 884.273987f) / fConst2) + 1.0f);
		fConst186 = (0.0f - (993290.812f / (fConst5 * fConst185)));
		fConst187 = (0.0f - (((993290.812f / fConst2) + 1768.54797f) / (fConst2 * fConst185)));
		fConst188 = ((((315896.406f / fConst2) + 613.268433f) / fConst2) + 1.0f);
		fConst189 = (1.0f / ((fConst183 * fConst185) * fConst188));
		fConst190 = (0.0f - (1263585.62f / (fConst5 * fConst188)));
		fConst191 = (0.0f - (((1263585.62f / fConst2) + 1226.53687f) / (fConst2 * fConst188)));
		fConst192 = ((((159026.469f / fConst2) + 764.044434f) / fConst2) + 1.0f);
		fConst193 = (0.0f - (636105.875f / (fConst5 * fConst192)));
		fConst194 = (0.0f - (((636105.875f / fConst2) + 1528.08887f) / (fConst2 * fConst192)));
		fConst195 = ((((199872.875f / fConst2) + 554.997131f) / fConst2) + 1.0f);
		fConst196 = (1.0f / (fConst192 * fConst195));
		fConst197 = (0.0f - (799491.5f / (fConst5 * fConst195)));
		fConst198 = (0.0f - (((799491.5f / fConst2) + 1109.99426f) / (fConst2 * fConst195)));
		fConst199 = ((306.305908f / fConst2) + 1.0f);
		fConst200 = (0.0f - (612.611816f / (fConst2 * fConst199)));
		fConst201 = ((((112385.781f / fConst2) + 485.119049f) / fConst2) + 1.0f);
		fConst202 = (1.0f / (fConst199 * fConst201));
		fConst203 = (0.0f - (449543.125f / (fConst5 * fConst201)));
		fConst204 = (0.0f - (((449543.125f / fConst2) + 970.238098f) / (fConst2 * fConst201)));
		fConst205 = ((131.90416f / fConst2) + 1.0f);
		fConst206 = (1.0f / fConst205);
		fConst207 = (0.0f - (263.808319f / (fConst2 * fConst205)));
		fConst208 = ((((52196.1211f / fConst2) + 395.712463f) / fConst2) + 1.0f);
		fConst209 = (1.0f / fConst208);
		fConst210 = (0.0f - (208784.484f / (fConst5 * fConst208)));
		fConst211 = (0.0f - (((208784.484f / fConst2) + 791.424927f) / (fConst2 * fConst208)));
		iConst212 = int(((0.000696357281f * float(iConst0)) + 0.5f));
		
	}
	
	virtual void instanceResetUserInterface() {
		fHslider0 = FAUSTFLOAT(-10.0f);
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider1 = FAUSTFLOAT(400.0f);
		fHslider2 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec1[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
			fRec2[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec3[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec19[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 3); l5 = (l5 + 1)) {
			fRec20[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 3); l6 = (l6 + 1)) {
			fRec21[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 3); l7 = (l7 + 1)) {
			fRec22[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec23[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 3); l9 = (l9 + 1)) {
			fRec24[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 3); l10 = (l10 + 1)) {
			fRec25[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 3); l11 = (l11 + 1)) {
			fRec26[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 3); l12 = (l12 + 1)) {
			fRec27[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 3); l13 = (l13 + 1)) {
			fRec28[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 3); l14 = (l14 + 1)) {
			fRec29[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 2); l15 = (l15 + 1)) {
			fRec18[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			fRec16[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec15[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 2); l18 = (l18 + 1)) {
			fRec13[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec12[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec10[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec9[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 2); l22 = (l22 + 1)) {
			fRec7[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 2); l23 = (l23 + 1)) {
			fRec6[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 2); l24 = (l24 + 1)) {
			fRec4[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 3); l25 = (l25 + 1)) {
			fRec42[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 3); l26 = (l26 + 1)) {
			fRec43[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 3); l27 = (l27 + 1)) {
			fRec44[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 3); l28 = (l28 + 1)) {
			fRec45[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 3); l29 = (l29 + 1)) {
			fRec46[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 3); l30 = (l30 + 1)) {
			fRec47[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 3); l31 = (l31 + 1)) {
			fRec48[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 3); l32 = (l32 + 1)) {
			fRec49[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 3); l33 = (l33 + 1)) {
			fRec50[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 2); l34 = (l34 + 1)) {
			fRec41[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec39[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 2); l36 = (l36 + 1)) {
			fRec38[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec36[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec35[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 2); l39 = (l39 + 1)) {
			fRec33[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 2); l40 = (l40 + 1)) {
			fRec32[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 2); l41 = (l41 + 1)) {
			fRec30[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 3); l42 = (l42 + 1)) {
			fRec60[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 3); l43 = (l43 + 1)) {
			fRec61[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 3); l44 = (l44 + 1)) {
			fRec62[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 3); l45 = (l45 + 1)) {
			fRec63[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 3); l46 = (l46 + 1)) {
			fRec64[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 3); l47 = (l47 + 1)) {
			fRec65[l47] = 0.0f;
			
		}
		for (int l48 = 0; (l48 < 3); l48 = (l48 + 1)) {
			fRec66[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec59[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec57[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec56[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec54[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2); l53 = (l53 + 1)) {
			fRec53[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 2); l54 = (l54 + 1)) {
			fRec51[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 3); l55 = (l55 + 1)) {
			fRec70[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 3); l56 = (l56 + 1)) {
			fRec71[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 3); l57 = (l57 + 1)) {
			fRec72[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec69[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec67[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 3); l60 = (l60 + 1)) {
			fRec79[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 3); l61 = (l61 + 1)) {
			fRec80[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 3); l62 = (l62 + 1)) {
			fRec81[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 3); l63 = (l63 + 1)) {
			fRec82[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 3); l64 = (l64 + 1)) {
			fRec83[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 2); l65 = (l65 + 1)) {
			fRec78[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec76[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 2); l67 = (l67 + 1)) {
			fRec75[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec73[l68] = 0.0f;
			
		}
		IOTA = 0;
		for (int l69 = 0; (l69 < 256); l69 = (l69 + 1)) {
			fVec0[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 2); l70 = (l70 + 1)) {
			fRec98[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec96[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec95[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 2); l73 = (l73 + 1)) {
			fRec93[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 2); l74 = (l74 + 1)) {
			fRec92[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec90[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec89[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec87[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec86[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec84[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec110[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec108[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec107[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 2); l83 = (l83 + 1)) {
			fRec105[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec104[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec102[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec101[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec99[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 2); l88 = (l88 + 1)) {
			fRec119[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec117[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 2); l90 = (l90 + 1)) {
			fRec116[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec114[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 2); l92 = (l92 + 1)) {
			fRec113[l92] = 0.0f;
			
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec111[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 2); l94 = (l94 + 1)) {
			fRec122[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 2); l95 = (l95 + 1)) {
			fRec120[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 2); l96 = (l96 + 1)) {
			fRec128[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec126[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 2); l98 = (l98 + 1)) {
			fRec125[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 2); l99 = (l99 + 1)) {
			fRec123[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 128); l100 = (l100 + 1)) {
			fVec1[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec143[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 2); l102 = (l102 + 1)) {
			fRec141[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 2); l103 = (l103 + 1)) {
			fRec140[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 2); l104 = (l104 + 1)) {
			fRec138[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fRec137[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 2); l106 = (l106 + 1)) {
			fRec135[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 2); l107 = (l107 + 1)) {
			fRec134[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 2); l108 = (l108 + 1)) {
			fRec132[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 2); l109 = (l109 + 1)) {
			fRec131[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec129[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 2); l111 = (l111 + 1)) {
			fRec155[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 2); l112 = (l112 + 1)) {
			fRec153[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 2); l113 = (l113 + 1)) {
			fRec152[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 2); l114 = (l114 + 1)) {
			fRec150[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 2); l115 = (l115 + 1)) {
			fRec149[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 2); l116 = (l116 + 1)) {
			fRec147[l116] = 0.0f;
			
		}
		for (int l117 = 0; (l117 < 2); l117 = (l117 + 1)) {
			fRec146[l117] = 0.0f;
			
		}
		for (int l118 = 0; (l118 < 2); l118 = (l118 + 1)) {
			fRec144[l118] = 0.0f;
			
		}
		for (int l119 = 0; (l119 < 2); l119 = (l119 + 1)) {
			fRec164[l119] = 0.0f;
			
		}
		for (int l120 = 0; (l120 < 2); l120 = (l120 + 1)) {
			fRec162[l120] = 0.0f;
			
		}
		for (int l121 = 0; (l121 < 2); l121 = (l121 + 1)) {
			fRec161[l121] = 0.0f;
			
		}
		for (int l122 = 0; (l122 < 2); l122 = (l122 + 1)) {
			fRec159[l122] = 0.0f;
			
		}
		for (int l123 = 0; (l123 < 2); l123 = (l123 + 1)) {
			fRec158[l123] = 0.0f;
			
		}
		for (int l124 = 0; (l124 < 2); l124 = (l124 + 1)) {
			fRec156[l124] = 0.0f;
			
		}
		for (int l125 = 0; (l125 < 2); l125 = (l125 + 1)) {
			fRec167[l125] = 0.0f;
			
		}
		for (int l126 = 0; (l126 < 2); l126 = (l126 + 1)) {
			fRec165[l126] = 0.0f;
			
		}
		for (int l127 = 0; (l127 < 2); l127 = (l127 + 1)) {
			fRec173[l127] = 0.0f;
			
		}
		for (int l128 = 0; (l128 < 2); l128 = (l128 + 1)) {
			fRec171[l128] = 0.0f;
			
		}
		for (int l129 = 0; (l129 < 2); l129 = (l129 + 1)) {
			fRec170[l129] = 0.0f;
			
		}
		for (int l130 = 0; (l130 < 2); l130 = (l130 + 1)) {
			fRec168[l130] = 0.0f;
			
		}
		for (int l131 = 0; (l131 < 128); l131 = (l131 + 1)) {
			fVec2[l131] = 0.0f;
			
		}
		for (int l132 = 0; (l132 < 2); l132 = (l132 + 1)) {
			fRec188[l132] = 0.0f;
			
		}
		for (int l133 = 0; (l133 < 2); l133 = (l133 + 1)) {
			fRec186[l133] = 0.0f;
			
		}
		for (int l134 = 0; (l134 < 2); l134 = (l134 + 1)) {
			fRec185[l134] = 0.0f;
			
		}
		for (int l135 = 0; (l135 < 2); l135 = (l135 + 1)) {
			fRec183[l135] = 0.0f;
			
		}
		for (int l136 = 0; (l136 < 2); l136 = (l136 + 1)) {
			fRec182[l136] = 0.0f;
			
		}
		for (int l137 = 0; (l137 < 2); l137 = (l137 + 1)) {
			fRec180[l137] = 0.0f;
			
		}
		for (int l138 = 0; (l138 < 2); l138 = (l138 + 1)) {
			fRec179[l138] = 0.0f;
			
		}
		for (int l139 = 0; (l139 < 2); l139 = (l139 + 1)) {
			fRec177[l139] = 0.0f;
			
		}
		for (int l140 = 0; (l140 < 2); l140 = (l140 + 1)) {
			fRec176[l140] = 0.0f;
			
		}
		for (int l141 = 0; (l141 < 2); l141 = (l141 + 1)) {
			fRec174[l141] = 0.0f;
			
		}
		for (int l142 = 0; (l142 < 2); l142 = (l142 + 1)) {
			fRec200[l142] = 0.0f;
			
		}
		for (int l143 = 0; (l143 < 2); l143 = (l143 + 1)) {
			fRec198[l143] = 0.0f;
			
		}
		for (int l144 = 0; (l144 < 2); l144 = (l144 + 1)) {
			fRec197[l144] = 0.0f;
			
		}
		for (int l145 = 0; (l145 < 2); l145 = (l145 + 1)) {
			fRec195[l145] = 0.0f;
			
		}
		for (int l146 = 0; (l146 < 2); l146 = (l146 + 1)) {
			fRec194[l146] = 0.0f;
			
		}
		for (int l147 = 0; (l147 < 2); l147 = (l147 + 1)) {
			fRec192[l147] = 0.0f;
			
		}
		for (int l148 = 0; (l148 < 2); l148 = (l148 + 1)) {
			fRec191[l148] = 0.0f;
			
		}
		for (int l149 = 0; (l149 < 2); l149 = (l149 + 1)) {
			fRec189[l149] = 0.0f;
			
		}
		for (int l150 = 0; (l150 < 2); l150 = (l150 + 1)) {
			fRec209[l150] = 0.0f;
			
		}
		for (int l151 = 0; (l151 < 2); l151 = (l151 + 1)) {
			fRec207[l151] = 0.0f;
			
		}
		for (int l152 = 0; (l152 < 2); l152 = (l152 + 1)) {
			fRec206[l152] = 0.0f;
			
		}
		for (int l153 = 0; (l153 < 2); l153 = (l153 + 1)) {
			fRec204[l153] = 0.0f;
			
		}
		for (int l154 = 0; (l154 < 2); l154 = (l154 + 1)) {
			fRec203[l154] = 0.0f;
			
		}
		for (int l155 = 0; (l155 < 2); l155 = (l155 + 1)) {
			fRec201[l155] = 0.0f;
			
		}
		for (int l156 = 0; (l156 < 2); l156 = (l156 + 1)) {
			fRec212[l156] = 0.0f;
			
		}
		for (int l157 = 0; (l157 < 2); l157 = (l157 + 1)) {
			fRec210[l157] = 0.0f;
			
		}
		for (int l158 = 0; (l158 < 2); l158 = (l158 + 1)) {
			fRec218[l158] = 0.0f;
			
		}
		for (int l159 = 0; (l159 < 2); l159 = (l159 + 1)) {
			fRec216[l159] = 0.0f;
			
		}
		for (int l160 = 0; (l160 < 2); l160 = (l160 + 1)) {
			fRec215[l160] = 0.0f;
			
		}
		for (int l161 = 0; (l161 < 2); l161 = (l161 + 1)) {
			fRec213[l161] = 0.0f;
			
		}
		for (int l162 = 0; (l162 < 32); l162 = (l162 + 1)) {
			fVec3[l162] = 0.0f;
			
		}
		for (int l163 = 0; (l163 < 2); l163 = (l163 + 1)) {
			fRec233[l163] = 0.0f;
			
		}
		for (int l164 = 0; (l164 < 2); l164 = (l164 + 1)) {
			fRec231[l164] = 0.0f;
			
		}
		for (int l165 = 0; (l165 < 2); l165 = (l165 + 1)) {
			fRec230[l165] = 0.0f;
			
		}
		for (int l166 = 0; (l166 < 2); l166 = (l166 + 1)) {
			fRec228[l166] = 0.0f;
			
		}
		for (int l167 = 0; (l167 < 2); l167 = (l167 + 1)) {
			fRec227[l167] = 0.0f;
			
		}
		for (int l168 = 0; (l168 < 2); l168 = (l168 + 1)) {
			fRec225[l168] = 0.0f;
			
		}
		for (int l169 = 0; (l169 < 2); l169 = (l169 + 1)) {
			fRec224[l169] = 0.0f;
			
		}
		for (int l170 = 0; (l170 < 2); l170 = (l170 + 1)) {
			fRec222[l170] = 0.0f;
			
		}
		for (int l171 = 0; (l171 < 2); l171 = (l171 + 1)) {
			fRec221[l171] = 0.0f;
			
		}
		for (int l172 = 0; (l172 < 2); l172 = (l172 + 1)) {
			fRec219[l172] = 0.0f;
			
		}
		for (int l173 = 0; (l173 < 2); l173 = (l173 + 1)) {
			fRec245[l173] = 0.0f;
			
		}
		for (int l174 = 0; (l174 < 2); l174 = (l174 + 1)) {
			fRec243[l174] = 0.0f;
			
		}
		for (int l175 = 0; (l175 < 2); l175 = (l175 + 1)) {
			fRec242[l175] = 0.0f;
			
		}
		for (int l176 = 0; (l176 < 2); l176 = (l176 + 1)) {
			fRec240[l176] = 0.0f;
			
		}
		for (int l177 = 0; (l177 < 2); l177 = (l177 + 1)) {
			fRec239[l177] = 0.0f;
			
		}
		for (int l178 = 0; (l178 < 2); l178 = (l178 + 1)) {
			fRec237[l178] = 0.0f;
			
		}
		for (int l179 = 0; (l179 < 2); l179 = (l179 + 1)) {
			fRec236[l179] = 0.0f;
			
		}
		for (int l180 = 0; (l180 < 2); l180 = (l180 + 1)) {
			fRec234[l180] = 0.0f;
			
		}
		for (int l181 = 0; (l181 < 2); l181 = (l181 + 1)) {
			fRec254[l181] = 0.0f;
			
		}
		for (int l182 = 0; (l182 < 2); l182 = (l182 + 1)) {
			fRec252[l182] = 0.0f;
			
		}
		for (int l183 = 0; (l183 < 2); l183 = (l183 + 1)) {
			fRec251[l183] = 0.0f;
			
		}
		for (int l184 = 0; (l184 < 2); l184 = (l184 + 1)) {
			fRec249[l184] = 0.0f;
			
		}
		for (int l185 = 0; (l185 < 2); l185 = (l185 + 1)) {
			fRec248[l185] = 0.0f;
			
		}
		for (int l186 = 0; (l186 < 2); l186 = (l186 + 1)) {
			fRec246[l186] = 0.0f;
			
		}
		for (int l187 = 0; (l187 < 2); l187 = (l187 + 1)) {
			fRec257[l187] = 0.0f;
			
		}
		for (int l188 = 0; (l188 < 2); l188 = (l188 + 1)) {
			fRec255[l188] = 0.0f;
			
		}
		for (int l189 = 0; (l189 < 2); l189 = (l189 + 1)) {
			fRec263[l189] = 0.0f;
			
		}
		for (int l190 = 0; (l190 < 2); l190 = (l190 + 1)) {
			fRec261[l190] = 0.0f;
			
		}
		for (int l191 = 0; (l191 < 2); l191 = (l191 + 1)) {
			fRec260[l191] = 0.0f;
			
		}
		for (int l192 = 0; (l192 < 2); l192 = (l192 + 1)) {
			fRec258[l192] = 0.0f;
			
		}
		for (int l193 = 0; (l193 < 2); l193 = (l193 + 1)) {
			fRec278[l193] = 0.0f;
			
		}
		for (int l194 = 0; (l194 < 2); l194 = (l194 + 1)) {
			fRec276[l194] = 0.0f;
			
		}
		for (int l195 = 0; (l195 < 2); l195 = (l195 + 1)) {
			fRec275[l195] = 0.0f;
			
		}
		for (int l196 = 0; (l196 < 2); l196 = (l196 + 1)) {
			fRec273[l196] = 0.0f;
			
		}
		for (int l197 = 0; (l197 < 2); l197 = (l197 + 1)) {
			fRec272[l197] = 0.0f;
			
		}
		for (int l198 = 0; (l198 < 2); l198 = (l198 + 1)) {
			fRec270[l198] = 0.0f;
			
		}
		for (int l199 = 0; (l199 < 2); l199 = (l199 + 1)) {
			fRec269[l199] = 0.0f;
			
		}
		for (int l200 = 0; (l200 < 2); l200 = (l200 + 1)) {
			fRec267[l200] = 0.0f;
			
		}
		for (int l201 = 0; (l201 < 2); l201 = (l201 + 1)) {
			fRec266[l201] = 0.0f;
			
		}
		for (int l202 = 0; (l202 < 2); l202 = (l202 + 1)) {
			fRec264[l202] = 0.0f;
			
		}
		for (int l203 = 0; (l203 < 2); l203 = (l203 + 1)) {
			fRec290[l203] = 0.0f;
			
		}
		for (int l204 = 0; (l204 < 2); l204 = (l204 + 1)) {
			fRec288[l204] = 0.0f;
			
		}
		for (int l205 = 0; (l205 < 2); l205 = (l205 + 1)) {
			fRec287[l205] = 0.0f;
			
		}
		for (int l206 = 0; (l206 < 2); l206 = (l206 + 1)) {
			fRec285[l206] = 0.0f;
			
		}
		for (int l207 = 0; (l207 < 2); l207 = (l207 + 1)) {
			fRec284[l207] = 0.0f;
			
		}
		for (int l208 = 0; (l208 < 2); l208 = (l208 + 1)) {
			fRec282[l208] = 0.0f;
			
		}
		for (int l209 = 0; (l209 < 2); l209 = (l209 + 1)) {
			fRec281[l209] = 0.0f;
			
		}
		for (int l210 = 0; (l210 < 2); l210 = (l210 + 1)) {
			fRec279[l210] = 0.0f;
			
		}
		for (int l211 = 0; (l211 < 2); l211 = (l211 + 1)) {
			fRec299[l211] = 0.0f;
			
		}
		for (int l212 = 0; (l212 < 2); l212 = (l212 + 1)) {
			fRec297[l212] = 0.0f;
			
		}
		for (int l213 = 0; (l213 < 2); l213 = (l213 + 1)) {
			fRec296[l213] = 0.0f;
			
		}
		for (int l214 = 0; (l214 < 2); l214 = (l214 + 1)) {
			fRec294[l214] = 0.0f;
			
		}
		for (int l215 = 0; (l215 < 2); l215 = (l215 + 1)) {
			fRec293[l215] = 0.0f;
			
		}
		for (int l216 = 0; (l216 < 2); l216 = (l216 + 1)) {
			fRec291[l216] = 0.0f;
			
		}
		for (int l217 = 0; (l217 < 2); l217 = (l217 + 1)) {
			fRec302[l217] = 0.0f;
			
		}
		for (int l218 = 0; (l218 < 2); l218 = (l218 + 1)) {
			fRec300[l218] = 0.0f;
			
		}
		for (int l219 = 0; (l219 < 2); l219 = (l219 + 1)) {
			fRec308[l219] = 0.0f;
			
		}
		for (int l220 = 0; (l220 < 2); l220 = (l220 + 1)) {
			fRec306[l220] = 0.0f;
			
		}
		for (int l221 = 0; (l221 < 2); l221 = (l221 + 1)) {
			fRec305[l221] = 0.0f;
			
		}
		for (int l222 = 0; (l222 < 2); l222 = (l222 + 1)) {
			fRec303[l222] = 0.0f;
			
		}
		for (int l223 = 0; (l223 < 128); l223 = (l223 + 1)) {
			fVec4[l223] = 0.0f;
			
		}
		for (int l224 = 0; (l224 < 2); l224 = (l224 + 1)) {
			fRec323[l224] = 0.0f;
			
		}
		for (int l225 = 0; (l225 < 2); l225 = (l225 + 1)) {
			fRec321[l225] = 0.0f;
			
		}
		for (int l226 = 0; (l226 < 2); l226 = (l226 + 1)) {
			fRec320[l226] = 0.0f;
			
		}
		for (int l227 = 0; (l227 < 2); l227 = (l227 + 1)) {
			fRec318[l227] = 0.0f;
			
		}
		for (int l228 = 0; (l228 < 2); l228 = (l228 + 1)) {
			fRec317[l228] = 0.0f;
			
		}
		for (int l229 = 0; (l229 < 2); l229 = (l229 + 1)) {
			fRec315[l229] = 0.0f;
			
		}
		for (int l230 = 0; (l230 < 2); l230 = (l230 + 1)) {
			fRec314[l230] = 0.0f;
			
		}
		for (int l231 = 0; (l231 < 2); l231 = (l231 + 1)) {
			fRec312[l231] = 0.0f;
			
		}
		for (int l232 = 0; (l232 < 2); l232 = (l232 + 1)) {
			fRec311[l232] = 0.0f;
			
		}
		for (int l233 = 0; (l233 < 2); l233 = (l233 + 1)) {
			fRec309[l233] = 0.0f;
			
		}
		for (int l234 = 0; (l234 < 2); l234 = (l234 + 1)) {
			fRec335[l234] = 0.0f;
			
		}
		for (int l235 = 0; (l235 < 2); l235 = (l235 + 1)) {
			fRec333[l235] = 0.0f;
			
		}
		for (int l236 = 0; (l236 < 2); l236 = (l236 + 1)) {
			fRec332[l236] = 0.0f;
			
		}
		for (int l237 = 0; (l237 < 2); l237 = (l237 + 1)) {
			fRec330[l237] = 0.0f;
			
		}
		for (int l238 = 0; (l238 < 2); l238 = (l238 + 1)) {
			fRec329[l238] = 0.0f;
			
		}
		for (int l239 = 0; (l239 < 2); l239 = (l239 + 1)) {
			fRec327[l239] = 0.0f;
			
		}
		for (int l240 = 0; (l240 < 2); l240 = (l240 + 1)) {
			fRec326[l240] = 0.0f;
			
		}
		for (int l241 = 0; (l241 < 2); l241 = (l241 + 1)) {
			fRec324[l241] = 0.0f;
			
		}
		for (int l242 = 0; (l242 < 2); l242 = (l242 + 1)) {
			fRec344[l242] = 0.0f;
			
		}
		for (int l243 = 0; (l243 < 2); l243 = (l243 + 1)) {
			fRec342[l243] = 0.0f;
			
		}
		for (int l244 = 0; (l244 < 2); l244 = (l244 + 1)) {
			fRec341[l244] = 0.0f;
			
		}
		for (int l245 = 0; (l245 < 2); l245 = (l245 + 1)) {
			fRec339[l245] = 0.0f;
			
		}
		for (int l246 = 0; (l246 < 2); l246 = (l246 + 1)) {
			fRec338[l246] = 0.0f;
			
		}
		for (int l247 = 0; (l247 < 2); l247 = (l247 + 1)) {
			fRec336[l247] = 0.0f;
			
		}
		for (int l248 = 0; (l248 < 2); l248 = (l248 + 1)) {
			fRec347[l248] = 0.0f;
			
		}
		for (int l249 = 0; (l249 < 2); l249 = (l249 + 1)) {
			fRec345[l249] = 0.0f;
			
		}
		for (int l250 = 0; (l250 < 2); l250 = (l250 + 1)) {
			fRec353[l250] = 0.0f;
			
		}
		for (int l251 = 0; (l251 < 2); l251 = (l251 + 1)) {
			fRec351[l251] = 0.0f;
			
		}
		for (int l252 = 0; (l252 < 2); l252 = (l252 + 1)) {
			fRec350[l252] = 0.0f;
			
		}
		for (int l253 = 0; (l253 < 2); l253 = (l253 + 1)) {
			fRec348[l253] = 0.0f;
			
		}
		for (int l254 = 0; (l254 < 256); l254 = (l254 + 1)) {
			fVec5[l254] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("Haptic20005");
		ui_interface->declare(&fHslider0, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider0, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider2, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider1, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider1, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* input14 = inputs[14];
		FAUSTFLOAT* input15 = inputs[15];
		FAUSTFLOAT* input16 = inputs[16];
		FAUSTFLOAT* input17 = inputs[17];
		FAUSTFLOAT* input18 = inputs[18];
		FAUSTFLOAT* input19 = inputs[19];
		FAUSTFLOAT* input20 = inputs[20];
		FAUSTFLOAT* input21 = inputs[21];
		FAUSTFLOAT* input22 = inputs[22];
		FAUSTFLOAT* input23 = inputs[23];
		FAUSTFLOAT* input24 = inputs[24];
		FAUSTFLOAT* input25 = inputs[25];
		FAUSTFLOAT* input26 = inputs[26];
		FAUSTFLOAT* input27 = inputs[27];
		FAUSTFLOAT* input28 = inputs[28];
		FAUSTFLOAT* input29 = inputs[29];
		FAUSTFLOAT* input30 = inputs[30];
		FAUSTFLOAT* input31 = inputs[31];
		FAUSTFLOAT* input32 = inputs[32];
		FAUSTFLOAT* input33 = inputs[33];
		FAUSTFLOAT* input34 = inputs[34];
		FAUSTFLOAT* input35 = inputs[35];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		float fSlow0 = (0.00100000005f * (powf(10.0f, (0.0500000007f * float(fHslider0))) * float((float(fCheckbox0) < 0.5f))));
		float fSlow1 = (0.00100000005f * float(fHslider1));
		float fSlow2 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider2))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			fRec1[0] = (fSlow1 + (0.999000013f * fRec1[1]));
			float fTemp0 = tanf((fConst1 * fRec1[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = ((fTemp0 * (fTemp0 + -2.0f)) + 1.0f);
			float fTemp3 = (fTemp1 + -1.0f);
			float fTemp4 = ((fTemp0 * (fTemp0 + 2.0f)) + 1.0f);
			fRec2[0] = (float(input0[i]) - (((fRec2[2] * fTemp2) + (2.0f * (fRec2[1] * fTemp3))) / fTemp4));
			fRec3[0] = (fSlow2 + (0.999000013f * fRec3[1]));
			float fTemp5 = (fRec3[0] * fTemp4);
			float fTemp6 = (0.0f - (2.0f / fTemp4));
			float fTemp7 = (((fTemp1 * (fRec2[2] + (fRec2[0] + (2.0f * fRec2[1])))) / fTemp5) + (fRec3[0] * (0.0f - ((fRec2[1] * fTemp6) + ((fRec2[0] + fRec2[2]) / fTemp4)))));
			float fTemp8 = (fConst4 * fRec4[1]);
			float fTemp9 = (fConst7 * fRec7[1]);
			float fTemp10 = (fConst8 * fRec10[1]);
			fRec19[0] = (float(input26[i]) - (((fTemp2 * fRec19[2]) + (2.0f * (fTemp3 * fRec19[1]))) / fTemp4));
			float fTemp11 = (((fTemp1 * (fRec19[2] + (fRec19[0] + (2.0f * fRec19[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec19[1]) + ((fRec19[0] + fRec19[2]) / fTemp4))))));
			fRec20[0] = (float(input27[i]) - (((fTemp2 * fRec20[2]) + (2.0f * (fTemp3 * fRec20[1]))) / fTemp4));
			float fTemp12 = (((fTemp1 * (fRec20[2] + (fRec20[0] + (2.0f * fRec20[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec20[1]) + ((fRec20[0] + fRec20[2]) / fTemp4))))));
			fRec21[0] = (float(input28[i]) - (((fTemp2 * fRec21[2]) + (2.0f * (fTemp3 * fRec21[1]))) / fTemp4));
			float fTemp13 = (((fTemp1 * (fRec21[2] + (fRec21[0] + (2.0f * fRec21[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec21[1]) + ((fRec21[0] + fRec21[2]) / fTemp4))))));
			fRec22[0] = (float(input30[i]) - (((fTemp2 * fRec22[2]) + (2.0f * (fTemp3 * fRec22[1]))) / fTemp4));
			float fTemp14 = (((fTemp1 * (fRec22[2] + (fRec22[0] + (2.0f * fRec22[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec22[1]) + ((fRec22[0] + fRec22[2]) / fTemp4))))));
			fRec23[0] = (float(input31[i]) - (((fTemp2 * fRec23[2]) + (2.0f * (fTemp3 * fRec23[1]))) / fTemp4));
			float fTemp15 = (((fTemp1 * (fRec23[2] + (fRec23[0] + (2.0f * fRec23[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec23[1]) + ((fRec23[0] + fRec23[2]) / fTemp4))))));
			fRec24[0] = (float(input32[i]) - (((fTemp2 * fRec24[2]) + (2.0f * (fTemp3 * fRec24[1]))) / fTemp4));
			float fTemp16 = (((fTemp1 * (fRec24[2] + (fRec24[0] + (2.0f * fRec24[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec24[1]) + ((fRec24[0] + fRec24[2]) / fTemp4))))));
			fRec25[0] = (float(input33[i]) - (((fTemp2 * fRec25[2]) + (2.0f * (fTemp3 * fRec25[1]))) / fTemp4));
			float fTemp17 = (((fTemp1 * (fRec25[2] + (fRec25[0] + (2.0f * fRec25[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec25[1]) + ((fRec25[0] + fRec25[2]) / fTemp4))))));
			fRec26[0] = (float(input34[i]) - (((fTemp2 * fRec26[2]) + (2.0f * (fTemp3 * fRec26[1]))) / fTemp4));
			float fTemp18 = (((fTemp1 * (fRec26[2] + (fRec26[0] + (2.0f * fRec26[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec26[1]) + ((fRec26[0] + fRec26[2]) / fTemp4))))));
			fRec27[0] = (float(input35[i]) - (((fTemp2 * fRec27[2]) + (2.0f * (fTemp3 * fRec27[1]))) / fTemp4));
			float fTemp19 = (((fTemp1 * (fRec27[2] + (fRec27[0] + (2.0f * fRec27[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec27[1]) + ((fRec27[0] + fRec27[2]) / fTemp4))))));
			fRec28[0] = (float(input25[i]) - (((fTemp2 * fRec28[2]) + (2.0f * (fTemp3 * fRec28[1]))) / fTemp4));
			float fTemp20 = (((fTemp1 * (fRec28[2] + (fRec28[0] + (2.0f * fRec28[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec28[1]) + ((fRec28[0] + fRec28[2]) / fTemp4))))));
			fRec29[0] = (float(input29[i]) - (((fTemp2 * fRec29[2]) + (2.0f * (fTemp3 * fRec29[1]))) / fTemp4));
			float fTemp21 = (((fTemp1 * (fRec29[2] + (fRec29[0] + (2.0f * fRec29[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec29[1]) + ((fRec29[0] + fRec29[2]) / fTemp4))))));
			float fTemp22 = (fConst10 * ((((((((((0.00117140915f * fTemp11) + (0.00279840827f * fTemp12)) + (0.00128609291f * fTemp13)) + (0.0379560217f * fTemp14)) + (0.0112825241f * fTemp15)) + (0.000854527927f * fTemp16)) + (6.03300009e-07f * fTemp17)) + (0.00134000182f * fTemp18)) + (0.000223502997f * fTemp19)) - ((0.000224090705f * fTemp20) + (0.011546514f * fTemp21))));
			float fTemp23 = (fConst11 * fRec13[1]);
			float fTemp24 = (fConst12 * fRec16[1]);
			fRec18[0] = (fTemp22 + (fTemp23 + (fRec18[1] + fTemp24)));
			fRec16[0] = fRec18[0];
			float fRec17 = ((fTemp24 + fTemp23) + fTemp22);
			fRec15[0] = (fRec16[0] + fRec15[1]);
			fRec13[0] = fRec15[0];
			float fRec14 = fRec17;
			fRec12[0] = (fTemp9 + (fTemp10 + (fRec14 + fRec12[1])));
			fRec10[0] = fRec12[0];
			float fRec11 = (fTemp9 + (fRec14 + fTemp10));
			fRec9[0] = (fRec10[0] + fRec9[1]);
			fRec7[0] = fRec9[0];
			float fRec8 = fRec11;
			fRec6[0] = (fTemp8 + (fRec8 + fRec6[1]));
			fRec4[0] = fRec6[0];
			float fRec5 = (fRec8 + fTemp8);
			float fTemp25 = (fConst14 * fRec30[1]);
			float fTemp26 = (fConst15 * fRec33[1]);
			fRec42[0] = (float(input16[i]) - (((fTemp2 * fRec42[2]) + (2.0f * (fTemp3 * fRec42[1]))) / fTemp4));
			float fTemp27 = (((fTemp1 * (fRec42[2] + (fRec42[0] + (2.0f * fRec42[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec42[1]) + ((fRec42[0] + fRec42[2]) / fTemp4))))));
			fRec43[0] = (float(input17[i]) - (((fTemp2 * fRec43[2]) + (2.0f * (fTemp3 * fRec43[1]))) / fTemp4));
			float fTemp28 = (((fTemp1 * (fRec43[2] + (fRec43[0] + (2.0f * fRec43[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec43[1]) + ((fRec43[0] + fRec43[2]) / fTemp4))))));
			fRec44[0] = (float(input18[i]) - (((fTemp2 * fRec44[2]) + (2.0f * (fTemp3 * fRec44[1]))) / fTemp4));
			float fTemp29 = (((fTemp1 * (fRec44[2] + (fRec44[0] + (2.0f * fRec44[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec44[1]) + ((fRec44[0] + fRec44[2]) / fTemp4))))));
			fRec45[0] = (float(input20[i]) - (((fTemp2 * fRec45[2]) + (2.0f * (fTemp3 * fRec45[1]))) / fTemp4));
			float fTemp30 = (((fTemp1 * (fRec45[2] + (fRec45[0] + (2.0f * fRec45[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec45[1]) + ((fRec45[0] + fRec45[2]) / fTemp4))))));
			fRec46[0] = (float(input21[i]) - (((fTemp2 * fRec46[2]) + (2.0f * (fTemp3 * fRec46[1]))) / fTemp4));
			float fTemp31 = (((fTemp1 * (fRec46[2] + (fRec46[0] + (2.0f * fRec46[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec46[1]) + ((fRec46[0] + fRec46[2]) / fTemp4))))));
			fRec47[0] = (float(input22[i]) - (((fTemp2 * fRec47[2]) + (2.0f * (fTemp3 * fRec47[1]))) / fTemp4));
			float fTemp32 = (((fTemp1 * (fRec47[2] + (fRec47[0] + (2.0f * fRec47[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec47[1]) + ((fRec47[0] + fRec47[2]) / fTemp4))))));
			fRec48[0] = (float(input23[i]) - (((fTemp2 * fRec48[2]) + (2.0f * (fTemp3 * fRec48[1]))) / fTemp4));
			float fTemp33 = (((fTemp1 * (fRec48[2] + (fRec48[0] + (2.0f * fRec48[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec48[1]) + ((fRec48[0] + fRec48[2]) / fTemp4))))));
			fRec49[0] = (float(input24[i]) - (((fTemp2 * fRec49[2]) + (2.0f * (fTemp3 * fRec49[1]))) / fTemp4));
			float fTemp34 = (((fTemp1 * (fRec49[2] + (fRec49[0] + (2.0f * fRec49[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec49[1]) + ((fRec49[0] + fRec49[2]) / fTemp4))))));
			fRec50[0] = (float(input19[i]) - (((fTemp2 * fRec50[2]) + (2.0f * (fTemp3 * fRec50[1]))) / fTemp4));
			float fTemp35 = (((fTemp1 * (fRec50[2] + (fRec50[0] + (2.0f * fRec50[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec50[1]) + ((fRec50[0] + fRec50[2]) / fTemp4))))));
			float fTemp36 = (fConst17 * (((((((((0.000452356093f * fTemp27) + (0.00179918972f * fTemp28)) + (0.00120926928f * fTemp29)) + (0.0688572228f * fTemp30)) + (0.0147536509f * fTemp31)) + (0.000720839889f * fTemp32)) + (2.27966993e-05f * fTemp33)) + (0.000527242606f * fTemp34)) - (0.0137249175f * fTemp35)));
			float fTemp37 = (fConst18 * fRec36[1]);
			float fTemp38 = (fConst19 * fRec39[1]);
			fRec41[0] = (fTemp36 + (fTemp37 + (fRec41[1] + fTemp38)));
			fRec39[0] = fRec41[0];
			float fRec40 = ((fTemp38 + fTemp37) + fTemp36);
			fRec38[0] = (fRec39[0] + fRec38[1]);
			fRec36[0] = fRec38[0];
			float fRec37 = fRec40;
			fRec35[0] = (fTemp25 + (fTemp26 + (fRec37 + fRec35[1])));
			fRec33[0] = fRec35[0];
			float fRec34 = (fTemp25 + (fRec37 + fTemp26));
			fRec32[0] = (fRec33[0] + fRec32[1]);
			fRec30[0] = fRec32[0];
			float fRec31 = fRec34;
			float fTemp39 = (fConst21 * fRec51[1]);
			fRec60[0] = (float(input9[i]) - (((fTemp2 * fRec60[2]) + (2.0f * (fTemp3 * fRec60[1]))) / fTemp4));
			float fTemp40 = (((fTemp1 * (fRec60[2] + (fRec60[0] + (2.0f * fRec60[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec60[1]) + ((fRec60[0] + fRec60[2]) / fTemp4))))));
			fRec61[0] = (float(input10[i]) - (((fTemp2 * fRec61[2]) + (2.0f * (fTemp3 * fRec61[1]))) / fTemp4));
			float fTemp41 = (((fTemp1 * (fRec61[2] + (fRec61[0] + (2.0f * fRec61[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec61[1]) + ((fRec61[0] + fRec61[2]) / fTemp4))))));
			fRec62[0] = (float(input12[i]) - (((fTemp2 * fRec62[2]) + (2.0f * (fTemp3 * fRec62[1]))) / fTemp4));
			float fTemp42 = (((fTemp1 * (fRec62[2] + (fRec62[0] + (2.0f * fRec62[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec62[1]) + ((fRec62[0] + fRec62[2]) / fTemp4))))));
			fRec63[0] = (float(input13[i]) - (((fTemp2 * fRec63[2]) + (2.0f * (fTemp3 * fRec63[1]))) / fTemp4));
			float fTemp43 = (((fTemp1 * (fRec63[2] + (fRec63[0] + (2.0f * fRec63[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec63[1]) + ((fRec63[0] + fRec63[2]) / fTemp4))))));
			fRec64[0] = (float(input14[i]) - (((fTemp2 * fRec64[2]) + (2.0f * (fTemp3 * fRec64[1]))) / fTemp4));
			float fTemp44 = (((fTemp1 * (fRec64[2] + (fRec64[0] + (2.0f * fRec64[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec64[1]) + ((fRec64[0] + fRec64[2]) / fTemp4))))));
			fRec65[0] = (float(input15[i]) - (((fTemp2 * fRec65[2]) + (2.0f * (fTemp3 * fRec65[1]))) / fTemp4));
			float fTemp45 = (((fTemp1 * (fRec65[2] + (fRec65[0] + (2.0f * fRec65[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec65[1]) + ((fRec65[0] + fRec65[2]) / fTemp4))))));
			fRec66[0] = (float(input11[i]) - (((fTemp2 * fRec66[2]) + (2.0f * (fTemp3 * fRec66[1]))) / fTemp4));
			float fTemp46 = (((fTemp1 * (fRec66[2] + (fRec66[0] + (2.0f * fRec66[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec66[1]) + ((fRec66[0] + fRec66[2]) / fTemp4))))));
			float fTemp47 = (fConst23 * (((((((0.000781867187f * fTemp40) + (0.000857524981f * fTemp41)) + (0.0959466398f * fTemp42)) + (0.0149955805f * fTemp43)) + (0.000476720888f * fTemp44)) + (1.64839003e-05f * fTemp45)) - (0.0132319108f * fTemp46)));
			float fTemp48 = (fConst24 * fRec54[1]);
			float fTemp49 = (fConst25 * fRec57[1]);
			fRec59[0] = (fTemp47 + (fTemp48 + (fRec59[1] + fTemp49)));
			fRec57[0] = fRec59[0];
			float fRec58 = ((fTemp49 + fTemp48) + fTemp47);
			fRec56[0] = (fRec57[0] + fRec56[1]);
			fRec54[0] = fRec56[0];
			float fRec55 = fRec58;
			fRec53[0] = (fTemp39 + (fRec55 + fRec53[1]));
			fRec51[0] = fRec53[0];
			float fRec52 = (fRec55 + fTemp39);
			fRec70[0] = (float(input2[i]) - (((fTemp2 * fRec70[2]) + (2.0f * (fTemp3 * fRec70[1]))) / fTemp4));
			float fTemp50 = (((fTemp1 * (fRec70[2] + (fRec70[0] + (2.0f * fRec70[1])))) / fTemp5) + (0.932469487f * (fRec3[0] * (0.0f - ((fTemp6 * fRec70[1]) + ((fRec70[0] + fRec70[2]) / fTemp4))))));
			fRec71[0] = (float(input3[i]) - (((fTemp2 * fRec71[2]) + (2.0f * (fTemp3 * fRec71[1]))) / fTemp4));
			float fTemp51 = (((fTemp1 * (fRec71[2] + (fRec71[0] + (2.0f * fRec71[1])))) / fTemp5) + (0.932469487f * (fRec3[0] * (0.0f - ((fTemp6 * fRec71[1]) + ((fRec71[0] + fRec71[2]) / fTemp4))))));
			fRec72[0] = (float(input1[i]) - (((fTemp2 * fRec72[2]) + (2.0f * (fTemp3 * fRec72[1]))) / fTemp4));
			float fTemp52 = (((fTemp1 * (fRec72[2] + (fRec72[0] + (2.0f * fRec72[1])))) / fTemp5) + (0.932469487f * (fRec3[0] * (0.0f - ((fTemp6 * fRec72[1]) + ((fRec72[0] + fRec72[2]) / fTemp4))))));
			float fTemp53 = (fConst27 * (((0.103525728f * fTemp50) + (0.00626889663f * fTemp51)) - (0.00526397908f * fTemp52)));
			float fTemp54 = (fConst28 * fRec67[1]);
			fRec69[0] = (fTemp53 + (fRec69[1] + fTemp54));
			fRec67[0] = fRec69[0];
			float fRec68 = (fTemp54 + fTemp53);
			fRec79[0] = (float(input4[i]) - (((fTemp2 * fRec79[2]) + (2.0f * (fTemp3 * fRec79[1]))) / fTemp4));
			float fTemp55 = (((fTemp1 * (fRec79[2] + (fRec79[0] + (2.0f * fRec79[1])))) / fTemp5) + (0.804249108f * (fRec3[0] * (0.0f - ((fTemp6 * fRec79[1]) + ((fRec79[0] + fRec79[2]) / fTemp4))))));
			fRec80[0] = (float(input6[i]) - (((fTemp2 * fRec80[2]) + (2.0f * (fTemp3 * fRec80[1]))) / fTemp4));
			float fTemp56 = (((fTemp1 * (fRec80[2] + (fRec80[0] + (2.0f * fRec80[1])))) / fTemp5) + (0.804249108f * (fRec3[0] * (0.0f - ((fTemp6 * fRec80[1]) + ((fRec80[0] + fRec80[2]) / fTemp4))))));
			fRec81[0] = (float(input7[i]) - (((fTemp2 * fRec81[2]) + (2.0f * (fTemp3 * fRec81[1]))) / fTemp4));
			float fTemp57 = (((fTemp1 * (fRec81[2] + (fRec81[0] + (2.0f * fRec81[1])))) / fTemp5) + (0.804249108f * (fRec3[0] * (0.0f - ((fTemp6 * fRec81[1]) + ((fRec81[0] + fRec81[2]) / fTemp4))))));
			fRec82[0] = (float(input8[i]) - (((fTemp2 * fRec82[2]) + (2.0f * (fTemp3 * fRec82[1]))) / fTemp4));
			float fTemp58 = (((fTemp1 * (fRec82[2] + (fRec82[0] + (2.0f * fRec82[1])))) / fTemp5) + (0.804249108f * (fRec3[0] * (0.0f - ((fTemp6 * fRec82[1]) + ((fRec82[0] + fRec82[2]) / fTemp4))))));
			fRec83[0] = (float(input5[i]) - (((fTemp2 * fRec83[2]) + (2.0f * (fTemp3 * fRec83[1]))) / fTemp4));
			float fTemp59 = (((fTemp1 * (fRec83[2] + (fRec83[0] + (2.0f * fRec83[1])))) / fTemp5) + (0.804249108f * (fRec3[0] * (0.0f - ((fTemp6 * fRec83[1]) + ((fRec83[0] + fRec83[2]) / fTemp4))))));
			float fTemp60 = (fConst30 * (((((0.000408570602f * fTemp55) + (0.110278629f * fTemp56)) + (0.0117853126f * fTemp57)) + (0.000216953005f * fTemp58)) - (0.0100772418f * fTemp59)));
			float fTemp61 = (fConst31 * fRec73[1]);
			float fTemp62 = (fConst32 * fRec76[1]);
			fRec78[0] = (fTemp60 + (fTemp61 + (fRec78[1] + fTemp62)));
			fRec76[0] = fRec78[0];
			float fRec77 = ((fTemp62 + fTemp61) + fTemp60);
			fRec75[0] = (fRec76[0] + fRec75[1]);
			fRec73[0] = fRec75[0];
			float fRec74 = fRec77;
			fVec0[(IOTA & 255)] = ((0.0655872375f * fTemp7) + (fRec5 + (fRec31 + (fRec52 + (fRec68 + fRec74)))));
			output0[i] = FAUSTFLOAT((0.775974035f * (fRec0[0] * fVec0[((IOTA - iConst33) & 255)])));
			float fTemp63 = (fConst35 * fRec84[1]);
			float fTemp64 = (fConst37 * fRec87[1]);
			float fTemp65 = (fConst38 * fRec90[1]);
			float fTemp66 = (fConst40 * (((((((0.000596685393f * fTemp20) + (0.00216656318f * fTemp11)) + (0.0120926276f * fTemp21)) + (0.0240930431f * fTemp17)) + (0.000158870898f * fTemp18)) + (0.00403913483f * fTemp19)) - (((((0.0109345727f * fTemp12) + (0.00864047837f * fTemp13)) + (0.000964419218f * fTemp14)) + (0.00327471155f * fTemp15)) + (0.0105868289f * fTemp16))));
			float fTemp67 = (fConst41 * fRec93[1]);
			float fTemp68 = (fConst42 * fRec96[1]);
			fRec98[0] = (fTemp66 + (fTemp67 + (fRec98[1] + fTemp68)));
			fRec96[0] = fRec98[0];
			float fRec97 = ((fTemp68 + fTemp67) + fTemp66);
			fRec95[0] = (fRec96[0] + fRec95[1]);
			fRec93[0] = fRec95[0];
			float fRec94 = fRec97;
			fRec92[0] = (fTemp64 + (fTemp65 + (fRec94 + fRec92[1])));
			fRec90[0] = fRec92[0];
			float fRec91 = (fTemp64 + (fRec94 + fTemp65));
			fRec89[0] = (fRec90[0] + fRec89[1]);
			fRec87[0] = fRec89[0];
			float fRec88 = fRec91;
			fRec86[0] = (fTemp63 + (fRec88 + fRec86[1]));
			fRec84[0] = fRec86[0];
			float fRec85 = (fRec88 + fTemp63);
			float fTemp69 = (fConst44 * fRec99[1]);
			float fTemp70 = (fConst45 * fRec102[1]);
			float fTemp71 = (fConst47 * (((((0.0204185769f * fTemp27) + (0.0151314354f * fTemp35)) + (0.011729463f * fTemp33)) + (0.000503460586f * fTemp34)) - (((((0.00573151466f * fTemp28) + (0.0291441549f * fTemp29)) + (0.00676874258f * fTemp30)) + (0.00451927865f * fTemp31)) + (0.0313781276f * fTemp32))));
			float fTemp72 = (fConst48 * fRec105[1]);
			float fTemp73 = (fConst49 * fRec108[1]);
			fRec110[0] = (fTemp71 + (fTemp72 + (fRec110[1] + fTemp73)));
			fRec108[0] = fRec110[0];
			float fRec109 = ((fTemp73 + fTemp72) + fTemp71);
			fRec107[0] = (fRec108[0] + fRec107[1]);
			fRec105[0] = fRec107[0];
			float fRec106 = fRec109;
			fRec104[0] = (fTemp69 + (fTemp70 + (fRec106 + fRec104[1])));
			fRec102[0] = fRec104[0];
			float fRec103 = (fTemp69 + (fRec106 + fTemp70));
			fRec101[0] = (fRec102[0] + fRec101[1]);
			fRec99[0] = fRec101[0];
			float fRec100 = fRec103;
			float fTemp74 = (fConst51 * fRec111[1]);
			float fTemp75 = (fConst53 * ((((0.0374376029f * fTemp46) + (0.0020169632f * fTemp42)) + (0.0626121685f * fTemp45)) - ((((0.0284145605f * fTemp40) + (0.0197730698f * fTemp41)) + (0.0138366157f * fTemp43)) + (0.0218312722f * fTemp44))));
			float fTemp76 = (fConst54 * fRec114[1]);
			float fTemp77 = (fConst55 * fRec117[1]);
			fRec119[0] = (fTemp75 + (fTemp76 + (fRec119[1] + fTemp77)));
			fRec117[0] = fRec119[0];
			float fRec118 = ((fTemp77 + fTemp76) + fTemp75);
			fRec116[0] = (fRec117[0] + fRec116[1]);
			fRec114[0] = fRec116[0];
			float fRec115 = fRec118;
			fRec113[0] = (fTemp74 + (fRec115 + fRec113[1]));
			fRec111[0] = fRec113[0];
			float fRec112 = (fRec115 + fTemp74);
			float fTemp78 = (fConst57 * (((0.178464279f * fTemp52) + (0.0418821983f * fTemp50)) - (0.0710267648f * fTemp51)));
			float fTemp79 = (fConst58 * fRec120[1]);
			fRec122[0] = (fTemp78 + (fRec122[1] + fTemp79));
			fRec120[0] = fRec122[0];
			float fRec121 = (fTemp79 + fTemp78);
			float fTemp80 = (fConst60 * ((0.0446497388f * fTemp59) - ((((0.0914057717f * fTemp55) + (0.00601211097f * fTemp56)) + (0.0167624969f * fTemp57)) + (0.0960628092f * fTemp58))));
			float fTemp81 = (fConst61 * fRec123[1]);
			float fTemp82 = (fConst62 * fRec126[1]);
			fRec128[0] = (fTemp80 + (fTemp81 + (fRec128[1] + fTemp82)));
			fRec126[0] = fRec128[0];
			float fRec127 = ((fTemp82 + fTemp81) + fTemp80);
			fRec125[0] = (fRec126[0] + fRec125[1]);
			fRec123[0] = fRec125[0];
			float fRec124 = fRec127;
			fVec1[(IOTA & 127)] = ((0.154614002f * fTemp7) + (fRec85 + (fRec100 + (fRec112 + (fRec121 + fRec124)))));
			output1[i] = FAUSTFLOAT((0.855194807f * (fRec0[0] * fVec1[((IOTA - iConst63) & 127)])));
			float fTemp83 = (fConst65 * fRec129[1]);
			float fTemp84 = (fConst67 * fRec132[1]);
			float fTemp85 = (fConst68 * fRec135[1]);
			float fTemp86 = (fConst70 * (((((0.0098721832f * fTemp12) + (0.011639867f * fTemp13)) + (0.00176495011f * fTemp21)) + (0.00530634401f * fTemp15)) - (((((((0.0032992342f * fTemp20) + (0.00169749954f * fTemp11)) + (0.00630766619f * fTemp14)) + (0.000175576497f * fTemp16)) + (0.0268385708f * fTemp17)) + (0.00449786242f * fTemp18)) + (0.00619470142f * fTemp19))));
			float fTemp87 = (fConst71 * fRec138[1]);
			float fTemp88 = (fConst72 * fRec141[1]);
			fRec143[0] = (fTemp86 + (fTemp87 + (fRec143[1] + fTemp88)));
			fRec141[0] = fRec143[0];
			float fRec142 = ((fTemp88 + fTemp87) + fTemp86);
			fRec140[0] = (fRec141[0] + fRec140[1]);
			fRec138[0] = fRec140[0];
			float fRec139 = fRec142;
			fRec137[0] = (fTemp84 + (fTemp85 + (fRec139 + fRec137[1])));
			fRec135[0] = fRec137[0];
			float fRec136 = (fTemp84 + (fRec139 + fTemp85));
			fRec134[0] = (fRec135[0] + fRec134[1]);
			fRec132[0] = fRec134[0];
			float fRec133 = fRec136;
			fRec131[0] = (fTemp83 + (fRec133 + fRec131[1]));
			fRec129[0] = fRec131[0];
			float fRec130 = (fRec133 + fTemp83);
			float fTemp89 = (fConst74 * fRec144[1]);
			float fTemp90 = (fConst75 * fRec147[1]);
			float fTemp91 = (fConst77 * (((((0.00603558356f * fTemp28) + (0.0384779051f * fTemp29)) + (0.00469165808f * fTemp35)) + (0.00750146108f * fTemp31)) - (((((0.0146305803f * fTemp27) + (0.013191767f * fTemp30)) + (0.0107597187f * fTemp32)) + (0.014120413f * fTemp33)) + (0.0274924524f * fTemp34))));
			float fTemp92 = (fConst78 * fRec150[1]);
			float fTemp93 = (fConst79 * fRec153[1]);
			fRec155[0] = (fTemp91 + (fTemp92 + (fRec155[1] + fTemp93)));
			fRec153[0] = fRec155[0];
			float fRec154 = ((fTemp93 + fTemp92) + fTemp91);
			fRec152[0] = (fRec153[0] + fRec152[1]);
			fRec150[0] = fRec152[0];
			float fRec151 = fRec154;
			fRec149[0] = (fTemp89 + (fTemp90 + (fRec151 + fRec149[1])));
			fRec147[0] = fRec149[0];
			float fRec148 = (fTemp89 + (fRec151 + fTemp90));
			fRec146[0] = (fRec147[0] + fRec146[1]);
			fRec144[0] = fRec146[0];
			float fRec145 = fRec148;
			float fTemp94 = (fConst81 * fRec156[1]);
			float fTemp95 = (fConst83 * (((((0.0246870499f * fTemp40) + (0.0285582226f * fTemp41)) + (0.0237993617f * fTemp46)) + (0.0201961137f * fTemp43)) - (((0.00653513242f * fTemp42) + (0.00741897058f * fTemp44)) + (0.07245332f * fTemp45))));
			float fTemp96 = (fConst84 * fRec159[1]);
			float fTemp97 = (fConst85 * fRec162[1]);
			fRec164[0] = (fTemp95 + (fTemp96 + (fRec164[1] + fTemp97)));
			fRec162[0] = fRec164[0];
			float fRec163 = ((fTemp97 + fTemp96) + fTemp95);
			fRec161[0] = (fRec162[0] + fRec161[1]);
			fRec159[0] = fRec161[0];
			float fRec160 = fRec163;
			fRec158[0] = (fTemp94 + (fRec160 + fRec158[1]));
			fRec156[0] = fRec158[0];
			float fRec157 = (fRec160 + fTemp94);
			float fTemp98 = (fConst87 * (((0.146832123f * fTemp52) + (0.0330515839f * fTemp50)) + (0.105333939f * fTemp51)));
			float fTemp99 = (fConst88 * fRec165[1]);
			fRec167[0] = (fTemp98 + (fRec167[1] + fTemp99));
			fRec165[0] = fRec167[0];
			float fRec166 = (fTemp99 + fTemp98);
			float fTemp100 = (fConst90 * ((((0.125141412f * fTemp55) + (0.0335854292f * fTemp59)) + (0.0268673711f * fTemp57)) - ((0.0147995166f * fTemp56) + (0.0419319756f * fTemp58))));
			float fTemp101 = (fConst91 * fRec168[1]);
			float fTemp102 = (fConst92 * fRec171[1]);
			fRec173[0] = (fTemp100 + (fTemp101 + (fRec173[1] + fTemp102)));
			fRec171[0] = fRec173[0];
			float fRec172 = ((fTemp102 + fTemp101) + fTemp100);
			fRec170[0] = (fRec171[0] + fRec170[1]);
			fRec168[0] = fRec170[0];
			float fRec169 = fRec172;
			fVec2[(IOTA & 127)] = ((0.140189067f * fTemp7) + (fRec130 + (fRec145 + (fRec157 + (fRec166 + fRec169)))));
			output2[i] = FAUSTFLOAT((0.922727287f * (fRec0[0] * fVec2[((IOTA - iConst93) & 127)])));
			float fTemp103 = (fConst95 * fRec174[1]);
			float fTemp104 = (fConst97 * fRec177[1]);
			float fTemp105 = (fConst98 * fRec180[1]);
			float fTemp106 = (fConst100 * (((((((0.017601734f * fTemp20) + (0.00214043981f * fTemp11)) + (0.00564556569f * fTemp16)) + (0.0184893422f * fTemp17)) + (0.00150676095f * fTemp18)) + (0.00476671942f * fTemp19)) - (((((0.0106791798f * fTemp12) + (0.0052786218f * fTemp13)) + (0.000100495301f * fTemp21)) + (0.00996193197f * fTemp14)) + (0.00700029638f * fTemp15))));
			float fTemp107 = (fConst101 * fRec183[1]);
			float fTemp108 = (fConst102 * fRec186[1]);
			fRec188[0] = (fTemp106 + (fTemp107 + (fRec188[1] + fTemp108)));
			fRec186[0] = fRec188[0];
			float fRec187 = ((fTemp108 + fTemp107) + fTemp106);
			fRec185[0] = (fRec186[0] + fRec185[1]);
			fRec183[0] = fRec185[0];
			float fRec184 = fRec187;
			fRec182[0] = (fTemp104 + (fTemp105 + (fRec184 + fRec182[1])));
			fRec180[0] = fRec182[0];
			float fRec181 = (fTemp104 + (fRec184 + fTemp105));
			fRec179[0] = (fRec180[0] + fRec179[1]);
			fRec177[0] = fRec179[0];
			float fRec178 = fRec181;
			fRec176[0] = (fTemp103 + (fRec178 + fRec176[1]));
			fRec174[0] = fRec176[0];
			float fRec175 = (fRec178 + fTemp103);
			float fTemp109 = (fConst104 * fRec189[1]);
			float fTemp110 = (fConst105 * fRec192[1]);
			float fTemp111 = (fConst107 * (((((0.0073975292f * fTemp27) + (0.0336818695f * fTemp32)) + (0.00974866655f * fTemp33)) + (0.0143417958f * fTemp34)) - (((((0.00456190063f * fTemp28) + (0.0161853041f * fTemp29)) + (0.00107440085f * fTemp35)) + (0.0225187f * fTemp30)) + (0.00382086844f * fTemp31))));
			float fTemp112 = (fConst108 * fRec195[1]);
			float fTemp113 = (fConst109 * fRec198[1]);
			fRec200[0] = (fTemp111 + (fTemp112 + (fRec200[1] + fTemp113)));
			fRec198[0] = fRec200[0];
			float fRec199 = ((fTemp113 + fTemp112) + fTemp111);
			fRec197[0] = (fRec198[0] + fRec197[1]);
			fRec195[0] = fRec197[0];
			float fRec196 = fRec199;
			fRec194[0] = (fTemp109 + (fTemp110 + (fRec196 + fRec194[1])));
			fRec192[0] = fRec194[0];
			float fRec193 = (fTemp109 + (fRec196 + fTemp110));
			fRec191[0] = (fRec192[0] + fRec191[1]);
			fRec189[0] = fRec191[0];
			float fRec190 = fRec193;
			float fTemp114 = (fConst111 * fRec201[1]);
			float fTemp115 = (fConst113 * ((((0.0232535545f * fTemp43) + (0.0251043458f * fTemp44)) + (0.0517983586f * fTemp45)) - ((((0.0225795712f * fTemp40) + (0.0105120596f * fTemp41)) + (0.00682708621f * fTemp46)) + (0.0182650927f * fTemp42))));
			float fTemp116 = (fConst114 * fRec204[1]);
			float fTemp117 = (fConst115 * fRec207[1]);
			fRec209[0] = (fTemp115 + (fTemp116 + (fRec209[1] + fTemp117)));
			fRec207[0] = fRec209[0];
			float fRec208 = ((fTemp117 + fTemp116) + fTemp115);
			fRec206[0] = (fRec207[0] + fRec206[1]);
			fRec204[0] = fRec206[0];
			float fRec205 = fRec208;
			fRec203[0] = (fTemp114 + (fRec205 + fRec203[1]));
			fRec201[0] = fRec203[0];
			float fRec202 = (fRec205 + fTemp114);
			float fTemp118 = (fConst117 * (((0.0272701606f * fTemp50) + (0.204333559f * fTemp51)) - (0.0425000452f * fTemp52)));
			float fTemp119 = (fConst118 * fRec210[1]);
			fRec212[0] = (fTemp118 + (fRec212[1] + fTemp119));
			fRec210[0] = fRec212[0];
			float fRec211 = (fTemp119 + fTemp118);
			float fTemp120 = (fConst120 * (((0.0397926383f * fTemp57) + (0.124420598f * fTemp58)) - (((0.0492154211f * fTemp55) + (0.00888533425f * fTemp59)) + (0.0316267349f * fTemp56))));
			float fTemp121 = (fConst121 * fRec213[1]);
			float fTemp122 = (fConst122 * fRec216[1]);
			fRec218[0] = (fTemp120 + (fTemp121 + (fRec218[1] + fTemp122)));
			fRec216[0] = fRec218[0];
			float fRec217 = ((fTemp122 + fTemp121) + fTemp120);
			fRec215[0] = (fRec216[0] + fRec215[1]);
			fRec213[0] = fRec215[0];
			float fRec214 = fRec217;
			fVec3[(IOTA & 31)] = ((0.164402053f * fTemp7) + (fRec175 + (fRec190 + (fRec202 + (fRec211 + fRec214)))));
			output3[i] = FAUSTFLOAT((0.974025965f * (fRec0[0] * fVec3[((IOTA - iConst123) & 31)])));
			float fTemp123 = (fConst125 * fRec219[1]);
			float fTemp124 = (fConst127 * fRec222[1]);
			float fTemp125 = (fConst128 * fRec225[1]);
			float fTemp126 = (fConst130 * (((0.01085837f * fTemp12) + (0.007121081f * fTemp21)) - (((((((((0.000191474406f * fTemp20) + (0.00283277826f * fTemp11)) + (0.00265362835f * fTemp13)) + (0.0119208666f * fTemp14)) + (0.00150735874f * fTemp15)) + (0.00647052703f * fTemp16)) + (0.00635479996f * fTemp17)) + (0.00144588423f * fTemp18)) + (0.00730795227f * fTemp19))));
			float fTemp127 = (fConst131 * fRec228[1]);
			float fTemp128 = (fConst132 * fRec231[1]);
			fRec233[0] = (fTemp126 + (fTemp127 + (fRec233[1] + fTemp128)));
			fRec231[0] = fRec233[0];
			float fRec232 = ((fTemp128 + fTemp127) + fTemp126);
			fRec230[0] = (fRec231[0] + fRec230[1]);
			fRec228[0] = fRec230[0];
			float fRec229 = fRec232;
			fRec227[0] = (fTemp124 + (fTemp125 + (fRec229 + fRec227[1])));
			fRec225[0] = fRec227[0];
			float fRec226 = (fTemp124 + (fRec229 + fTemp125));
			fRec224[0] = (fRec225[0] + fRec224[1]);
			fRec222[0] = fRec224[0];
			float fRec223 = fRec226;
			fRec221[0] = (fTemp123 + (fRec223 + fRec221[1]));
			fRec219[0] = fRec221[0];
			float fRec220 = (fRec223 + fTemp123);
			float fTemp129 = (fConst134 * fRec234[1]);
			float fTemp130 = (fConst135 * fRec237[1]);
			float fTemp131 = (fConst137 * (((0.0055433861f * fTemp28) + (0.00335547631f * fTemp35)) - (((((((0.00804608408f * fTemp27) + (0.0136376573f * fTemp29)) + (0.0260490403f * fTemp30)) + (0.000853162026f * fTemp31)) + (0.0324630663f * fTemp32)) + (0.00283613242f * fTemp33)) + (0.000433237088f * fTemp34))));
			float fTemp132 = (fConst138 * fRec240[1]);
			float fTemp133 = (fConst139 * fRec243[1]);
			fRec245[0] = (fTemp131 + (fTemp132 + (fRec245[1] + fTemp133)));
			fRec243[0] = fRec245[0];
			float fRec244 = ((fTemp133 + fTemp132) + fTemp131);
			fRec242[0] = (fRec243[0] + fRec242[1]);
			fRec240[0] = fRec242[0];
			float fRec241 = fRec244;
			fRec239[0] = (fTemp129 + (fTemp130 + (fRec241 + fRec239[1])));
			fRec237[0] = fRec239[0];
			float fRec238 = (fTemp129 + (fRec241 + fTemp130));
			fRec236[0] = (fRec237[0] + fRec236[1]);
			fRec234[0] = fRec236[0];
			float fRec235 = fRec238;
			float fTemp134 = (fConst141 * fRec246[1]);
			float fTemp135 = (fConst143 * (((0.0312141068f * fTemp40) + (0.00528814644f * fTemp43)) - (((((0.00958592724f * fTemp41) + (0.0265802797f * fTemp46)) + (0.0206528027f * fTemp42)) + (0.0236536935f * fTemp44)) + (0.0172249489f * fTemp45))));
			float fTemp136 = (fConst144 * fRec249[1]);
			float fTemp137 = (fConst145 * fRec252[1]);
			fRec254[0] = (fTemp135 + (fTemp136 + (fRec254[1] + fTemp137)));
			fRec252[0] = fRec254[0];
			float fRec253 = ((fTemp137 + fTemp136) + fTemp135);
			fRec251[0] = (fRec252[0] + fRec251[1]);
			fRec249[0] = fRec251[0];
			float fRec250 = fRec253;
			fRec248[0] = (fTemp134 + (fRec250 + fRec248[1]));
			fRec246[0] = fRec248[0];
			float fRec247 = (fRec250 + fTemp134);
			float fTemp138 = (fConst147 * (((0.0307844132f * fTemp50) + (0.0467489548f * fTemp51)) - (0.223771259f * fTemp52)));
			float fTemp139 = (fConst148 * fRec255[1]);
			fRec257[0] = (fTemp138 + (fRec257[1] + fTemp139));
			fRec255[0] = fRec257[0];
			float fRec256 = (fTemp139 + fTemp138);
			float fTemp140 = (fConst150 * ((0.00867377967f * fTemp57) - ((((0.0492974669f * fTemp55) + (0.0434436277f * fTemp59)) + (0.0355822034f * fTemp56)) + (0.116754435f * fTemp58))));
			float fTemp141 = (fConst151 * fRec258[1]);
			float fTemp142 = (fConst152 * fRec261[1]);
			fRec263[0] = (fTemp140 + (fTemp141 + (fRec263[1] + fTemp142)));
			fRec261[0] = fRec263[0];
			float fRec262 = ((fTemp142 + fTemp141) + fTemp140);
			fRec260[0] = (fRec261[0] + fRec260[1]);
			fRec258[0] = fRec260[0];
			float fRec259 = fRec262;
			output4[i] = FAUSTFLOAT((fRec0[0] * ((0.187742293f * fTemp7) + (fRec220 + (fRec235 + (fRec247 + (fRec256 + fRec259)))))));
			float fTemp143 = (fConst154 * fRec264[1]);
			float fTemp144 = (fConst156 * fRec267[1]);
			float fTemp145 = (fConst157 * fRec270[1]);
			float fTemp146 = (fConst159 * ((((((0.00357122556f * fTemp11) + (0.0113166999f * fTemp13)) + (0.000557932595f * fTemp15)) + (0.0141416918f * fTemp17)) + (0.00820876565f * fTemp19)) - ((((((0.0138387056f * fTemp20) + (0.0269964561f * fTemp12)) + (0.00388680631f * fTemp21)) + (0.00761676673f * fTemp14)) + (0.00217405031f * fTemp16)) + (0.00467212079f * fTemp18))));
			float fTemp147 = (fConst160 * fRec273[1]);
			float fTemp148 = (fConst161 * fRec276[1]);
			fRec278[0] = (fTemp146 + (fTemp147 + (fRec278[1] + fTemp148)));
			fRec276[0] = fRec278[0];
			float fRec277 = ((fTemp148 + fTemp147) + fTemp146);
			fRec275[0] = (fRec276[0] + fRec275[1]);
			fRec273[0] = fRec275[0];
			float fRec274 = fRec277;
			fRec272[0] = (fTemp144 + (fTemp145 + (fRec274 + fRec272[1])));
			fRec270[0] = fRec272[0];
			float fRec271 = (fTemp144 + (fRec274 + fTemp145));
			fRec269[0] = (fRec270[0] + fRec269[1]);
			fRec267[0] = fRec269[0];
			float fRec268 = fRec271;
			fRec266[0] = (fTemp143 + (fRec268 + fRec266[1]));
			fRec264[0] = fRec266[0];
			float fRec265 = (fRec268 + fTemp143);
			float fTemp149 = (fConst163 * fRec279[1]);
			float fTemp150 = (fConst164 * fRec282[1]);
			float fTemp151 = (fConst166 * (((((0.0238492079f * fTemp27) + (0.0393353142f * fTemp29)) + (0.00476471055f * fTemp32)) + (0.00751847215f * fTemp33)) - (((((0.0137709212f * fTemp28) + (0.0063038012f * fTemp35)) + (0.0147041222f * fTemp30)) + (0.00206293561f * fTemp31)) + (0.0265560169f * fTemp34))));
			float fTemp152 = (fConst167 * fRec285[1]);
			float fTemp153 = (fConst168 * fRec288[1]);
			fRec290[0] = (fTemp151 + (fTemp152 + (fRec290[1] + fTemp153)));
			fRec288[0] = fRec290[0];
			float fRec289 = ((fTemp153 + fTemp152) + fTemp151);
			fRec287[0] = (fRec288[0] + fRec287[1]);
			fRec285[0] = fRec287[0];
			float fRec286 = fRec289;
			fRec284[0] = (fTemp149 + (fTemp150 + (fRec286 + fRec284[1])));
			fRec282[0] = fRec284[0];
			float fRec283 = (fTemp149 + (fRec286 + fTemp150));
			fRec281[0] = (fRec282[0] + fRec281[1]);
			fRec279[0] = fRec281[0];
			float fRec280 = fRec283;
			float fTemp154 = (fConst170 * fRec291[1]);
			float fTemp155 = (fConst172 * ((((0.0278805941f * fTemp41) + (0.00353613659f * fTemp44)) + (0.0318443961f * fTemp45)) - ((((0.0715131387f * fTemp40) + (0.0205610227f * fTemp46)) + (0.00850819983f * fTemp42)) + (0.0199216101f * fTemp43))));
			float fTemp156 = (fConst173 * fRec294[1]);
			float fTemp157 = (fConst174 * fRec297[1]);
			fRec299[0] = (fTemp155 + (fTemp156 + (fRec299[1] + fTemp157)));
			fRec297[0] = fRec299[0];
			float fRec298 = ((fTemp157 + fTemp156) + fTemp155);
			fRec296[0] = (fRec297[0] + fRec296[1]);
			fRec294[0] = fRec296[0];
			float fRec295 = fRec298;
			fRec293[0] = (fTemp154 + (fRec295 + fRec293[1]));
			fRec291[0] = fRec293[0];
			float fRec292 = (fRec295 + fTemp154);
			float fTemp158 = (fConst176 * ((0.028236473f * fTemp50) - ((0.110709824f * fTemp52) + (0.131656095f * fTemp51))));
			float fTemp159 = (fConst177 * fRec300[1]);
			fRec302[0] = (fTemp158 + (fRec302[1] + fTemp159));
			fRec300[0] = fRec302[0];
			float fRec301 = (fTemp159 + fTemp158);
			float fTemp160 = (fConst179 * (((0.126431569f * fTemp55) + (0.0252631698f * fTemp58)) - (((0.0265602339f * fTemp59) + (0.0163341369f * fTemp56)) + (0.0283783656f * fTemp57))));
			float fTemp161 = (fConst180 * fRec303[1]);
			float fTemp162 = (fConst181 * fRec306[1]);
			fRec308[0] = (fTemp160 + (fTemp161 + (fRec308[1] + fTemp162)));
			fRec306[0] = fRec308[0];
			float fRec307 = ((fTemp162 + fTemp161) + fTemp160);
			fRec305[0] = (fRec306[0] + fRec305[1]);
			fRec303[0] = fRec305[0];
			float fRec304 = fRec307;
			fVec4[(IOTA & 127)] = ((0.131561235f * fTemp7) + (fRec265 + (fRec280 + (fRec292 + (fRec301 + fRec304)))));
			output5[i] = FAUSTFLOAT((0.877922058f * (fRec0[0] * fVec4[((IOTA - iConst182) & 127)])));
			float fTemp163 = (fConst184 * fRec309[1]);
			float fTemp164 = (fConst186 * fRec312[1]);
			float fTemp165 = (fConst187 * fRec315[1]);
			float fTemp166 = (fConst189 * (((((0.019754976f * fTemp20) + (0.0241851266f * fTemp12)) + (0.0126912873f * fTemp16)) + (0.00483014807f * fTemp18)) - (((((((0.00758416951f * fTemp11) + (0.00507471943f * fTemp13)) + (0.000186893405f * fTemp21)) + (0.00177946629f * fTemp14)) + (0.0100926897f * fTemp15)) + (0.0229627639f * fTemp17)) + (0.0121952677f * fTemp19))));
			float fTemp167 = (fConst190 * fRec318[1]);
			float fTemp168 = (fConst191 * fRec321[1]);
			fRec323[0] = (fTemp166 + (fTemp167 + (fRec323[1] + fTemp168)));
			fRec321[0] = fRec323[0];
			float fRec322 = ((fTemp168 + fTemp167) + fTemp166);
			fRec320[0] = (fRec321[0] + fRec320[1]);
			fRec318[0] = fRec320[0];
			float fRec319 = fRec322;
			fRec317[0] = (fTemp164 + (fTemp165 + (fRec319 + fRec317[1])));
			fRec315[0] = fRec317[0];
			float fRec316 = (fTemp164 + (fRec319 + fTemp165));
			fRec314[0] = (fRec315[0] + fRec314[1]);
			fRec312[0] = fRec314[0];
			float fRec313 = fRec316;
			fRec311[0] = (fTemp163 + (fRec313 + fRec311[1]));
			fRec309[0] = fRec311[0];
			float fRec310 = (fRec313 + fTemp163);
			float fTemp169 = (fConst193 * fRec324[1]);
			float fTemp170 = (fConst194 * fRec327[1]);
			float fTemp171 = (fConst196 * (((((0.0124552334f * fTemp28) + (0.000854959595f * fTemp35)) + (0.0339042544f * fTemp32)) + (0.0253450647f * fTemp34)) - (((((0.0419839211f * fTemp27) + (0.0200452134f * fTemp29)) + (0.00606057374f * fTemp30)) + (0.0126966843f * fTemp31)) + (0.0121027157f * fTemp33))));
			float fTemp172 = (fConst197 * fRec330[1]);
			float fTemp173 = (fConst198 * fRec333[1]);
			fRec335[0] = (fTemp171 + (fTemp172 + (fRec335[1] + fTemp173)));
			fRec333[0] = fRec335[0];
			float fRec334 = ((fTemp173 + fTemp172) + fTemp171);
			fRec332[0] = (fRec333[0] + fRec332[1]);
			fRec330[0] = fRec332[0];
			float fRec331 = fRec334;
			fRec329[0] = (fTemp169 + (fTemp170 + (fRec331 + fRec329[1])));
			fRec327[0] = fRec329[0];
			float fRec328 = (fTemp169 + (fRec331 + fTemp170));
			fRec326[0] = (fRec327[0] + fRec326[1]);
			fRec324[0] = fRec326[0];
			float fRec325 = fRec328;
			float fTemp174 = (fConst200 * fRec336[1]);
			float fTemp175 = (fConst202 * (((((0.0615413934f * fTemp40) + (0.00693626096f * fTemp46)) + (0.000571844517f * fTemp42)) + (0.0236705095f * fTemp44)) - (((0.0139523977f * fTemp41) + (0.0306160189f * fTemp43)) + (0.0557824783f * fTemp45))));
			float fTemp176 = (fConst203 * fRec339[1]);
			float fTemp177 = (fConst204 * fRec342[1]);
			fRec344[0] = (fTemp175 + (fTemp176 + (fRec344[1] + fTemp177)));
			fRec342[0] = fRec344[0];
			float fRec343 = ((fTemp177 + fTemp176) + fTemp175);
			fRec341[0] = (fRec342[0] + fRec341[1]);
			fRec339[0] = fRec341[0];
			float fRec340 = fRec343;
			fRec338[0] = (fTemp174 + (fRec340 + fRec338[1]));
			fRec336[0] = fRec338[0];
			float fRec337 = (fRec340 + fTemp174);
			float fTemp178 = (fConst206 * (((0.042532295f * fTemp52) + (0.0308437794f * fTemp50)) - (0.142909408f * fTemp51)));
			float fTemp179 = (fConst207 * fRec345[1]);
			fRec347[0] = (fTemp178 + (fRec347[1] + fTemp179));
			fRec345[0] = fRec347[0];
			float fRec346 = (fTemp179 + fTemp178);
			float fTemp180 = (fConst209 * (((0.00930338632f * fTemp59) + (0.0998487398f * fTemp58)) - (((0.0646818727f * fTemp55) + (0.00509015517f * fTemp56)) + (0.0364908017f * fTemp57))));
			float fTemp181 = (fConst210 * fRec348[1]);
			float fTemp182 = (fConst211 * fRec351[1]);
			fRec353[0] = (fTemp180 + (fTemp181 + (fRec353[1] + fTemp182)));
			fRec351[0] = fRec353[0];
			float fRec352 = ((fTemp182 + fTemp181) + fTemp180);
			fRec350[0] = (fRec351[0] + fRec350[1]);
			fRec348[0] = fRec350[0];
			float fRec349 = fRec352;
			fVec5[(IOTA & 255)] = ((0.114496559f * fTemp7) + (fRec310 + (fRec325 + (fRec337 + (fRec346 + fRec349)))));
			output6[i] = FAUSTFLOAT((0.844805181f * (fRec0[0] * fVec5[((IOTA - iConst212) & 255)])));
			fRec0[1] = fRec0[0];
			fRec1[1] = fRec1[0];
			fRec2[2] = fRec2[1];
			fRec2[1] = fRec2[0];
			fRec3[1] = fRec3[0];
			fRec19[2] = fRec19[1];
			fRec19[1] = fRec19[0];
			fRec20[2] = fRec20[1];
			fRec20[1] = fRec20[0];
			fRec21[2] = fRec21[1];
			fRec21[1] = fRec21[0];
			fRec22[2] = fRec22[1];
			fRec22[1] = fRec22[0];
			fRec23[2] = fRec23[1];
			fRec23[1] = fRec23[0];
			fRec24[2] = fRec24[1];
			fRec24[1] = fRec24[0];
			fRec25[2] = fRec25[1];
			fRec25[1] = fRec25[0];
			fRec26[2] = fRec26[1];
			fRec26[1] = fRec26[0];
			fRec27[2] = fRec27[1];
			fRec27[1] = fRec27[0];
			fRec28[2] = fRec28[1];
			fRec28[1] = fRec28[0];
			fRec29[2] = fRec29[1];
			fRec29[1] = fRec29[0];
			fRec18[1] = fRec18[0];
			fRec16[1] = fRec16[0];
			fRec15[1] = fRec15[0];
			fRec13[1] = fRec13[0];
			fRec12[1] = fRec12[0];
			fRec10[1] = fRec10[0];
			fRec9[1] = fRec9[0];
			fRec7[1] = fRec7[0];
			fRec6[1] = fRec6[0];
			fRec4[1] = fRec4[0];
			fRec42[2] = fRec42[1];
			fRec42[1] = fRec42[0];
			fRec43[2] = fRec43[1];
			fRec43[1] = fRec43[0];
			fRec44[2] = fRec44[1];
			fRec44[1] = fRec44[0];
			fRec45[2] = fRec45[1];
			fRec45[1] = fRec45[0];
			fRec46[2] = fRec46[1];
			fRec46[1] = fRec46[0];
			fRec47[2] = fRec47[1];
			fRec47[1] = fRec47[0];
			fRec48[2] = fRec48[1];
			fRec48[1] = fRec48[0];
			fRec49[2] = fRec49[1];
			fRec49[1] = fRec49[0];
			fRec50[2] = fRec50[1];
			fRec50[1] = fRec50[0];
			fRec41[1] = fRec41[0];
			fRec39[1] = fRec39[0];
			fRec38[1] = fRec38[0];
			fRec36[1] = fRec36[0];
			fRec35[1] = fRec35[0];
			fRec33[1] = fRec33[0];
			fRec32[1] = fRec32[0];
			fRec30[1] = fRec30[0];
			fRec60[2] = fRec60[1];
			fRec60[1] = fRec60[0];
			fRec61[2] = fRec61[1];
			fRec61[1] = fRec61[0];
			fRec62[2] = fRec62[1];
			fRec62[1] = fRec62[0];
			fRec63[2] = fRec63[1];
			fRec63[1] = fRec63[0];
			fRec64[2] = fRec64[1];
			fRec64[1] = fRec64[0];
			fRec65[2] = fRec65[1];
			fRec65[1] = fRec65[0];
			fRec66[2] = fRec66[1];
			fRec66[1] = fRec66[0];
			fRec59[1] = fRec59[0];
			fRec57[1] = fRec57[0];
			fRec56[1] = fRec56[0];
			fRec54[1] = fRec54[0];
			fRec53[1] = fRec53[0];
			fRec51[1] = fRec51[0];
			fRec70[2] = fRec70[1];
			fRec70[1] = fRec70[0];
			fRec71[2] = fRec71[1];
			fRec71[1] = fRec71[0];
			fRec72[2] = fRec72[1];
			fRec72[1] = fRec72[0];
			fRec69[1] = fRec69[0];
			fRec67[1] = fRec67[0];
			fRec79[2] = fRec79[1];
			fRec79[1] = fRec79[0];
			fRec80[2] = fRec80[1];
			fRec80[1] = fRec80[0];
			fRec81[2] = fRec81[1];
			fRec81[1] = fRec81[0];
			fRec82[2] = fRec82[1];
			fRec82[1] = fRec82[0];
			fRec83[2] = fRec83[1];
			fRec83[1] = fRec83[0];
			fRec78[1] = fRec78[0];
			fRec76[1] = fRec76[0];
			fRec75[1] = fRec75[0];
			fRec73[1] = fRec73[0];
			IOTA = (IOTA + 1);
			fRec98[1] = fRec98[0];
			fRec96[1] = fRec96[0];
			fRec95[1] = fRec95[0];
			fRec93[1] = fRec93[0];
			fRec92[1] = fRec92[0];
			fRec90[1] = fRec90[0];
			fRec89[1] = fRec89[0];
			fRec87[1] = fRec87[0];
			fRec86[1] = fRec86[0];
			fRec84[1] = fRec84[0];
			fRec110[1] = fRec110[0];
			fRec108[1] = fRec108[0];
			fRec107[1] = fRec107[0];
			fRec105[1] = fRec105[0];
			fRec104[1] = fRec104[0];
			fRec102[1] = fRec102[0];
			fRec101[1] = fRec101[0];
			fRec99[1] = fRec99[0];
			fRec119[1] = fRec119[0];
			fRec117[1] = fRec117[0];
			fRec116[1] = fRec116[0];
			fRec114[1] = fRec114[0];
			fRec113[1] = fRec113[0];
			fRec111[1] = fRec111[0];
			fRec122[1] = fRec122[0];
			fRec120[1] = fRec120[0];
			fRec128[1] = fRec128[0];
			fRec126[1] = fRec126[0];
			fRec125[1] = fRec125[0];
			fRec123[1] = fRec123[0];
			fRec143[1] = fRec143[0];
			fRec141[1] = fRec141[0];
			fRec140[1] = fRec140[0];
			fRec138[1] = fRec138[0];
			fRec137[1] = fRec137[0];
			fRec135[1] = fRec135[0];
			fRec134[1] = fRec134[0];
			fRec132[1] = fRec132[0];
			fRec131[1] = fRec131[0];
			fRec129[1] = fRec129[0];
			fRec155[1] = fRec155[0];
			fRec153[1] = fRec153[0];
			fRec152[1] = fRec152[0];
			fRec150[1] = fRec150[0];
			fRec149[1] = fRec149[0];
			fRec147[1] = fRec147[0];
			fRec146[1] = fRec146[0];
			fRec144[1] = fRec144[0];
			fRec164[1] = fRec164[0];
			fRec162[1] = fRec162[0];
			fRec161[1] = fRec161[0];
			fRec159[1] = fRec159[0];
			fRec158[1] = fRec158[0];
			fRec156[1] = fRec156[0];
			fRec167[1] = fRec167[0];
			fRec165[1] = fRec165[0];
			fRec173[1] = fRec173[0];
			fRec171[1] = fRec171[0];
			fRec170[1] = fRec170[0];
			fRec168[1] = fRec168[0];
			fRec188[1] = fRec188[0];
			fRec186[1] = fRec186[0];
			fRec185[1] = fRec185[0];
			fRec183[1] = fRec183[0];
			fRec182[1] = fRec182[0];
			fRec180[1] = fRec180[0];
			fRec179[1] = fRec179[0];
			fRec177[1] = fRec177[0];
			fRec176[1] = fRec176[0];
			fRec174[1] = fRec174[0];
			fRec200[1] = fRec200[0];
			fRec198[1] = fRec198[0];
			fRec197[1] = fRec197[0];
			fRec195[1] = fRec195[0];
			fRec194[1] = fRec194[0];
			fRec192[1] = fRec192[0];
			fRec191[1] = fRec191[0];
			fRec189[1] = fRec189[0];
			fRec209[1] = fRec209[0];
			fRec207[1] = fRec207[0];
			fRec206[1] = fRec206[0];
			fRec204[1] = fRec204[0];
			fRec203[1] = fRec203[0];
			fRec201[1] = fRec201[0];
			fRec212[1] = fRec212[0];
			fRec210[1] = fRec210[0];
			fRec218[1] = fRec218[0];
			fRec216[1] = fRec216[0];
			fRec215[1] = fRec215[0];
			fRec213[1] = fRec213[0];
			fRec233[1] = fRec233[0];
			fRec231[1] = fRec231[0];
			fRec230[1] = fRec230[0];
			fRec228[1] = fRec228[0];
			fRec227[1] = fRec227[0];
			fRec225[1] = fRec225[0];
			fRec224[1] = fRec224[0];
			fRec222[1] = fRec222[0];
			fRec221[1] = fRec221[0];
			fRec219[1] = fRec219[0];
			fRec245[1] = fRec245[0];
			fRec243[1] = fRec243[0];
			fRec242[1] = fRec242[0];
			fRec240[1] = fRec240[0];
			fRec239[1] = fRec239[0];
			fRec237[1] = fRec237[0];
			fRec236[1] = fRec236[0];
			fRec234[1] = fRec234[0];
			fRec254[1] = fRec254[0];
			fRec252[1] = fRec252[0];
			fRec251[1] = fRec251[0];
			fRec249[1] = fRec249[0];
			fRec248[1] = fRec248[0];
			fRec246[1] = fRec246[0];
			fRec257[1] = fRec257[0];
			fRec255[1] = fRec255[0];
			fRec263[1] = fRec263[0];
			fRec261[1] = fRec261[0];
			fRec260[1] = fRec260[0];
			fRec258[1] = fRec258[0];
			fRec278[1] = fRec278[0];
			fRec276[1] = fRec276[0];
			fRec275[1] = fRec275[0];
			fRec273[1] = fRec273[0];
			fRec272[1] = fRec272[0];
			fRec270[1] = fRec270[0];
			fRec269[1] = fRec269[0];
			fRec267[1] = fRec267[0];
			fRec266[1] = fRec266[0];
			fRec264[1] = fRec264[0];
			fRec290[1] = fRec290[0];
			fRec288[1] = fRec288[0];
			fRec287[1] = fRec287[0];
			fRec285[1] = fRec285[0];
			fRec284[1] = fRec284[0];
			fRec282[1] = fRec282[0];
			fRec281[1] = fRec281[0];
			fRec279[1] = fRec279[0];
			fRec299[1] = fRec299[0];
			fRec297[1] = fRec297[0];
			fRec296[1] = fRec296[0];
			fRec294[1] = fRec294[0];
			fRec293[1] = fRec293[0];
			fRec291[1] = fRec291[0];
			fRec302[1] = fRec302[0];
			fRec300[1] = fRec300[0];
			fRec308[1] = fRec308[0];
			fRec306[1] = fRec306[0];
			fRec305[1] = fRec305[0];
			fRec303[1] = fRec303[0];
			fRec323[1] = fRec323[0];
			fRec321[1] = fRec321[0];
			fRec320[1] = fRec320[0];
			fRec318[1] = fRec318[0];
			fRec317[1] = fRec317[0];
			fRec315[1] = fRec315[0];
			fRec314[1] = fRec314[0];
			fRec312[1] = fRec312[0];
			fRec311[1] = fRec311[0];
			fRec309[1] = fRec309[0];
			fRec335[1] = fRec335[0];
			fRec333[1] = fRec333[0];
			fRec332[1] = fRec332[0];
			fRec330[1] = fRec330[0];
			fRec329[1] = fRec329[0];
			fRec327[1] = fRec327[0];
			fRec326[1] = fRec326[0];
			fRec324[1] = fRec324[0];
			fRec344[1] = fRec344[0];
			fRec342[1] = fRec342[0];
			fRec341[1] = fRec341[0];
			fRec339[1] = fRec339[0];
			fRec338[1] = fRec338[0];
			fRec336[1] = fRec336[0];
			fRec347[1] = fRec347[0];
			fRec345[1] = fRec345[0];
			fRec353[1] = fRec353[0];
			fRec351[1] = fRec351[0];
			fRec350[1] = fRec350[0];
			fRec348[1] = fRec348[0];
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if defined(F2SC_DEBUG_MES)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if defined(F2SC_DEBUG_MES) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if defined(F2SC_DEBUG_MES)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // F2SC_DEBUG_MES
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
