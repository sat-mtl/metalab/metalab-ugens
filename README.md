# metalab-ugens

The metalab-ugens is a collection of ambisonic decoders for use with the [Satosphere](http://sat.qc.ca/fr/satosphere), as well as with other audio systems, at the [Society for arts and technology](http://sat.qc.ca/) [SAT]. These UGens were designed to be used with the _HOAUGens_ (see [sc3-plugins](https://github.com/supercollider/sc3-plugins)), but should also be usable with similar b-format ambisonic signals. See the `/doc` folder for information on how these decoders were created.

In addition to SuperCollider UGens, this repository contains a few optional resources, such as a small collection of [SATIE](https://gitlab.com/sat-metalab/SATIE) plugins that make use of the metalab-ugens. These can be found in `/satie` folder and copied to your 'SatieConfiguration.satieUserSupportDir' (see SATIE's documentation for more info).


## Installation

The metalab-ugens can be compilled from source and added to your SuperCollider installation.

To compile the plugins, you will need a copy of the metalab-ugens repository, as well as a copy of the SuperCollider repository. Run the following commands in a terminal to create a new directory and clone the metalab-ugens repository:

```
mkdir -p ~/src && cd ~/src
git clone https://gitlab.com/sat-metalab/metalab-ugens.git
```

Building SuperCollider plugins requires some header files that are included in the SuperCollider source. The easiest way to obtain these headers is by cloning the SuperCollider repository. In a terminal, run the following commands:

```
cd ~/src
git clone --recursive https://github.com/supercollider/supercollider.git
```

Once you've cloned the SuperCollider repository, you need to 'checkout' a specific version of this repository. For example, if you want to use the metalab-ugens with SuperCollider 3.9.3, you will need to build the metalab-ugens using the SuperCollider 3.9.3 header files. The cloned SuperCollider repository can be made to reflect a specific version of SuperCollider via tags. Move into your cloned SuperCollider repository and execute the following commands:

```
cd ~/src/supercollider
git checkout tags/Version-3.9.3    # replacing Version-x.x.x with the version you require
```

You can now begin building the metalab-ugens. Create a new directory called 'build' at the root of the repository:
```
cd ~/src/metalab-ugens
mkdir -p build && cd build
```

The metalab-ugens build system makes use of `cmake`. Run the following command in order to configure your build:

```
cmake -DSC_PATH=~/src/supercollider -DSUPERNOVA=ON -DNATIVE=ON ..
```

>>>
**Note:** The `SC_PATH` variable tells cmake where to find the SuperCollider source. You may need to change this variable to point to the correct location on your system.
>>>

Once cmake is configured correctly, you can now build and install the project:

```
cmake --build . --config Release --target install    # you may need to precede this with `sudo` on Linux
```

By default on Linux, the last command will install the plugins in two locations: `/usr/local/share/SuperCollider/Extensions/SC3plugins/metalab-ugens/` and `/usr/local/lib/SuperCollider/plugins/`. This is configurable via the `CMAKE_INSTALL_PREFIX` variable (defaults to `/usr/local/`).

If you are on macOS or Windows, after running the `--target install` command, you will find a new folder called `metalab-ugens` in your build folder. Copy this folder to your SuperCollider User Extensions folder. You can find this location on your system by opening SuperCollider and evaluating the following line:

```supercollider
Platform.userExtensionDir
```

Restart SuperCollider and the metalab-ugens should now be available to use. 

## Uninstallation

The build system offers an uninstall target to allow the removal of the metalab-ugens from your system. This is primarily aimed at Linux systems. In order to uninstall the metala-ugens, run the following command:

```
cmake --build . --target uninstall
```

On macOS and Windows, simply delete the `metalab-ugens` folder from the SuperCollider User Extensions directory mentionned above.

## License

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. See [LICENSE](LICENSE) for the license text.

## Sponsors

This project is made possible thanks to the [Society for Arts and Technology](http://www.sat.qc.ca/) [SAT] and to the Ministère de l'Économie, de la Science et de l'Innovation (MESI) du Québec

